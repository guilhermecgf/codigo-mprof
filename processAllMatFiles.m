%% --------------------------------------------------
% Script to open all .mat files and save everything into a single file
clear all; close all
% Second set of simulations: from BU cluster
% modifications: T = 0.02, smaller noise variances, less total time, no
% noise on input sequence (not odometry anymore, but deterministic input

setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

obs_landmarks_total = [];
obs_players_total = [];

err_x_time_total = [];
err_y_time_total = [];
err_psi_time_total = [];

mse_x_time_total = [];
mse_y_time_total = [];
mse_psi_time_total = [];
mse_dist_time_total = [];

data = cell(1,4);

% config BU
ID = 513;
M = 123;
strbase = ['output_final/sim_cluster_bu/' num2str(ID)];

% config teste
ID = 553;
M = 10;
strbase = ['output_final/teste1/' num2str(ID)];

% config teste
ID = 692;
M = 10;
strbase = ['output_final/teste2/' num2str(ID)];

% config itandroids
ID = 119;
M = 31;
strbase = ['output_final/sim_itandroids/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.2
ID = 13;
M = 100;
strbase = ['output_final/sim_itandroids_2/' num2str(ID)];

% config itandroids, trajet�ria e parametros antigos, T = 0.02, ruido muito baixo na camera
ID = 17;
M = 37;
strbase = ['output_final/sim_itandroids_3/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.02, ruido medio
% na camera
ID = 19;
M = 76;
strbase = ['output_final/sim_itandroids_4/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.02, ruido medio
% na camera
ID = 23;
M = 18;
strbase = ['output_final/sim_itandroids_5/' num2str(ID)];

% teste apenas PL
setupName = {'PL', 'PL', 'PL', 'PL'};
ID = 641;
M = 10;
strbase = ['output_final/' num2str(ID)];

ID = 715;
M = 10;
strbase = ['output_final/' num2str(ID)];

% teste de todos
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};
ID = 421;
M = 10;
strbase = ['output_final/' num2str(ID)];


for m = 1:M
    for s = 1:4
        aux = load([strbase '_' setupName{s} '_MCtraj' num2str(m,'%03d') '.mat']);
        data{s} = aux.savedvariable;
    end     
        
%     % APENAS PARA O TESTE S� DE PL
%     for s = 2:4
%         R = data{1}.simConfig.R;
%         for r = 1:R
%             data{s}.TeamEstSave{r,s} = data{1}.TeamEstSave{r,1};
%         end
%     end
%     % COMENTAR DEPOIS
    
    if m == 1
        R = data{1}.simConfig.R;
        nIterations = data{1}.simConfig.nIterations;
        T = data{1}.simConfig.T;
        time_vec = T * [1:nIterations];

        obs_landmarks = zeros(M, R, nIterations);
        obs_players = zeros(M, R, nIterations);

        err_x_time = zeros(M, nIterations, R, 4);
        err_y_time = zeros(M, nIterations, R, 4);
        err_psi_time = zeros(M, nIterations, R, 4);

        mse_x_time = zeros(M, nIterations, R, 4);
        mse_y_time = zeros(M, nIterations, R, 4);
        mse_psi_time = zeros(M, nIterations, R, 4);
        mse_dist_time = zeros(M, nIterations, R, 4);
        
        avg_cputime_per_robot_allmc = zeros(4, nIterations, M);

        
    end 

    for r=1:R
        for i=2:nIterations
            try
                obs_landmarks(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.goalsID) + length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.cornersID);
                obs_players(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.playersID);
            catch
                %disp(['no camera observations at time instant ' num2str(i)]);
            end
        end
    end

    for r = 1:R
        for s = 1:4
            err_x = data{s}.TeamEstSave{r,s}.estimationError(1,:);
            err_x_time(m, :, r, s) = err_x;
            mse_x_time(m, :, r, s) = err_x.^2;

            err_y = data{s}.TeamEstSave{r,s}.estimationError(2,:);
            err_y_time(m, :, r, s) = err_y;
            mse_y_time(m, :, r, s) = err_y.^2;

            mse_dist_time(m, :, r, s) = mse_x_time(m, :, r, s) + mse_y_time(m, :, r, s);

            err_psi = data{s}.TeamEstSave{r,s}.estimationError(3,:);
            err_psi_time(m, :, r, s) = err_psi;
            mse_psi_time(m, :, r, s) = err_psi.^2;
        end
    end
    
    for s = 1:4
        avg_cputime_per_robot_allmc(s,:,m) = data{s}.avg_cputime_per_robot(s,:);
    end
    
end

obs_landmarks_total = obs_landmarks;
obs_players_total = obs_players;

err_x_time_total = err_x_time;
err_y_time_total = err_y_time;
err_psi_time_total = err_psi_time;

mse_x_time_total = mse_x_time;
mse_y_time_total = mse_y_time;
mse_psi_time_total = mse_psi_time;
mse_dist_time_total = mse_dist_time;

% save(['output_final/sim_cluster_bu/sim_cluster_bu' num2str(ID) '.mat']);
% save(['output_final/sim_itandroids_5/sim' num2str(ID) '.mat']);
save(['output_final/sim' num2str(ID) '.mat']);