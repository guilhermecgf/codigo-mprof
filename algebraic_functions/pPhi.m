% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% p = pPhi(phi12, state1, state2, phiMean, phiVariance)
% phi12 is the measured angle, in global coordinates, of agent in state2 with
% reference in the agent in state1.
%
% Considering random variables Phi12, State1, State2 and a noise VPhi, with
% statistics phiMean and phiVariance, they relate to each other according 
% to the model:
%
% Phi12 = hPhi(State1, State2) + VPhi.
%
% Conditioning on known states, we have:
%
% Phi12 | {State1 = state1, State2 = state2} = hPhi(state1, state2) + VPhi,
%
% thus the distribution of Phi12 is the distribution of VPhi with an offset
% equal to hPhi(state1, state2)
function p = pPhi(phi12, state1, state2, DoAMean, DoAVariance)
    offset = hPhi(state1, state2);
    mu = DoAMean + offset;
    mu = wrapAngle(mu);
    stddev = sqrt(DoAVariance);
    
    inov = phi12 - mu;
    inov = wrapAngle(inov);
    p = normpdf(inov, 0, stddev);
    
%     % assuming normally distributed noise, with variance DoAVariance,
%     % ALREADY CODED IN THE HUMANOID CLASS!!!
%     % note that normpdf function uses the standard deviation as argument!
%     p = normpdf(phi12, mu, stddev);    
end