%% Testing probability functions
% pObs
rhoMean = 0;
rangeVariancePar = 0.0011^2;
phiMean = 0;
phiVariance = 0.0048^2;

DoAMean = 0;
DoAVariance = 0.0048^2;

state1 = [0;0;0];
state2 = [-1;0;0];
obs12 = [1; pi];
phi12 = pi;

% state1 = [0;0;0];
% state2 = [1;0;0];
% obs12 = [1; 0];
% phi12 = 0;

pObs(obs12, state1, state2, rhoMean, rangeVariancePar, phiMean, phiVariance)
pPhi(phi12, state1, state2, DoAMean, DoAVariance)

rhoVec = obs12(1) + linspace(-0.1,0.1,101);
phiVec = obs12(2) + linspace(-0.1,0.1,101);

[rr, pp] = meshgrid(rhoVec, phiVec);
funcObs = zeros(length(rhoVec), length(phiVec));
for i = 1:length(rhoVec)
    for j = 1:length(phiVec)
        obs12pluseps = [rhoVec(i); phiVec(j)];
%         funcObs(i,j) = log( pObs(obs12pluseps, state1, state2, rhoMean, rangeVariancePar, phiMean, phiVariance) );
        funcObs(i,j) = pObsLog(obs12pluseps, state1, state2, rhoMean, rangeVariancePar, phiMean, phiVariance) ;
    end
end

figure(1);
subplot(1,2,1);
mesh(rr, pp, funcObs);
xlabel('range (m)'); ylabel('bearing (rad)'); zlabel('log probability density');

funcPhi = zeros(length(phiVec), 1);
for j = 1:length(phiVec)
    funcPhi(j) = log( pPhi(phiVec(j), state1, state2, DoAMean, DoAVariance) );
end

subplot(1,2,2);
plot(phiVec, funcPhi);
xlabel('bearing (rad)'); ylabel('log probability density');
        