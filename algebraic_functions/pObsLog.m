% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% p = pObs(obs12, state1, state2, rhoMean, rhoVariancePar, phiMean, phiVariance)
% obs12 contains the measured distance and angle, in global coordinates, by 
% the agent in state1 of the agent in state2.
%
% Considering random variables Obs12, State1, State2 and noises VRho and VPhi,
% with statistics rhoMean, rhoVariance, phiMean and phiVariance, they 
% relate to each other according to the model:
%
% Obs12 = [Rho12; Phi12] 
%       = [hRho(State1, State2); hPhi(State1, State2)] + [VRho; VPhi]
%
% Conditioning on known states, we have:
%
% Obs12 | {State1 = state1, State2 = state2} = [hRho(State1, State2); hPhi(State1, State2)] + [VRho; VPhi],
%
% thus the distribution of Obs12 is the joint distribution of VRho and VPhi
% with an vector offset equal to [hRho(state1, state2); hPhi(state1, state2)]
%
% Note that, in some models, the variance of the noise associated with the
% distance, VRho, depends on the distance being measured. In this function,
% we assume rhoVariance = rhoVariancePar*(0.01*rho)
function pLog = pObsLog(obs12, state1, state2, rhoMean, rhoVariancePar, phiMean, phiVariance)
    offset = [hRho(state1, state2); hPhi(state1, state2)];
    
    mu = [rhoMean; phiMean] + offset;
    mu(2) = wrapAngle(mu(2));
    
    Sigma = zeros(2,2);
    Sigma(1,1) = rhoVariancePar * ( offset(1) )^2;
    Sigma(2,2) = phiVariance;
    
    inov = obs12 - mu;
    inov(2) = wrapAngle(inov(2));
    pLog = -1/2*inov.' * pinv(Sigma) * inov;
    
%     
%     % assuming normally distributed noise, with variance DoAVariance,
%     % ALREADY CODED IN THE HUMANOID CLASS!!!
%     % note that mvnpdf function uses the covariance matrix as argument!
%     p = mvnpdf(obs12, mu, Sigma);    
end