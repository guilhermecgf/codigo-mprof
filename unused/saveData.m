function saveData(realData, observed, odometryData)

addpath('../matlab_json');

for i=1:length(observed)
    if isempty(observed(i).corners')
        observed(i).corners = 0;
    else
        observed(i).corners = linearizeNx2Matrix(observed(i).corners');
    end
    if isempty(observed(i).centralCorners)
        observed(i).centralCorners = 0;
    else
        observed(i).centralCorners = linearizeNx2Matrix(observed(i).centralCorners');
    end
    if isempty(observed(i).middleOfTheField)
        observed(i).middleOfTheField = 0;
    else
        observed(i).middleOfTheField = linearizeNx2Matrix(observed(i).middleOfTheField');
    end
    if isempty(observed(i).goals)
        observed(i).goals = 0;
    else
        observed(i).goals = linearizeNx2Matrix(observed(i).goals');
    end
    if isempty(observed(i).penaltyKick)
        observed(i).penaltyKick = 0;
    else
        observed(i).penaltyKick = linearizeNx2Matrix(observed(i).penaltyKick');
    end
    if isempty(observed(i).goalAreaCorners)
        observed(i).goalAreaCorners = 0;
    else
        observed(i).goalAreaCorners = linearizeNx2Matrix(observed(i).goalAreaCorners');
    end
    if isempty(observed(i).lineList)
        observed(i).lineList = 0;
    else
        observed(i).lineList = linearizeNx2Matrix(observed(i).lineList');
    end
end

save('real_data.txt', 'realData', '-ascii');
save('odometry_data.txt', 'odometryData', '-ascii');
save('observed.mat', 'observed');
%json.write(observed, 'observed.txt');

end
% 
% function array = linearizeNx2Matrix(matrix)
%     numLines = size(matrix, 1);
%     array = [];
%     for i=1:length(numLines) observed(i).lineList
%         array = [array, matrix(i, :)];
%     end
% end
function array = linearizeNx2Matrix(matrix)
    array = reshape(matrix,1,length(matrix(:,1))*length(matrix(1,:)));
end