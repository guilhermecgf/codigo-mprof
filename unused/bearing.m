function bearing = bearing(x,y,dist,x1,y1,x2,y2,psi)
theta = atan2(y2-y1,x2-x1);
pa = [x y] + dist*[-sin(theta) cos(theta)];
pb = [x y] - dist*[-sin(theta) cos(theta)];
if abs((y1- y2)*pa(1) + (x2-x1)*pa(2) - (x2*y1 - y2*x1)) < 0.00001
    bearing = - theta + psi;
else
    bearing = psi -(theta + pi);
    
end
end