
%%%%%%% Field draw %%%%%%%
landmarks = generateLandmarkList();
%==========================

estimateData = load('estimate_data.txt');

xEstimatePosition = estimateData(:,1);
yEstimatePosition = estimateData(:,2);
psiEstimatePosition = estimateData(:,3);

realData = load('real_data.txt');

xRealData = realData(:,1);
yRealData = realData(:,2);
psiRealData = realData(:,3);

resetData = load('reset_data.txt');

xResetData = resetData(:,1);
yResetData = resetData(:,2);

figure(1)
drawField(landmarks);
hold on
h(1) = plot(xEstimatePosition, yEstimatePosition,'b', 'LineWidth', 2);
h(2) = plot(xRealData, yRealData, 'k--', 'LineWidth', 2);
% plot(xResetData, yResetData,'r*', 'LineWidth', 2);
legend([h(1),h(2)],{'estimated position'; 'real position'})

% print -depsc2 UPF_non_zero_noise.eps