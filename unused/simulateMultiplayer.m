close all; clear all;
addpath('algebraic_functions')
%% Multiplayer simulation: based on simulateHumanoidKidSize;
check = [];
simID = randi(1000); % para o nome do arquivo onde os dados ser�o salvos

%% Parameter for generating scenario
rng(30);
simConfig.time = 6;
simConfig.T = 0.2;
simConfig.nIterations = simConfig.time/simConfig.T;
simConfig.R = 5;

% Number of Monte Carlo iterations
simConfig.M = 15;

talkerSchedule = zeros(simConfig.nIterations, 1);
nextTalker = 1;
for n = 2:2:simConfig.nIterations
    talkerSchedule(n) = nextTalker;
    nextTalker = nextTalker + 1;
    if nextTalker > simConfig.R
        nextTalker = nextTalker - simConfig.R;
    end
end

%Outputs configuration 
showfigOverview = false; % plot only with the players' position and pose
outputConfig.showfig = false; % for R = 4, w/ figure: 30 ms; without: 0.5 ms
outputConfig.withpause = false;
outputConfig.generatePrints = false;

% zagueiro esquerdo, camisa 4
initialConditions(1).x0 = -3.0;
initialConditions(1).y0 = 1.0;
initialConditions(1).psi0 = 0.9; %-2.7; % 0.9; % -180*pi/180;
initialConditions(1).v_x = 1*0.2;
initialConditions(1).v_y = 0*0.0;
initialConditions(1).v_psi = 1*-0.45;

% segundo volante, camisa 8
initialConditions(2).x0 = 0.5;
initialConditions(2).y0 = -0.5;
initialConditions(2).psi0 = 45*pi/180;
initialConditions(2).v_x = 1*0.1;
initialConditions(2).v_y = 1*0.1;
initialConditions(2).v_psi = 1*0.10;

% ponta esquerda, camisa 11
initialConditions(3).x0 = 2.5;
initialConditions(3).y0 = 1.0;
initialConditions(3).psi0 = -90*pi/180;
initialConditions(3).v_x = 0*0.0;
initialConditions(3).v_y = 1*-0.2;
initialConditions(3).v_psi = 1*0.12;

% centro avante, camisa 9
initialConditions(4).x0 = 3.5;
initialConditions(4).y0 = -1.0;
initialConditions(4).psi0 = 180*pi/180;
initialConditions(4).v_x = 1*0.35;
initialConditions(4).v_y = 0*0.0;
initialConditions(4).v_psi = 1*-0.28;

% zagueiro direito, camisa 3
initialConditions(5).x0 = -3.0;
initialConditions(5).y0 = -1.0;
initialConditions(5).psi0 = -10*pi/180;
initialConditions(5).v_x = 1*0.22;
initialConditions(5).v_y = 0*0.0;
initialConditions(5).v_psi = 1*0.25;

% primeiro volante, camisa 5
initialConditions(6).x0 = -0.5;
initialConditions(6).y0 = 0.25;
initialConditions(6).psi0 = 0*pi/180;
initialConditions(6).v_x = 1*0.1;
initialConditions(6).v_y = 1*0.1;
initialConditions(6).v_psi = 1*0.35;

% ponta direita, camisa 7
initialConditions(7).x0 = 2.5;
initialConditions(7).y0 = -1.0;
initialConditions(7).psi0 = 45*pi/180;
initialConditions(7).v_x = 0*0.0;
initialConditions(7).v_y = 1*0.2;
initialConditions(7).v_psi = 1*-0.20;

% meia cl�ssico, camisa 10
initialConditions(8).x0 = 2.5;
initialConditions(8).y0 = 0.0;
initialConditions(8).psi0 = 0*pi/180;
initialConditions(8).v_x = 1*0.1;
initialConditions(8).v_y = 0*0.0;
initialConditions(8).v_psi = 1*-0.30;

% depois, conforme surgir necessidade, posicionar os outros jogadores

simConfig.landmarks = generateLandmarkList();

% Defining team of robots and initializing the parameters for each one
Team = cell(simConfig.R,1);
playerMap = zeros(simConfig.R, 3, simConfig.nIterations);

%% Starting simulation: generating scenario

for r = 1:simConfig.R
    % por enquanto, s� especifiquei posi��o de 8 jogadores
    Team{r} = Humanoid(r, initialConditions(r));
    
    % estou copiando todo o objeto... talvez d� para copiar apenas o que for relevante
    Team{r} = Team{r}.simulationSetup(simConfig, outputConfig);
    
    playerMap(r,:,1) = Team{r}.realData(1,:);
end
playerMapUpdated = playerMap;

% Running simulation; external loop is temporal, so each step corresponds to a time step T
% Inner loops could be run in parallel
% This loop generates sensor information.
% estimation can either be done inside or in another timeIndex loop
for timeIndex = 2:simConfig.nIterations
    disp(['generating simulation data, iteration number: ' num2str(timeIndex)]);
    tic
    
    for r = 1:simConfig.R
        % estou copiando todo o objeto... 
        r;    
        Team{r} = Team{r}.move(timeIndex);        
        playerMapUpdated(r,:,timeIndex) = Team{r}.realData(timeIndex,:);        
    end
    playerMap = playerMapUpdated;
    
    % Hear bearing every two time instants
%     if mod(timeIndex,2) == 0
    talkerIndex = talkerSchedule(timeIndex);
    if talkerIndex ~= 0
        for r = [1:talkerIndex-1, talkerIndex+1:simConfig.R]
            r;
            Team{r} = Team{r}.hear(timeIndex, talkerIndex, playerMap);
        end
    end
    
    % Observe with the camera every three time instants
    if mod(timeIndex,3) == 0
        for r = 1:simConfig.R
            r;
            %Team{r} = Team{r}.observe(timeIndex, landmarks, playerMap);
            Team{r} = Team{r}.observe(timeIndex, playerMap);
        end
    end
    
    if(outputConfig.showfig)        
        figure(1);
        
        ncols_plot = ceil(sqrt(simConfig.R));
        nrows_plot = ceil(simConfig.R/ncols_plot);
        for r = 1:simConfig.R 
            subplot(nrows_plot,nrows_plot,r); hold off;
            Team{r}.plotPlayerInfo(timeIndex);
        end        
    end
            
    
    if(showfigOverview)    
        figure(100); %draw playerMapUpdated   
        clf('reset');
        drawField(simConfig.landmarks)
        for r = 1:simConfig.R
            drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex) );
            %drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex), Team{r}.visionAngle );
        end
    end
    
    timeElapsedInCalculations = toc;
    
    % to evaluate performance in different scenarios
    timeElapsedInCalculationsVec(timeIndex-1) = timeElapsedInCalculations;
    
    if(timeElapsedInCalculations > simConfig.T)
        warning('robot iterations are taking more than desired step length. Hint: remove plotting of each robot');
    end
    
    if(outputConfig.withpause)
%         pause(simConfig.T - timeElapsedInCalculations);
        pause; % advance pressing any key
    end
    toc
end
        
mean(timeElapsedInCalculationsVec)

%% Configuring setup for estimation
setup = {'PL','PLH','PLHA','PLA'};
flags = [true, true, false, false;
         true, true, true, false;
         true, true, true, true;
         true, true, false, true];
     
% setup = {'PLH', 'PLHA', 'PLA'};
% flags = [true, true, true, false; 
%          true, true, true, true;
%          true, true, false, true];
     
% setup = {'PLHA'};
% flags = [true, true, true, true];
     
% setup = {'PL', 'PLH', 'PLHA'};
% flags = [true, true, false, false;
%          true, true, true, false;
%          true, true, true, true];
     
% % if true, resamples after every set assimilation (landmarks, hearing and/or allies) 
% % if false, resamples only after the last assimilation
% alwaysResample = false;

% if true, filter performs resampling and regularization
% if false, filter performs only resampling
bRegularize = false;

% if true, all players know at all times the most recent belief of each ally
% if false, the information from each ally's belief comes only at the talking instants
mostRecentBelief = false;

% Number of particles. P and L steps are linear on Np, whereas H and A are
% quadratic
Np = 600;

%% Estimation step

% MONTE CARLO ITERATIONS
for m = 1:simConfig.M
    
% on each column, one estimation algorithm will be performed and the
% corresponding data for each robot will be stored.
TeamEst = cell(simConfig.R, length(setup));
for r = 1:simConfig.R
    for s = 1:length(setup)
        TeamEst{r,s} = Team{r};
    end
end

% initializing the algorithms
% self position uncertainty
var_x0 = 1;
var_y0 = 1;
var_psi0 = 1;
for s = 1:length(setup)
    for r = 1:simConfig.R
        TeamEst{r,s} = TeamEst{r,s}.initializeSMC(Np, var_x0, var_y0, var_psi0);
    end
    
    % All allies have each other initial belief
    for talkerIndex = 1:simConfig.R
        initialBelief = cell(1,5);
        initialBelief{1} = 0; % time
        initialBelief{2} = 1; % number of modes
        initialBelief{3} = 1; % vector of weight of modes
        initialBelief{4} = [initialConditions(talkerIndex).x0; initialConditions(talkerIndex).y0; initialConditions(talkerIndex).psi0];
        initialBelief{5} = diag([var_x0, var_y0, var_psi0]);
        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, initialBelief);
        end
    end  
end

for timeIndex = 2:simConfig.nIterations
    
%     timeIndex
    
    for s = 1:length(setup)
%         s
    
        performPrediction = flags(s,1);
        performLandmarks = flags(s,2);
        performHearing = flags(s,3);
        performAlliesObs = flags(s,4);

        if mod(timeIndex, 1) == 0
            disp(['Monte Carlo: ' num2str(m) ' - ' setup{s} ' - performing estimation, iteration number: ' num2str(timeIndex)]);
        end

        if performPrediction
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO MODIFICATION
%                 TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex);
                
                % WITH MONTE CARLO MODIFICATION
                TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 1);
            end
        end

        if performLandmarks
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 2);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after L, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 6);
            end
        end

        if mostRecentBelief
            % Receiving all allies' most recent belief at all time instants
            for talkerIndex = 1:simConfig.R
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end        
        else    
            % Receive neighbors' self belief according to talkerSchedule
            talkerIndex = talkerSchedule(timeIndex);
            if talkerIndex ~= 0
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);    
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end
        end

        if performHearing
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 3);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end                
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after H, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 7);
                
            end
        end

        if performAlliesObs
            for r = 1:simConfig. R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 4);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end                
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after A, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 8);
            end
        end
        
        % Important theoretical issue: if assimilating hearing AND camera
        % from the same agent at the same time instant, that must be done
        % jointly, not in two separate steps, because it is a single
        % integral

%         for r = 1:simConfig.R
%             if ~alwaysResample
%                 TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%             end
%         end

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateNewBelief(timeIndex);
        end

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateErrors(timeIndex);
        end

    %     for r = 1:simConfig.R
    %         TeamEst{r,s}.plotParticles(timeIndex)
    % %         TeamEst{r,s}.plotMeanParticles(timeIndex);
    %     end
%         pause;
    
        if mod(timeIndex, 5) == 0
            try
%                 save(['output_temp/' datestr(now, 'yyyymmddTHHMMSS') '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
                save(['output_temp/' datestr(now, 'yyyymmdd-') num2str(simID) '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
            catch
            end
        end
        if timeIndex == simConfig.nIterations
            try
%                 save(['output_final/' datestr(now, 'yyyymmddTHHMMSS') '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
                save(['output_final/' datestr(now, 'yyyymmdd-') num2str(simID) '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
            catch
            end
        end
    end % s = 1:length(setup)

end % timeIndex = 1:nIterations

end % m = 1:M
