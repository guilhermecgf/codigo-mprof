% Determines which features (such as landmarks or other players) a robot
% can see, given its position, pose and angle of vision.
% The plotting step is also performed here

function observed = observation_withplot(x,y,psi,visionAngle,landmarks,config,currentPlayerPos)
showfig = config.showfig;
pointSize = 50;
% withError = 0;
withError = 1;

% players, goal posts and field corners are returned with labels, as we
% choose to represent them as uniquely tagged.
    
[observed.players, label] = visionPoint(x,y,psi,visionAngle,currentPlayerPos);

if ~isscalar(observed.players)
    for i = 1:length(observed.players(:,1))
        observed.players(i,:) = globalToLocalPoint(x,y,psi,observed.players(i,:));
    end
    if withError
        for i = 1:length(observed.players(:,1))
            % noise is being added in local reference frame, isn't it?
            observed.players(i,:) = addNoise(observed.players(i, :), config);
        end
    end
    for i = 1:length(observed.players(:,1))
        % reconversion after noise has been added
        observed.players(i,:) = localToGlobalPoint(x,y,psi,observed.players(i,:));
    end
    if showfig
        plot(observed.players(:,1),observed.players(:,2),'hk','MarkerSize',pointSize);
        playerTags = {'P1', 'P2', 'P3', 'P4'};
        for i = 1:length(observed.players(:,1))
            text(observed.players(i,1), observed.players(i,2), playerTags{label(i)});
        end
    end
end

[observed.goals, label] = visionPoint(x,y,psi,visionAngle,landmarks.goalList);

if ~isscalar(observed.goals)
    for i = 1:length(observed.goals(:,1))
        observed.goals(i,:) = globalToLocalPoint(x,y,psi,observed.goals(i,:));
    end
    if withError
        for i = 1:length(observed.goals(:,1))
            observed.goals(i,:) = addNoise(observed.goals(i, :), config);%mvnrnd(observed.goals(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.goals(:,1))
        observed.goals(i,:) = localToGlobalPoint(x,y,psi,observed.goals(i,:));
    end
    if showfig
        plot(observed.goals(:,1),observed.goals(:,2),'sr','MarkerSize',pointSize);
        goalTags = {'G1', 'G2', 'G3', 'G4'};
        for i = 1:length(observed.goals(:,1))
            text(observed.goals(i,1), observed.goals(i,2), goalTags{label(i)}, 'Color', 'red');
        end
    end
end

[observed.corners, label] = visionPoint(x,y,psi,visionAngle,landmarks.cornerList);

if ~isscalar(observed.corners)
    for i = 1:length(observed.corners(:,1))
        observed.corners(i,:) = globalToLocalPoint(x,y,psi,observed.corners(i,:));
    end
    if withError
        for i = 1:length(observed.corners(:,1))
            observed.corners(i,:) = addNoise(observed.corners(i, :), config);%mvnrnd(observed.corners(i,:),[errorVariance 0; 0  errorVariance]);
        end
    end
    for i = 1:length(observed.corners(:,1))
        observed.corners(i,:) = localToGlobalPoint(x,y,psi,observed.corners(i,:));
    end
    if showfig
        plot(observed.corners(:,1),observed.corners(:,2),'sg','MarkerSize',pointSize);
        cornerTags = {'C1', 'C2', 'C3', 'C4'};
        for i = 1:length(observed.corners(:,1))
            text(observed.corners(i,1), observed.corners(i,2), cornerTags{label(i)}, 'Color', 'green');
        end
    end
end

%{

observed.centralCorners = visionPoint(x,y,psi,visionAngle,landmarks.centralCornerList);
if ~isscalar(observed.centralCorners)
    for i = 1:length(observed.centralCorners(:,1))
        observed.centralCorners(i,:) = globalToLocalPoint(x,y,psi,observed.centralCorners(i,:));
    end
    
    if withError
        for i = 1:length(observed.centralCorners(:,1))
            observed.centralCorners(i,:) = addNoise(observed.centralCorners(i, :), config);%mvnrnd(observed.centralCorners(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.centralCorners(:,1))
        observed.centralCorners(i,:) = localToGlobalPoint(x,y,psi,observed.centralCorners(i,:));
    end
    if showfig
        plot(observed.centralCorners(:,1),observed.centralCorners(:,2),'sy','MarkerSize',pointSize);
    end
end


observed.middleOfTheField = visionPoint(x,y,psi,visionAngle,landmarks.middleOfTheFieldList);
if ~isscalar(observed.middleOfTheField)
    for i = 1:length(observed.middleOfTheField(:,1))
        observed.middleOfTheField(i,:) = globalToLocalPoint(x,y,psi,observed.middleOfTheField(i,:));
    end
    if withError
        for i = 1:length(observed.middleOfTheField(:,1))
            observed.middleOfTheField(i,:) = addNoise(observed.middleOfTheField(i, :), config);%mvnrnd(observed.middleOfTheField(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.middleOfTheField(:,1))
        observed.middleOfTheField(i,:) = localToGlobalPoint(x,y,psi,observed.middleOfTheField(i,:));
    end
    if showfig
        plot(observed.middleOfTheField(:,1),observed.middleOfTheField(:,2),'sc','MarkerSize',pointSize);
    end
end


observed.penaltyKick = visionPoint(x,y,psi,visionAngle,landmarks.penaltyKickList);
if ~isscalar(observed.penaltyKick)
    for i = 1:length(observed.penaltyKick(:,1))
        observed.penaltyKick(i,:) = globalToLocalPoint(x,y,psi,observed.penaltyKick(i,:));
    end
    if withError
        for i = 1:length(observed.penaltyKick(:,1))
            observed.penaltyKick(i,:) = addNoise(observed.penaltyKick(i, :), config);%mvnrnd(observed.penaltyKick(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.penaltyKick(:,1))
        observed.penaltyKick(i,:) = localToGlobalPoint(x,y,psi,observed.penaltyKick(i,:));
    end
    if showfig
        plot(observed.penaltyKick(:,1),observed.penaltyKick(:,2),'sm','MarkerSize',pointSize);
    end
end

observed.goalAreaCorners = visionPoint(x,y,psi,visionAngle,landmarks.goalAreaCornerList);
if ~isscalar(observed.goalAreaCorners)
    for i = 1:length(observed.goalAreaCorners(:,1))
        observed.goalAreaCorners(i,:) = globalToLocalPoint(x,y,psi,observed.goalAreaCorners(i,:));
    end
    if withError
        for i = 1:length(observed.goalAreaCorners(:,1))
            observed.goalAreaCorners(i,:) = addNoise(observed.goalAreaCorners(i, :), config);%mvnrnd(observed.goalAreaCorners(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.goalAreaCorners(:,1))
        observed.goalAreaCorners(i,:) = localToGlobalPoint(x,y,psi,observed.goalAreaCorners(i,:));
    end
    if showfig
        plot(observed.goalAreaCorners(:,1),observed.goalAreaCorners(:,2),'sk','MarkerSize',pointSize);
    end
end

observed.lineList = visionLineExtremes(x, y, psi, visionAngle, landmarks.lineList, config);
if ~isscalar(observed.lineList)
    for i = 1:length(observed.lineList(:,1))
        observed.lineList(i,:) = globalToLocalPoint(x,y,psi,observed.lineList(i,:));
    end
    if withError
        for i = 1:length(observed.lineList(:,1))
            observed.lineList(i,:) = addNoise(observed.lineList(i, :), config);%mvnrnd(observed.lineList(i,:),[errorVariance 0;0 errorVariance]);
        end
    end
    for i = 1:length(observed.lineList(:,1))
        observed.lineList(i,:) = localToGlobalPoint(x,y,psi,observed.lineList(i,:));
    end
    if showfig
        plot(observed.lineList(:,1),observed.lineList(:,2),'xm','MarkerSize',pointSize);
    end
end

%}

end
