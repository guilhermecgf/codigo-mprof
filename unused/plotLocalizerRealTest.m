estimate = load('test_localize.txt');

landmarks = generateLandmarkList();

figure
drawField(landmarks);
hold on
plot(estimate(:,1), estimate(:,2), 'b', 'LineWidth', 2);