close all; clear all;
addpath('algebraic_functions')
%% Multiplayer simulation: based on simulateHumanoidKidSize;
simID = randi(1000); % para o nome do arquivo onde os dados ser�o salvos

%% Parameter for generating scenario
rng(30);
simConfig.time = 6;
simConfig.T = 0.2;
simConfig.nIterations = simConfig.time/simConfig.T;
simConfig.R = 5;

% Number of Monte Carlo iterations
simConfig.M = 3;
simConfig.cameraInterval = 3;

talkerSchedule = zeros(simConfig.nIterations, 1);
nextTalker = 1;
for n = 2:2:simConfig.nIterations
    talkerSchedule(n) = nextTalker;
    nextTalker = nextTalker + 1;
    if nextTalker > simConfig.R
        nextTalker = nextTalker - simConfig.R;
    end
end

%Outputs configuration 
showfigOverview = false; % plot only with the players' position and pose
outputConfig.showfig = false; % for R = 4, w/ figure: 30 ms; without: 0.5 ms
outputConfig.withpause = false;
outputConfig.generatePrints = false;

% zagueiro esquerdo, camisa 4
initialConditions(1).x0 = -3.0;
initialConditions(1).y0 = 1.0;
initialConditions(1).psi0 = 0.9; %-2.7; % 0.9; % -180*pi/180;
initialConditions(1).v_x = 1*0.2;
initialConditions(1).v_y = 0*0.0;
initialConditions(1).v_psi = 1*-0.45;

% segundo volante, camisa 8
initialConditions(2).x0 = 0.5;
initialConditions(2).y0 = -0.5;
initialConditions(2).psi0 = 45*pi/180;
initialConditions(2).v_x = 1*0.1;
initialConditions(2).v_y = 1*0.1;
initialConditions(2).v_psi = 1*0.10;

% ponta esquerda, camisa 11
initialConditions(3).x0 = 2.5;
initialConditions(3).y0 = 1.0;
initialConditions(3).psi0 = -90*pi/180;
initialConditions(3).v_x = 0*0.0;
initialConditions(3).v_y = 1*-0.2;
initialConditions(3).v_psi = 1*0.12;

% centro avante, camisa 9
initialConditions(4).x0 = 3.5;
initialConditions(4).y0 = -1.0;
initialConditions(4).psi0 = 180*pi/180;
initialConditions(4).v_x = 1*0.35;
initialConditions(4).v_y = 0*0.0;
initialConditions(4).v_psi = 1*-0.28;

% zagueiro direito, camisa 3
initialConditions(5).x0 = -3.0;
initialConditions(5).y0 = -1.0;
initialConditions(5).psi0 = -10*pi/180;
initialConditions(5).v_x = 1*0.22;
initialConditions(5).v_y = 0*0.0;
initialConditions(5).v_psi = 1*0.25;

% primeiro volante, camisa 5
initialConditions(6).x0 = -0.5;
initialConditions(6).y0 = 0.25;
initialConditions(6).psi0 = 0*pi/180;
initialConditions(6).v_x = 1*0.1;
initialConditions(6).v_y = 1*0.1;
initialConditions(6).v_psi = 1*0.35;

% ponta direita, camisa 7
initialConditions(7).x0 = 2.5;
initialConditions(7).y0 = -1.0;
initialConditions(7).psi0 = 45*pi/180;
initialConditions(7).v_x = 0*0.0;
initialConditions(7).v_y = 1*0.2;
initialConditions(7).v_psi = 1*-0.20;

% meia cl�ssico, camisa 10
initialConditions(8).x0 = 2.5;
initialConditions(8).y0 = 0.0;
initialConditions(8).psi0 = 0*pi/180;
initialConditions(8).v_x = 1*0.1;
initialConditions(8).v_y = 0*0.0;
initialConditions(8).v_psi = 1*-0.30;

% depois, conforme surgir necessidade, posicionar os outros jogadores

simConfig.landmarks = generateLandmarkList();

% Defining team of robots and initializing the parameters for each one
Team = cell(simConfig.R,1);
playerMap = zeros(simConfig.R, 3, simConfig.nIterations);

%% Starting simulation: generating scenario

for r = 1:simConfig.R
    % por enquanto, s� especifiquei posi��o de 8 jogadores
    Team{r} = Humanoid(r, initialConditions(r));
    
    % estou copiando todo o objeto... talvez d� para copiar apenas o que for relevante
    Team{r} = Team{r}.simulationSetup(simConfig, outputConfig);
    
    playerMap(r,:,1) = Team{r}.realData(1,:);
end
playerMapUpdated = playerMap;

% Running simulation; external loop is temporal, so each step corresponds to a time step T
% Inner loops could be run in parallel
% This loop generates sensor information.
% estimation can either be done inside or in another timeIndex loop
for timeIndex = 2:simConfig.nIterations
    disp(['generating simulation data, iteration number: ' num2str(timeIndex)]);
    tic
    
    for r = 1:simConfig.R
        % estou copiando todo o objeto... 
        r;    
        Team{r} = Team{r}.move(timeIndex);        
        playerMapUpdated(r,:,timeIndex) = Team{r}.realData(timeIndex,:);        
    end
    playerMap = playerMapUpdated;
    
    % Hear bearing every two time instants
%     if mod(timeIndex,2) == 0
    talkerIndex = talkerSchedule(timeIndex);
    if talkerIndex ~= 0
        for r = [1:talkerIndex-1, talkerIndex+1:simConfig.R]
            r;
            Team{r} = Team{r}.hear(timeIndex, talkerIndex, playerMap);
        end
    end
    
    % Observe with the camera every three time instants
    if mod(timeIndex,simConfig.cameraInterval) == 0
        for r = 1:simConfig.R
            r;
            %Team{r} = Team{r}.observe(timeIndex, landmarks, playerMap);
            Team{r} = Team{r}.observe(timeIndex, playerMap);
        end
    end
    
    if(outputConfig.showfig)        
        figure(1);
        
        ncols_plot = ceil(sqrt(simConfig.R));
        nrows_plot = ceil(simConfig.R/ncols_plot);
        for r = 1:simConfig.R 
            subplot(nrows_plot,nrows_plot,r); hold off;
            Team{r}.plotPlayerInfo(timeIndex);
        end
    end
            
    
    if(showfigOverview)    
        figure(100); %draw playerMapUpdated   
        clf('reset');
        drawField(simConfig.landmarks)
        for r = 1:simConfig.R
            drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex) );
            %drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex), Team{r}.visionAngle );
        end
    end
    
    timeElapsedInCalculations = toc;
    
    % to evaluate performance in different scenarios
    timeElapsedInCalculationsVec(timeIndex-1) = timeElapsedInCalculations;
    
    if(timeElapsedInCalculations > simConfig.T)
        warning('robot iterations are taking more than desired step length. Hint: remove plotting of each robot');
    end
    
    if(outputConfig.withpause)
%         pause(simConfig.T - timeElapsedInCalculations);
        pause; % advance pressing any key
    end
    toc
end
        
mean(timeElapsedInCalculationsVec)

save(['scenarios/' datestr(now, 'yyyymmdd-') 'MC' num2str(simConfig.M) '.mat']);

