% Converts each 2D point corresponding to a robot's observation from global to local coordinates

function observed = prepareObservedData_KidSize(x, y, psi, observed)

%{
fn = fieldnames(observed);

for nameInd = 1:numel(fn)
    data = observed.(fn{nameInd});
    if ~isscalar(data)
        for i = 1:size(data, 1)
            observedLocal.(fn{nameInd})(i, :) = globalToLocalPoint(x, y, psi, data(i, :));
        end
    end
end
%}

if ~isscalar(observed.corners)
    for i=1:size(observed.corners, 1)
        observed.corners(i, :) = globalToLocalPoint(x, y, psi, observed.corners(i, :));
    end
end

if ~isscalar(observed.centralCorners)
    for i=1:size(observed.centralCorners, 1)
        observed.centralCorners(i, :) = globalToLocalPoint(x, y, psi, observed.centralCorners(i, :));
    end
end

if ~isscalar(observed.middleOfTheField)
    for i=1:size(observed.middleOfTheField, 1)
        observed.middleOfTheField(i, :) = globalToLocalPoint(x, y, psi, observed.middleOfTheField(i, :));
    end
end

if ~isscalar(observed.goals)
    for i=1:size(observed.goals, 1)
        observed.goals(i, :) = globalToLocalPoint(x, y, psi, observed.goals(i, :));
    end
end

if ~isscalar(observed.penaltyKick)
    for i=1:size(observed.penaltyKick, 1)
        observed.penaltyKick(i, :) = globalToLocalPoint(x, y, psi, observed.penaltyKick(i, :));
    end
end

if ~isscalar(observed.goalAreaCorners)
    for i=1:size(observed.goalAreaCorners, 1)
        observed.goalAreaCorners(i, :) = globalToLocalPoint(x, y, psi, observed.goalAreaCorners(i, :));
    end
end

if ~isscalar(observed.lineList)
    %observed.lineList
    for i=1:size(observed.lineList, 1)
        observed.lineList(i, :) = globalToLocalPoint(x, y, psi, observed.lineList(i, :));
    end
    %observed.lineList
end

%}

end