function point = addNoise(point, config)
%     point = globalToLocalPoint(x, y, psi, point);
    [distance, bearing] = cartesianToDistanceBearing(point(1), point(2));
    distance = mvnrnd(distance, config.distanceVariance);
    bearing = mvnrnd(bearing, config.bearingVariance);
    [point(1), point(2)] = distanceBearingToCartesian(distance, bearing);
end