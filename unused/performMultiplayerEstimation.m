addpath('algebraic_functions')

scenario = 'scenarios/20200216-MC3.mat';
load(scenario);

estID = 2;

%% Configuring setup for estimation
setup = {'PL','PLH','PLHA','PLA'};
flags = [true, true, false, false;
         true, true, true, false;
         true, true, true, true;
         true, true, false, true];
     
% setup = {'PLH', 'PLHA', 'PLA'};
% flags = [true, true, true, false; 
%          true, true, true, true;
%          true, true, false, true];
     
% setup = {'PLHA'};
% flags = [true, true, true, true];
     
% setup = {'PL', 'PLH', 'PLHA'};
% flags = [true, true, false, false;
%          true, true, true, false;
%          true, true, true, true];
     
% % if true, resamples after every set assimilation (landmarks, hearing and/or allies) 
% % if false, resamples only after the last assimilation
% alwaysResample = false;

% if true, filter performs resampling and regularization
% if false, filter performs only resampling
bRegularize = false;

% if true, all players know at all times the most recent belief of each ally
% if false, the information from each ally's belief comes only at the talking instants
mostRecentBelief = true;

% Number of particles. P and L steps are linear on Np, whereas H and A are
% quadratic
Np = 10;

%% Estimation step

% MONTE CARLO ITERATIONS
for m = 1:simConfig.M
    
% on each column, one estimation algorithm will be performed and the
% corresponding data for each robot will be stored.
TeamEst = cell(simConfig.R, length(setup));
for r = 1:simConfig.R
    for s = 1:length(setup)
        TeamEst{r,s} = Team{r};
    end
end

% initializing the algorithms
% self position uncertainty
var_x0 = 1;
var_y0 = 1;
var_psi0 = 1;
for s = 1:length(setup)
    for r = 1:simConfig.R
        TeamEst{r,s} = TeamEst{r,s}.initializeSMC(Np, var_x0, var_y0, var_psi0);
    end
    
    % All allies have each other initial belief
    for talkerIndex = 1:simConfig.R
        initialBelief = cell(1,5);
        initialBelief{1} = 0; % time
        initialBelief{2} = 1; % number of modes
        initialBelief{3} = 1; % vector of weight of modes
        initialBelief{4} = [initialConditions(talkerIndex).x0; initialConditions(talkerIndex).y0; initialConditions(talkerIndex).psi0];
        initialBelief{5} = diag([var_x0, var_y0, var_psi0]);
        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, initialBelief);
        end
    end  
end

for timeIndex = 2:simConfig.nIterations
    
%     timeIndex
    
    for s = 1:length(setup)
%         s
    
        performPrediction = flags(s,1);
        performLandmarks = flags(s,2);
        performHearing = flags(s,3);
        performAlliesObs = flags(s,4);

        if mod(timeIndex, 1) == 0
            disp(['Monte Carlo: ' num2str(m) ' - ' setup{s} ' - performing estimation, iteration number: ' num2str(timeIndex)]);
        end

        if performPrediction
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO MODIFICATION
%                 TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex);
                
                % WITH MONTE CARLO MODIFICATION
                TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 1);
            end
        end

        if performLandmarks
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 2);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after L, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 6);
            end
        end

        if mostRecentBelief
            % Receiving all allies' most recent belief at all time instants
            for talkerIndex = 1:simConfig.R
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end        
        else    
            % Receive neighbors' self belief according to talkerSchedule
            talkerIndex = talkerSchedule(timeIndex);
            if talkerIndex ~= 0
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);    
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end
        end

        if performHearing
            for r = 1:simConfig.R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 3);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end                
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after H, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 7);
                
            end
        end

        if performAlliesObs
            for r = 1:simConfig. R
%                 % WITHOUT MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex);
                
                % WITH MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex, m);
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 4);
                
%                 if alwaysResample
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%                 end                
                
                Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
                if Neff < 0.6*Np
                    TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
%                     disp(['index ' num2str(timeIndex) ', after A, agent ' num2str(r) ' - resampled']);
                end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 8);
            end
        end
        
        % Important theoretical issue: if assimilating hearing AND camera
        % from the same agent at the same time instant, that must be done
        % jointly, not in two separate steps, because it is a single
        % integral

%         for r = 1:simConfig.R
%             if ~alwaysResample
%                 TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex);
%             end
%         end

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateNewBelief(timeIndex);
        end

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateErrors(timeIndex);
        end

    %     for r = 1:simConfig.R
    %         TeamEst{r,s}.plotParticles(timeIndex)
    % %         TeamEst{r,s}.plotMeanParticles(timeIndex);
    %     end
%         pause;
    
        if mod(timeIndex, 5) == 0
            try
%                 save(['output_temp/' datestr(now, 'yyyymmddTHHMMSS') '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
                save(['output_temp/' datestr(now, 'yyyymmdd-') num2str(estID) '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
            catch
            end
        end
        if timeIndex == simConfig.nIterations
            try
%                 save(['output_final/' datestr(now, 'yyyymmddTHHMMSS') '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
                save(['output_final/' datestr(now, 'yyyymmdd-') num2str(estID) '_' setup{s} '_MC' num2str(m, '%03d') '.mat']);
            catch
            end
        end
    end % s = 1:length(setup)

end % timeIndex = 1:nIterations

end % m = 1:M
