% ID: integer from 1 to 11
% initialConditions: x0, y0, psi0, v_x, v_y, v_psi
% simConfig: time, T, nIterations
% itIndex: index of current iteration
% landmarks: landmark map, generated using generateLandmarkList
% playerList: position of each player
function [realData, observed, odometryData] = entityHumanoidKidSize(ID, initialConditions, landmarks, playerList, simConfig, itIndex)


%Noise variance parameters
pointVariance = (0.01)^2;
bearingVariance = (1.0 * 0.35 * pi/180)^2;
distaceLineVariance = (0.05)^2;
odometryXVariance = (0.03)^2;
odometryYVariance = (0.03)^2;
odometryPsiVariance = (0.01)^2;

%Robot's parameters
visionAngle = pi/6;

%Outputs configuration
showfig = 1;
withpause = 1;
generatePrints = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Simulate humanoid localization system.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
config.showfig = showfig;
config.withpause = withpause;
config.generatePrints = generatePrints;
config.pointvariance = pointVariance;
config.bearingVariance = bearingVariance;
config.distanceLineVariance = distaceLineVariance;

% Using Soccer 3D's variances
config.distanceVariance = (0.05)^2;%0.0965;
config.bearingVariance = (0.035 * pi/180)^2;%0.1225 * (pi / 180)^2;

realData  = zeros(nIterations,3);
realData(1,:) = [x0,y0,psi0];
odometryData = zeros(nIterations,3);
odometryData(1,:) = zeros(1,3);
displace = zeros(nIterations,3);
displaceInLocal = zeros(1,3);


tic
for index=2:nIterations
     if (index==floor(nIterations/2))
%          v_psi = -v_psi;
           realData(index,1) = realData(index-1,1);
           realData(index,2) = realData(index-1,2)-1.2;
           realData(index,3) = realData(index-1,3);
%          v_y = -v_y;
     else
        realData(index,1)=realData(index-1,1)+v_x*T*cos(realData(index-1,3)+v_psi*T/2)-v_y*T*sin(realData(index-1,3)+v_psi*T/2)+randn*sqrt(odometryXVariance);
        realData(index,2)=realData(index-1,2)+v_x*T*sin(realData(index-1,3)+v_psi*T/2)+v_y*T*cos(realData(index-1,3)+v_psi*T/2)+randn*sqrt(odometryYVariance);
        realData(index,3)=realData(index-1,3)+v_psi*T+randn*odometryPsiVariance;
     end
    % === Get Odometry Data === 
    displace(index-1,1) = v_x*T*cos(realData(index-1,3)+v_psi*T/2)-v_y*T*sin(realData(index-1,3)+v_psi*T/2);
    displace(index-1,2) = v_x*T*sin(realData(index-1,3)+v_psi*T/2)+v_y*T*cos(realData(index-1,3)+v_psi*T/2);
    displace(index-1,3) = v_psi*T;
    displaceInLocal(1,1) = displace(index-1,1)*cos(realData(index-1,3)) + displace(index-1,2)*sin(realData(index-1,3));
    displaceInLocal(1,2) = (-1)*displace(index-1,1)*sin(realData(index-1,3)) + displace(index-1,2)*cos(realData(index-1,3));
    displaceInLocal(1,3) = displace(index-1,3);
    odometryData(index,:) = displaceInLocal(1,:);
    % =========================
       
    if(showfig)
        clf('reset');
        drawField(landmarks);
        drawPlayer(realData(index,1),realData(index,2),realData(index,3),visionAngle);
        observedGlobal = observation(realData(index,1),realData(index,2),realData(index,3),visionAngle,landmarks,config);
        observed(index) = prepareObservedData(realData(index, 1),...
            realData(index, 2), realData(index, 3), observedGlobal);
        filename = sprintf('./video/frame%4d',index);
        drawnow;
        if(generatePrints)
            print('-djpg',filename);
        end
        if(withpause)
            pause(T);
        end
    end
end
time=toc;

saveData(realData,observed,odometryData);

end