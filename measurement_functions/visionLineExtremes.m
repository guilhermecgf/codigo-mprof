function points = visionLineExtremes(x, y, psi, visionAngle, lineList, config)

numLines = size(lineList, 1);
points = [];

for i=1:numLines
    extremes = computeLineExtremes(x, y, psi, visionAngle, lineList(i, :), config);
    if ~isempty(extremes)
        if isempty(points)
            points = extremes;
        else
            points = [points; extremes];
        end
    end
end

end

function extremes = computeLineExtremes(x, y, psi, visionAngle, line, config)
    extremes = [];

    minAngle = psi - visionAngle;
    maxAngle = psi + visionAngle;
    
    fovLine1 = [x, y, x + cos(minAngle), y + sin(minAngle)];
    fovLine2 = [x, y, x + cos(maxAngle), y + sin(maxAngle)];
    
    i1 = getLinesIntersection(line, fovLine1);
    i2 = getLinesIntersection(line, fovLine2);
    
    e1 = line(1:2);
    e2 = line(3:4);
    
    fovDirection1 = fovLine1(3:4) - fovLine1(1:2);
    fovDirection1 = fovDirection1 / norm(fovDirection1);
    
    fovDirection2 = fovLine2(3:4) - fovLine2(1:2);
    fovDirection2 = fovDirection2 / norm(fovDirection2);
    
    lineDirection = e2 - e1;
    lineLength = norm(lineDirection);
    lineDirection = lineDirection / lineLength;
    
    innerFov1 = (i1 - [x, y])*fovDirection1';
    innerFov2 = (i2 - [x, y])*fovDirection2';
    innerLine1 = (i1 - e1)*lineDirection';
    innerLine2 = (i2 - e1)*lineDirection';
    
    if innerFov1 >= 0 && (innerLine1 >= 0 && innerLine1 <= lineLength)
        extremes(end+1, :) = i1;
    end
    if innerFov2 >= 0 && (innerLine2 >= 0 && innerLine2 <= lineLength)
        extremes(end+1, :) = i2;
    end
    
    e1Angle = atan2(e1(2) - y, e1(1) - x);
    e2Angle = atan2(e2(2) - y, e2(1) - x);
    
    angleDiff1 = normalizeAngle(psi - e1Angle);
    angleDiff2 = normalizeAngle(psi - e2Angle);
    
    if abs(angleDiff1) <= visionAngle
        extremes(end+1, :) = e1;
    end
    if abs(angleDiff2) <= visionAngle
        extremes(end+1, :) = e2;
    end
    
    if size(extremes, 1) > 2
        extremes = extremes(1:2, :);
    elseif size(extremes, 1) < 2
        extremes = [];
    end
    
%     for i=1:size(extremes, 1)
%         extremes(i, :) = globalToLocalPoint(x, y, psi, extremes(i, :));
%         [distance, bearing] = cartesianToDistanceBearing(extremes(i, 1), extremes(i, 2));
%         [distance, bearing] = addNoise(distance, bearing, config);
%         [xCorrupt, yCorrupt] = distanceBearingToCartesian(distance, bearing);
%         extremes(i, :) = [xCorrupt, yCorrupt];
%     end
end

function intersection = getLinesIntersection(l1, l2)
    dx1 = l1(3) - l1(1);
    dy1 = l1(4) - l1(2);
    dx2 = l2(3) - l2(1);
    dy2 = l2(4) - l2(2);
    b1 = dy1 * l1(1) - dx1 * l1(2);
    b2 = dy2 * l2(1) - dx2 * l2(2);
    A = [dy1, -dx1; dy2, -dx2];
    b = [b1; b2];
    if abs(det(A)) < 10^-3
        intersection = [];
    else
        intersection = linsolve(A, b)';
    end
end