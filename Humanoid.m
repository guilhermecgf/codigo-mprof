% ID: integer from 1 to 11
% initialConditions: x0, y0, psi0, v_x, v_y, v_psi
% simConfig: time, T, nIterations
% itIndex: index of current iteration
% landmarks: landmark map, generated using generateLandmarkList
% playerList: position of each player
classdef Humanoid
    properties
        
        % number of the robot, from 1 to 11
        ID
        
        % positions (x0, y0, psi0) and velocities (v_x, v_y, v_psi)
        x0, y0, psi0, v_x, v_y, v_psi
        
        %Noise variance parameters
        rangeVariancePar, bearingVariance
        DoAVariance
        odometryXVariance, odometryYVariance, odometryPsiVariance
        odometryCovariance
        %distanceLineVariance, distanceVariance

        %Robot's parameters
        visionAngle
        
        % storing self simulation data
        realData, odometryData
        displace, displaceInLocal
        u
        
        % landmark map, with field properties, is loaded on each robot
        landmarks
        
        % number of measurements taken on each measuring step.
        % used in the for evaluating the algorithm with multiple measurements
        % for the same trajectory
        M
        
        % storing data from hearing allies: 
        heardGlobal, heardLocal %time received, talker ID, measured bearing
        
        % storing data from observation
        observedGlobal, measurementCamera
        
        % "config" structs, used for time, noise, number of robots and other simulation parameters
        time, T, nIterations, R, config
        
        % attributes used for estimation step
        Np % number of particles for representing the pose
        beliefsGMM % cell{R,5}, with time, modes, mode weights, means, covariances
%         beliefSelfParticles % double(4,Np), with particle weight and state (x, y, psi)
        
        % maximum velocities in x, y and psi. Used for extrapolation
        vXYMax, vPsiMax
        
        mess_B_Particles, mess_P_Particles, mess_L_Particles, mess_H_Particles, mess_A_Particles, mess_Trans_Particles
        % observations and odometry are declared for the simulation step
        % landmarks map is being passed to the other methods, but could be
        % an attribute of Humanoid...
        
        % factors for inflating the covariance, only in the particle filter
        % (not for generating the scenario)
        inflateState, inflateRho, inflatePhiCam, inflatePhiDoA
        
        estimatedState, estimationError
        
        beliefHistory      
    end
    
    methods
        function self = Humanoid(ID, initialConditions)
            self.ID = ID;
            self.x0 = initialConditions.x0;
            self.y0 = initialConditions.y0;
            self.psi0 = initialConditions.psi0;
            self.v_x = initialConditions.v_x;
            self.v_y = initialConditions.v_y;
            self.v_psi = initialConditions.v_psi;
            
            flag_noisy = true;
            if flag_noisy
                aux = 1;
            else
                aux = 0;
            end
            
            %Noise variance parameters
%             self.rangeVariancePar = aux*1e-1*0.0965;
%             self.bearingVariance = aux*1e-1*0.1225; % horizontal = 0.1225 antes: (0.35*pi/180)^2. Por enquanto, bem elevada pois tratar o ru�do de �ngulo � dif�cil. Ent�o vamos considerar bem disperso para n�o quebrar o filtro
%             self.DoAVariance = aux*1e-1*0.0888; % similar to the bearing coming from the camera
%             self.odometryXVariance = aux*10*(0.03)^2;
%             self.odometryYVariance = aux*10*(0.03)^2;
%             self.odometryPsiVariance = aux*10*(0.01)^2;

%             self.rangeVariancePar = aux * (0.0965/10)^2;
%             self.bearingVariance = aux * 0.1480^2; % horizontal = 0.1225 antes: (0.35*pi/180)^2. Por enquanto, bem elevada pois tratar o ru�do de �ngulo � dif�cil. Ent�o vamos considerar bem disperso para n�o quebrar o filtro
%             self.DoAVariance = aux * 0.1480^2; % similar to the bearing coming from the camera
%             self.odometryCovariance = aux * 10 * [0.03, 0, 0.01;
%                                                   0, 0.03, 0.01;
%                                                   0.01, 0.01, 0.04].^2;
            
%             self.rangeVariancePar = aux * 0.0011^2*5.4;
            self.rangeVariancePar = aux * 0.0011^2*1;
            self.bearingVariance = aux * 0.0048^2;
            self.DoAVariance = aux * 0.0048^2 ;
            self.odometryCovariance = aux * 1e-3 * [0.0091, -0.0007, 0.0089;
                                                    -0.0007, 0.0087, 0.0099;
                                                    0.0089, 0.0099, 0.3575];

            %Robot's parameters
            self.visionAngle = pi/6;
        end
        
        % ------------------------------
        % METHODS FOR GENERATING THE SCENARIO
        function self = simulationSetup(self, simConfig, outputConfig)
            
            % Setting time simulation parameters for the robot
            self.time = simConfig.time;
            self.T = simConfig.T;
            self.nIterations = simConfig.nIterations;
            self.R = simConfig.R;
            
            self.M = simConfig.M;
            
            % Setting simulation data parameters for the robot
            self.realData = zeros(self.nIterations,3);
            
%             self.realData(1,:) = [self.x0, self.y0, self.psi0];

            % Adding no noise to the initial positions
            self.realData(1,1) = self.x0 + 0*0.1*randn;
            self.realData(1,2) = self.y0 + 0*0.1*randn;
            self.realData(1,3) = self.psi0 + 0*0.01*randn;
            self.realData(1,3) = wrapAngle(self.realData(1,3));
            
%             % WITHOUT MONTE CARLO MODIFICATION
%             self.odometryData = zeros(self.nIterations,3);
%             self.odometryData(1,:) = zeros(1,3);

            % WITH MONTE CARLO MODIFICATION - Preserved, for scenario
            % generation
            self.odometryData = zeros(self.nIterations,3,self.M);
            self.odometryData(1,:,1) = zeros(1,3);
            
            self.displace = zeros(self.nIterations,3);
            self.displaceInLocal = zeros(1,3);
            
            self.u = zeros(self.nIterations, 3);
            
            % Loading landmark map on each robot
            self.landmarks = simConfig.landmarks;
            
            % Setting matrix for storing bearing data from hearing other players
%             self.heardGlobal = zeros(self.nIterations, 3);
%             self.heardLocal = zeros(self.nIterations, 3);
            
            % WITH MONTE CARLO MODIFICATION - Preserved, for scenario
            % generation
            self.heardGlobal = cell(self.nIterations, 3);
            self.heardLocal = cell(self.nIterations, 3);
            
            % Cell array for storing GMM beliefs, from allies and self.
            % Row i corresponds to agent ID i. On each row:
            % {i,1} (double) time index of received GMM; for self, time index when GMM
            % parameters were calculated from Expectation-Maximization
            % {i,2} (double) number of modes
            % {i,3} (double(1,modes)) weight of modes
            % {i,4} (double(3, modes)) mean vector of modes
            % {i,5} (double(3,3,modes)) covariance matrix of modes
            self.beliefsGMM = cell(self.R, 5);
            % add index dimension here, then modify the corresponding parts
            % of the code. Remember to propagate the beliefs to every new
            % index (probably in receive belief method: update the
            % received, extrapolate the others)
            
            % Setting cell array for storing data from observations, in
            % global coordinates (for plotting) and local coordinates
            % For now, it is stored in a cell array because of the
            % heterogenous nature of the observations
            self.observedGlobal = cell(self.nIterations, 1);
            self.measurementCamera = cell(self.nIterations, 1);
            
            % Setting other configuration parameters for the robot
            self.config.showfig = outputConfig.showfig;
            self.config.withpause = outputConfig.withpause;
            self.config.generatePrints = outputConfig.generatePrints;
            self.config.rangeVariancePar = self.rangeVariancePar;
            self.config.bearingVariance = self.bearingVariance;
        end
        
        function self = move(self, index)
            
            % === Calculating the new position: uses old position,
            % displacement and noise.
            
            % === Get Odometry Data, or displacement in local coordinates === 
            % usar fStateDyn
            self.u(index,:) = [self.v_x * self.T, self.v_y * self.T, self.v_psi * self.T];

%             self.displace(index-1,1) = self.u(index, 1) * cos( self.realData(index-1,3) + self.u(index, 3)/2 ) ...
%                 - self.u(index, 2) * sin( self.realData(index-1,3) + self.u(index, 3)/2);
%             
%             self.displace(index-1,2) = self.u(index, 1) * sin( self.realData(index-1,3) + self.u(index, 3)/2 ) ...
%                 + self.u(index, 2) * cos( self.realData(index-1,3) + self.u(index, 3)/2);
%             
%             self.displace(index-1,3) = self.u(index, 3);
            
            self.displace(index-1, :) = reshape( displacementInGlobal( self.realData(index-1,:), self.u(index, :) ) , [1,3] ) ;            
                                    
%             % WITHOUT MONTE CARLO MODIFICATION
% %             self.odometryData(index,:) = self.displaceInLocal(1,:); % improductive step
%             self.odometryData(index,:) = self.u(index,:); % without noise; 
%             self.odometryData(index,:) = self.u(index,:) + sqrt([self.odometryXVariance, self.odometryYVariance, self.odometryPsiVariance]).*randn([1,3]); % with noise, that is, considering being measured from an inertial
            
            % WITH MONTE CARLO MODIFICATION - Preserved, for scenario
            % generation
            for m = 1:self.M
%                 % with noise, that is, considering being measured from an inertial
%                 self.odometryData(index,:,m) = self.u(index,:) + sqrt([self.odometryXVariance, self.odometryYVariance, self.odometryPsiVariance]).*randn([1,3]);
                
                % without noise, that is, considering the control sequence is directly fed into the filter
                self.odometryData(index,:,m) = self.u(index,:);
            end
            % odometryData, not u, must be used in the prediction step of the filter
            
            % === New position, using displacement in global coordinates
            
            
%             self.realData(index,:) = self.realData(index-1,:) + self.displace(index-1,:) + ... 
%                 mvnrnd(zeros(1,3), self.odometryCovariance);
            
%             self.realData(index,:) = reshape( fStateDyn(self.realData(index-1,:).', self.displace(index-1,:)), [1,3] ) + ...
%                 mvnrnd(zeros(1,3), self.odometryCovariance);
            
            self.realData(index,:) = reshape( fStateDyn(self.realData(index-1,:).', self.u(index,:)), [1,3] ) + ...
                mvnrnd(zeros(1,3), self.odometryCovariance);
            
            self.realData(index,3) = wrapAngle(self.realData(index,3));
            
        end
        
        function self = hear(self, index, talkerIndex, playerMap)
            
%             % WITHOUT MULTIPLE MEASUREMENTS, FOR MONTE CARLO
%             
%             % measuring the Direction of Arrival of the incoming
%             % information, in local coordinates of the receiving robot.
%             % globalToLocalPoint considers the listener's pose.
%             % This is what the robot will actually perceive
%             
% %             talkerLocal = globalToLocalPoint(self.realData(index, 1), self.realData(index, 2), self.realData(index, 3), playerMap(talkerIndex, 1:2, index));            
% %             DoA_local = atan2( talkerLocal(2)-0, talkerLocal(1)-0 ) + sqrt(self.DoAVariance)*randn;
%             
%             DoA_local = hPhi(self.realData(index,:), playerMap(talkerIndex, :, index)) + sqrt(self.DoAVariance)*randn;            
%             self.heardLocal(index, :) = [index*self.T, talkerIndex, DoA_local];
%             
%             % Converting DoA measurement, made in local coordinates, to
%             % global. It will be used only for plotting.
%             % Resulting angle must be within (-pi, +pi] interval!!
%             DoA_global = DoA_local + self.realData(index, 3);
%             while(DoA_global > pi)
%                 DoA_global = DoA_global - 2*pi;
%             end
%             while(DoA_global <= -pi)
%                 DoA_global = DoA_global + 2*pi;
%             end
%             
%             self.heardGlobal(index, :) = [index*self.T, talkerIndex, DoA_global];
            
            % WITH MULTIPLE MEASUREMENTS, FOR MONTE CARLO - Preserved, for scenario
            % generation
            
            DoA_local = hPhi(self.realData(index,:), playerMap(talkerIndex, :, index)) + sqrt(self.DoAVariance)*randn(1, self.M);
            DoA_local = wrapAngle(DoA_local);
            self.heardLocal{index,1} = index*self.T;
            self.heardLocal{index,2} = talkerIndex;
            self.heardLocal{index,3} = DoA_local;
            
            DoA_global = DoA_local + self.realData(index, 3);
            DoA_global = wrapAngle(DoA_global);
            
%             % substituir por fun��o wrapAngle
%             % DoA_global = wrapAngle(DoA_global);
%             for i = 1:self.M
%                 while(DoA_global(i) > pi)
%                     DoA_global(i) = DoA_global(i) - 2*pi;
%                 end
%                 while(DoA_global(i) <= -pi)
%                     DoA_global(i) = DoA_global(i) + 2*pi;
%                 end                
%             end
            self.heardGlobal{index,1} = index*self.T;
            self.heardGlobal{index,2} = talkerIndex;
            self.heardGlobal{index,3} = DoA_global;            
            
        end
        
        function self = observe(self, index, playerMap)
            
            currPlayerPositions = playerMap(:,1:2,index); % method "run" has to use index-1, whereas "observe" can use index
            
%             % WITHOUT ADAPTATION FOR MONTE CARLO
%             [self.observedGlobal{index}, self.measurementCamera{index}] = observation(...
%                 self.realData(index,1), self.realData(index,2), self.realData(index,3), ...
%                 self.visionAngle, self.landmarks, currPlayerPositions, self.config);

            % WITH ADAPTATION FOR MONTE CARLO - Preserved, for scenario
            % generation       
            [self.observedGlobal{index}, self.measurementCamera{index}] = observation(...
                self.realData(index,1), self.realData(index,2), self.realData(index,3), ...
                self.visionAngle, self.landmarks, currPlayerPositions, self.config, self.M);
 
        end
        
        function plotPlayerInfo(self, index)
            
            drawField(self.landmarks, self.ID);
            drawPlayer(self.realData(index,1), self.realData(index,2), self.realData(index,3), self.visionAngle, self.ID);
            
            % draw DoA line, only if there is measurement
            heardGlobalVec = zeros(1,3);
            if ~isempty(self.heardGlobal{index,1})
                heardGlobalVec(1) = self.heardGlobal{index,1}; 
                heardGlobalVec(2) = self.heardGlobal{index,2}; 
                heardGlobalVec(3) = self.heardGlobal{index,3}; 
            end
            
%             if self.heardGlobal(index, 2) ~= 0 % second column corresponds to ally's ID
%                 x_vec_global = [ self.realData(index, 1), self.realData(index, 1) + 10*cos(self.heardGlobal(index, 3)) ];
%                 y_vec_global = [ self.realData(index, 2), self.realData(index, 2) + 10*sin(self.heardGlobal(index, 3)) ];
%                 plot(x_vec_global, y_vec_global, 'k:');
%             end
            
            if heardGlobalVec(2) ~= 0 % second column corresponds to ally's ID
                x_vec_global = [ self.realData(index, 1), self.realData(index, 1) + 10*cos(heardGlobalVec(3)) ];
                y_vec_global = [ self.realData(index, 2), self.realData(index, 2) + 10*sin(heardGlobalVec(3)) ];
                plot(x_vec_global, y_vec_global, 'k:');
            end
            
            % draw observed features, only if there is measurement
            
            % if time allows, simplify to function:
            % drawFeatures(self.observedGlobal{index}.feature, plot options, (opt) self.observedGlobal{index}.featureID, (opt) tags);
            pointSize = 50;
            
            if ~isempty(self.observedGlobal{index})
            
                if ~isempty(self.observedGlobal{index}.players)
                    plot(self.observedGlobal{index}.players(:,1), self.observedGlobal{index}.players(:,2),'hk','MarkerSize',pointSize);
                    plot(self.observedGlobal{index}.players(:,1), self.observedGlobal{index}.players(:,2),'hk','MarkerSize',3);
                    %tags = {'P1', 'P2', 'P3', 'P4'};
                    tags = cell(self.R,1);
                    for r = 1:self.R
                        tags{r} = ['P' num2str(r)];
                    end
                    
                    for i = 1:length(self.observedGlobal{index}.players(:,1))
                        text(self.observedGlobal{index}.players(i,1), self.observedGlobal{index}.players(i,2), tags{self.observedGlobal{index}.playersID(i)});
                    end
                end

                if ~isempty(self.observedGlobal{index}.goals)
                    plot(self.observedGlobal{index}.goals(:,1), self.observedGlobal{index}.goals(:,2),'sr','MarkerSize',pointSize);
                    plot(self.observedGlobal{index}.goals(:,1), self.observedGlobal{index}.goals(:,2),'sr','MarkerSize',3);
                    tags = {'G1', 'G2', 'G3', 'G4'}; % {'G1L', 'G2L', 'G1R', 'G2R'};
                    for i = 1:length(self.observedGlobal{index}.goals(:,1))
                        text(self.observedGlobal{index}.goals(i,1), self.observedGlobal{index}.goals(i,2), tags{self.observedGlobal{index}.goalsID(i)});
                    end
                end

                if ~isempty(self.observedGlobal{index}.corners)
                    plot(self.observedGlobal{index}.corners(:,1), self.observedGlobal{index}.corners(:,2),'sg','MarkerSize',pointSize);
                    plot(self.observedGlobal{index}.corners(:,1), self.observedGlobal{index}.corners(:,2),'sg','MarkerSize',3);
                    tags = {'C1', 'C2', 'C3', 'C4'}; % {'F1L', 'F2L', 'F1R', 'F2R'};
                    for i = 1:length(self.observedGlobal{index}.corners(:,1))
                        text(self.observedGlobal{index}.corners(i,1), self.observedGlobal{index}.corners(i,2), tags{self.observedGlobal{index}.cornersID(i)});
                    end
                end
                
            end            
        end
        
        % ------------------------------
        % METHODS FOR ESTIMATION
        % Humanoid class has a method to perform estimations of self
        % position.
        
        % methods used for the Message-Passing, GMM/SMC filter
%         function self = initializeSMC(self, Np, x0_var, y0_var, psi0_var)
        function self = initializeSMC(self, Np, Sigma0)
            self.Np = Np;
            
            self.estimatedState = zeros(self.nIterations, 3);
            self.estimationError = zeros(3, self.nIterations);
            
            % drawing Np particles from initial belief
            self.mess_B_Particles = zeros(4, self.Np);
            self.mess_B_Particles(1,:) = 1/self.Np*ones(1, self.Np);
            
%             x0_var = Sigma0(1,1); y0_var = Sigma0(2,2); psi0_var = Sigma0(3,3);
%             self.mess_B_Particles(2,:) = self.x0 + sqrt(x0_var)*randn(1, self.Np);
%             self.mess_B_Particles(3,:) = self.y0 + sqrt(y0_var)*randn(1, self.Np);
%             self.mess_B_Particles(4,:) = self.psi0 + sqrt(psi0_var)*randn(1, self.Np);

            state0 = [self.x0; self.y0; self.psi0] + mvnrnd([0,0,0], Sigma0).';
            state0(3) = wrapAngle(state0(3));
            
            for p = 1:self.Np
%                 noise = mvnrnd(zeros(1,3), Sigma0).';
%                 self.mess_B_Particles(2:4,p) = [self.x0; self.y0; self.psi0] + noise;
                self.mess_B_Particles(2:4,p) = mvnrnd(state0, Sigma0).';
                
                % NOT WRAPPING ANGLE WHEN PARTICLES ARE DRAWN!
                self.mess_B_Particles(4,p) = wrapAngle(self.mess_B_Particles(4,p));
            end
            
            self.mess_Trans_Particles = self.mess_B_Particles;
            
%             % DEBUG ERRO DE WRAP, INIT PERTO DE 180 GRAUS
%             disp('Particulas em initializeSMC');
%             disp(self.mess_Trans_Particles);
%             % FIM DEBUG
            
            self.beliefHistory = cell(1,5);
            self.beliefHistory{1} = zeros(self.nIterations, 1); % time instants
            %self.beliefHistory{2} = zeros(self.nIterations, 1); % array of number modes
            %self.beliefHistory{3} = zeros(self.nIterations, 1); % array of weights
            self.beliefHistory{4} = zeros(self.nIterations, 3); % array of mean vectors
            self.beliefHistory{5} = zeros(self.nIterations, 3, 3); % array of covariance matrices
            
            % for extrapolating
%             self.vXYMax = 0.2;
%             self.vPsiMax = 0.2;
            
            self.vXYMax = 0.7 * sqrt(0.9^2 + 0.4^2);
            self.vPsiMax = 0.66 * 5;
            
            % for artificially inflating the covariance of the sensors and
            % state model
            self.inflateState = [20, 20, 1;
                                 20, 20, 1;
                                 1, 1, 1];
            self.inflateRho = 100;
            self.inflatePhiCam = 50;
            self.inflatePhiDoA = 20;
        end
        
        
        % Processing is too slow and has too large storage if all monte
        % carlo data is received
%         function self = predictSelfPosition(self, index, m)
        function self = predictSelfPosition(self, index)
            
%             % DEBUG ERRO DE WRAP, INIT PERTO DE 180 GRAUS
%             disp('Particulas input predictSelfPosition');
%             disp(self.mess_Trans_Particles);
%             % FIM DEBUG
            
            input_Particles = self.mess_Trans_Particles;
            
            % here, we are applying the SMC approach to calculate the
            % integral of the product of two Gaussians,although it has a
            % closed-form analytic expression
            for p = 1:self.Np
                % selecting one of the previous particles/modes
                mode = randi([1, self.Np]);
                self.mess_P_Particles(1,p) = 1/self.Np;
                
                % WITHOUT MONTE CARLO MODIFICATION - return to old way
                mu_n_nprev = fStateDyn(input_Particles(2:4,mode), self.odometryData(index, :));
                
                % NOT WRAPPING ANGLE WHEN PARTICLES ARE DRAWN!
                mu_n_nprev(3) = wrapAngle(mu_n_nprev(3));
                
%                 % WITH MONTE CARLO MODIFICATION
%                 mu_n_nprev = fStateDyn(input_Particles(2:4,mode), self.odometryData(index, :, m));

%                 self.mess_P_Particles(2:4,p) = mvnrnd(mu_n_nprev.', diag([self.odometryXVariance, self.odometryYVariance, self.odometryPsiVariance]) );
%                 if p == 1
%                     p
%                     input_Particles(2:4, mode)
%                     odoData = self.odometryData(index,:); odoData
%                     stateReal = self.realData(index,:); stateReal
%                     mu_n_nprev
%                     a = mvnrnd(mu_n_nprev.', self.odometryCovariance )
%                     self.mess_P_Particles(2:4,p) = a;
%                     b = self.mess_P_Particles(2:4,p);
%                     b
%                 end
                self.mess_P_Particles(2:4,p) = mvnrnd(mu_n_nprev.', self.inflateState.*self.odometryCovariance );
%                 self.mess_P_Particles(2:4,p) = mvnrnd(mu_n_nprev.', self.odometryCovariance );

                % NOT WRAPPING ANGLE WHEN PARTICLES ARE DRAWN!
                self.mess_P_Particles(4,p) = wrapAngle(self.mess_P_Particles(4,p));
            end
            
            self.mess_Trans_Particles = self.mess_P_Particles;
            
%             % DEBUG ERRO DE WRAP, INIT PERTO DE 180 GRAUS
%             disp('Particulas input predictSelfPosition');
%             disp(self.mess_Trans_Particles);
%             % FIM DEBUG
        end
        
        
        % Processing is too slow and has too large storage if all monte
        % carlo data is received        
        function self = assimilateLandmarks(self, index)
%         function self = assimilateLandmarks(self, index, m)
            
            input_Particles = self.mess_Trans_Particles;
%             self.mess_L_Particles(2:4,:) = self.mess_Trans_Particles(2:4,:);
            self.mess_L_Particles = self.mess_Trans_Particles;
            
            if ~isempty(self.measurementCamera{index})
                goals = self.measurementCamera{index}.goals;
                goalsID = self.measurementCamera{index}.goalsID;

                corners = self.measurementCamera{index}.corners;
                cornersID = self.measurementCamera{index}.cornersID;
                
%                 goals = self.observedGlobal{index}.goals;
%                 goalsID = self.observedGlobal{index}.goalsID;
% 
%                 corners = self.observedGlobal{index}.corners;
%                 cornersID = self.observedGlobal{index}.cornersID;
                
                % here, the true bearing is restricted to (-pi,pi]. The
                % noise is applied in the sequence
                % (this avoids that the noise seems much more powerful than
                % it is in the case of a wrapping).
                % (functions: observe and hPhi)
                %
                % Same for the measurement of DoA (in self.hear())
                %
                % Thus, the mean of the likleihood must be in local
                % coordinates (agent's pose), equal to the relative pose of
                % the observed target.
                % Apparently, this is done in the pObs function (since it
                % calls h_phi, which will measure in local coordinates
                
%                 totalLandmarks = size(goals,1) + size(corners,1);
%                 disp(['Robot ' num2str(self.ID) '. Total landmarks = ' num2str(totalLandmarks) '. Np = ' num2str(self.Np) '. Weights: ' ]);
                factorLogVec = zeros(1,self.Np);
                for p = 1:self.Np
%                     auxProd = 1;
                    auxProdLog = 0;

                    for i = 1:size(goals,1)
                        xl = [self.landmarks.goalList(goalsID(i),:), 0];
                        obs_n_r_l = goals(i,:).';
%                         obs_n_r_l = goals(i,:,m).';
                        
%                         factor = pObs(obs_n_r_l, input_Particles(2:4,p), xl, 0, self.rangeVariancePar, 0, self.bearingVariance);
                        factorLog = pObsLog(obs_n_r_l, input_Particles(2:4,p), xl, 0, self.inflateRho*self.rangeVariancePar, 0, self.inflatePhiCam*self.bearingVariance);
%                         if p == 1
%                             disp(['particle ' num2str(p) '. Likelihood = ' num2str(factor)]);
%                         end
%                         auxProd = auxProd*factor;
                        auxProdLog = auxProdLog + factorLog;
                    end

                    for i = 1:size(corners,1)
                        xl = [self.landmarks.cornerList(cornersID(i),:), 0];
                        obs_n_r_l = corners(i,:).';
%                         obs_n_r_l = corners(i,:,m).';
                        
                        % aqui, construir uma fun��o que j� calcule o
                        % loglikelihood. N�o � simplesmente tirar o log do
                        % que vem
%                         factor = pObs(obs_n_r_l, input_Particles(2:4,p), xl, 0, self.rangeVariancePar, 0, self.bearingVariance);
                        factorLog = pObsLog(obs_n_r_l, input_Particles(2:4,p), xl, 0, self.inflateRho*self.rangeVariancePar, 0, self.inflatePhiCam*self.bearingVariance);
%                         if p == 1
%                             disp(['particle ' num2str(p) '. Likelihood = ' num2str(factor)]);
%                         end
%                         auxProd = auxProd*factor;
                        auxProdLog = auxProdLog + factorLog;
                    end
                    % ISSO AQUI PODE DAR PAU
                    % TRABALHAR COM LOG LIKELIHOOD
                    
                    % DESCARTAR MEDIDA? QUAL O EFEITO? UMA MEDIDA COM POUCO
                    % RU�DO, SE AS PART�CULAS ESTIVEREM RUINS, SER�
                    % DESCARTADA
                    
                    % COVARIANCE INFLATION: DELIBERADAMENTE AUMENTAR,
                    % APENAS NA FILTRAGEM, O VARIANCIA NAS ASSIMILA��ES E/OU
                    % NA PREDI��O
                    
                    % 1e-10 est� muito alto
%                     auxProd = max(auxProd, 1e-10);

%                     disp(['auxProd = ' num2str(auxProd)]);
%                     self.mess_L_Particles(1,p) = input_Particles(1,p)*auxProd;
                    factorLogVec(p) = auxProdLog;
%                     disp(['final weight: ' num2str(self.mess_L_Particles(1,p))]);
                end
                
                for p = 1:self.Np
                    self.mess_L_Particles(1,p) = input_Particles(1,p) * 1/sum( exp(factorLogVec - factorLogVec(p)) );
                end
                
                if sum(self.mess_L_Particles(1,:)) == 0
                    disp(['sum of weights zero in landmark assimilation.']);                
                    totalLandmarks = size(goals,1) + size(corners,1);
                    disp(['Robot ' num2str(self.ID) '. Total landmarks = ' num2str(totalLandmarks) '. Np = ' num2str(self.Np) '. Weights: ' ]);
                    self.mess_L_Particles(1,:) = 1/self.Np*ones(1,self.Np);
                else
                    self.mess_L_Particles(1,:) = self.mess_L_Particles(1,:)/sum(self.mess_L_Particles(1,:));
                end
                
            end
            
            self.mess_Trans_Particles = self.mess_L_Particles;
        end
        
        
        function [mu, cov] = sampleStatistics(self, particles)
            
            Npart = size(particles,2);
            
            % sample mean mean of particles
%             particles(1,:).'
            % before summing, wrapping all headings to that of the first
            % particle
            % after summing, wrap back to (-pi, +pi]
            % keep it wrapped to the first heading when calculating the
            % covariance matrix            
            particles(4,:) = wrapAngle(particles(4,:), particles(4,1));
            mu_wrapped_local = particles(2:4,:) * particles(1,:).';
            
            aux_wrapped_local = particles(2:4,:) - mu_wrapped_local*ones(1,Npart);
            
            % limiting covariance matrix to 1e-8, which is extremely
            % accurate for our purposes (micrometers) and avoids numerical
            % precision errors            
            cov = aux_wrapped_local * diag(particles(1,:)) * aux_wrapped_local.' + ...
                  1e-16*eye(3);
            
            % wrapping back the returned mean to (-pi, +pi], centered in
            % zero
            mu = mu_wrapped_local;
            mu(3) = wrapAngle(mu_wrapped_local(3), 0);              
              
            % assuring symmetry of covariance matrix
            cov = (cov + cov.')/2;
                       
%             mu = particles(2:4,:)*(particles(1,:)).';
%             mu(3) = wrapAngle(mu(3));
%             
%             % sample covariance
%             aux = particles(2:4,:) - mu*ones(1,Npart);
%             aux(3) = wrapAngle(aux(3));
%             
%             % limiting covariance matrix to 1e-8, which is extremely
%             % accurate for our purposes (micrometers) and avoids numerical
%             % precision errors
%             cov = aux * diag(particles(1,:)) * aux.' + 1e-16*eye(3);
%             
%             % assuring symmetry of covariance matrix
%             cov = (cov + cov.')/2;
            
        end
        
        function Neff = effectiveParticles(self, particles)
            weights = particles(1,:);
            Neff = 1/sum(weights.^2);            
        end           
        
        
        function [beliefGMM, self] = generateGMM(self, index)
            
            input_Particles = self.mess_Trans_Particles;
            
%             % weighted mean of particles
%             mean_Particles = input_Particles(2:4,:)*(input_Particles(1,:)).';
%             
%             % weighted mean for covariance matrix of particles
%             aux = input_Particles(2:4,:) - mean_Particles*ones(1, self.Np);
%             
%             % numerical approach for avoiding ill-conditioned covariance
%             % matrices: adding 1e-8*I, to the eigenvalues of
%             % covariance_Particles will not be smaller than 1e-8.
%             covariance_Particles = aux*diag(input_Particles(1,:))*aux.' + 1e-8*eye(3);
%             
%             % assuring symmetry of covariance matrix
%             covariance_Particles = (covariance_Particles + covariance_Particles.')/2;
            
            [mean_Particles, covariance_Particles] = self.sampleStatistics(input_Particles);
            
            % allocating to a monomodal Gaussian
            beliefGMM = cell(1,5);
            beliefGMM{1} = index * self.T;
            beliefGMM{2} = 1;
            beliefGMM{3} = 1;
            beliefGMM{4} = mean_Particles;
            beliefGMM{5} = covariance_Particles;
            
            self.beliefHistory{1}(index) = index*self.T;
            self.beliefHistory{4}(index,:) = mean_Particles;
            self.beliefHistory{5}(index,:,:) = covariance_Particles;
            
%             % call expectation-maximization
%             beliefGMM = 0;
        end
        
        
        function self = receiveBelief(self, index, talkerID, belief)
            for content = 1:5
                self.beliefsGMM{talkerID, content} = belief{content};
            end
        end
        
        function self = assimilateDoA(self, index)
%         function self = assimilateDoA(self, index, m)
            
            input_Particles = self.mess_Trans_Particles;
%             self.mess_H_Particles(2:4,:) = self.mess_Trans_Particles(2:4,:);
            self.mess_H_Particles = self.mess_Trans_Particles;
            
%             if self.heardLocal(index, 2) ~= 0
            if self.heardLocal{index, 2} ~= 0
                playerID = self.heardLocal{index, 2};
                DoA = self.heardLocal{index, 3};

%                 playerID = self.heardLocal(index, 2);
%                 DoA = self.heardLocal(index, 3);
                
%                 playerID = self.heardLocal{index, 2};
%                 DoA_array = self.heardLocal{index, 3};
%                 DoA = DoA_array(m);
                
                % verify if it has received belief of that ally
                timestampBeliefOfAlly = self.beliefsGMM{playerID,1};
                if isempty(timestampBeliefOfAlly ~= index*self.T)
                    warning('algorithm error: should have assimilated the belief that was just transmitted!');
                end
                
                % Sampling particles for the position of the target
                ally_particles = zeros(4, self.Np);
                ally_particles(1,:) = 1/self.Np;

                
                dt = (index - self.beliefsGMM{playerID,1})*self.T;
                covExtrapolation = diag( [ self.vXYMax*dt, self.vXYMax*dt, self.vPsiMax*dt ].^2 );
                
                try                    
                    ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, self.beliefsGMM{playerID,5} + covExtrapolation, self.Np ).';
                    ally_particles(4,:) = wrapAngle(ally_particles(4,:));
                    
%                     ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, self.beliefsGMM{playerID,5}, self.Np ).';

                catch
                    disp('entrou no catch')
%                     disp('cov matrix from belief');
%                     self.beliefsGMM{playerID,5}
%                     disp('eigenvalues');
%                     eig(self.beliefsGMM{playerID,5});
%                     disp('sampling from 0.1*eye(3) covariance');
%                     ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, 0.1*eye(3), self.Np ).';
                end
                
                % here, the true bearing is restricted to (-pi,pi]. The
                % noise is applied in the sequence
                % (this avoids that the noise seems much more powerful than
                % it is in the case of a wrapping).
                % (functions: observe and hPhi)
                %
                % Same for the measurement of DoA (in self.hear())
                %
                % Thus, the mean of the likleihood must be in local
                % coordinates (agent's pose), equal to the relative pose of
                % the observed target.
                % Apparently, this is done in the pObs function (since it
                % calls h_phi, which will measure in local coordinates
                
                factorLogVec = zeros(1, self.Np);
                for p = 1:self.Np
%                     factor_vec = zeros(1, self.Np);
                    factorLogMarginalVec = zeros(1, self.Np);
                    for p_ally = 1:self.Np
%                         factor_vec(p_ally) = pPhi(DoA, input_Particles(2:4,p), ally_particles(2:4,p_ally), 0, self.DoAVariance);
                        factorLogMarginalVec(p_ally) = pPhiLog(DoA, input_Particles(2:4,p), ally_particles(2:4,p_ally), 0, self.inflatePhiDoA*self.DoAVariance);
                    end
                    
                    % lambda_p = sum( w_p_ally*lambda_p_ally )
                    % w_p = w_p_anterior * lambda_p
                    % normalizo w_p
                    
                    % lambda_p = sum( w_p_ally * exp(loglikelihood_p_ally) )
                    
                    % Aqui, recebo v�rios expoentes, p(k). Preciso fazer
                    % sum( exp( p(k) ) ). Para isso, acho o m�ximo dentre
                    % os p(k) e a soma se torna:
                    % exp(p(k)_max) * (1 + sum( exp( p(k)-p(k)_max ) ) = exp(p(k)_max) * S.
                    % efetuo S, aplicando as exponenciais. (Se um termo
                    % zerar, n�o tem problema. � pq ele � muito menor em
                    % rela��o ao p(k) m�ximo)
                    % da�, meu fator se torna:
                    % exp(p(k)_max) * S = exp( p(k)_max + log(S) )
                    
                    max_exponent = max(factorLogMarginalVec);
                    S = sum(  exp(factorLogMarginalVec - max_exponent) * ally_particles(1,:).'  );
                    factorLog = max_exponent + log(S);
                    factorLogVec(p) = factorLog;
                    
%                     factor = ally_particles(1,:)*factor_vec.';
                    
                    % MAX 1E-10 � UM LIMITE ALTO
                  
%                     factor = max(factor, 1e-10);                    
                    
%                     self.mess_H_Particles(1,p) = input_Particles(1,p)*factor;
                end
                
                for p = 1:self.Np
                    self.mess_H_Particles(1,p) = input_Particles(1,p) * 1/sum( exp(factorLogVec - factorLogVec(p)) );
                end
                
                % TESTAR LOG-LIKELIHOOD

                if sum(self.mess_H_Particles(1,:)) == 0
                    disp(['Robot ' num2str(self.ID) ': sum of weights zero in hearing assimilation']);
                    self.mess_H_Particles(1,:) = 1/self.Np*ones(1,self.Np);
                else
                    self.mess_H_Particles(1,:) = self.mess_H_Particles(1,:)/sum(self.mess_H_Particles(1,:));
                end
            end
            
            self.mess_Trans_Particles = self.mess_H_Particles;
        end
        
        function self = assimilateAllies(self, index)
%         function self = assimilateAllies(self, index, m)
            
            input_Particles = self.mess_Trans_Particles;
%             self.mess_A_Particles(2:4,:) = self.mess_Trans_Particles(2:4,:);
            self.mess_A_Particles = self.mess_Trans_Particles;
            
            if ~isempty(self.measurementCamera{index})
                allies = self.measurementCamera{index}.players;
                alliesID = self.measurementCamera{index}.playersID;
                
                % here, the true bearing is restricted to (-pi,pi]. The
                % noise is applied in the sequence
                % (this avoids that the noise seems much more powerful than
                % it is in the case of a wrapping).
                % (functions: observe and hPhi)
                %
                % Same for the measurement of DoA (in self.hear())
                %
                % Thus, the mean of the likleihood must be in local
                % coordinates (agent's pose), equal to the relative pose of
                % the observed target.
                % Apparently, this is done in the pObs function (since it
                % calls h_phi, which will measure in local coordinates

%                 auxProd = 1;
                % Here, the loop over allies i must come before the loop over
                % the particles p to avoid sampling Nq = Np particles for
                % the ally for each particle p of the current agent.
                logWeightsAlliesParticles = zeros(size(allies, 1), self.Np);
                for i = 1:size(allies, 1)
                    
                    % can only assimilate if knows some belief from target
                    playerID = alliesID(i);
                    beliefOfTarget = self.beliefsGMM{playerID,4};
                    if ~isempty(beliefOfTarget)
                        obs_n_r_ra = allies(i,:).';
%                         obs_n_r_ra = allies(i,:,m).';

                        % extrapolate belief. Maybe just increasing
                        % covariance with 4*stddev = Vmax*deltaT (now -
                        % last incoming message)

                        % sampling particles for the position of the
                        % target
                        ally_particles = zeros(4, self.Np);
                        ally_particles(1,:) = 1/self.Np;
                        
                        dt = index*self.T - self.beliefsGMM{playerID,1};

                        % Extrapolando como random walk: mantendo a
                        % m�dia e inchando a covari�ncia
                        covExtrapolation = diag( [ self.vXYMax*dt, self.vXYMax*dt, self.vPsiMax*dt ].^2 );
                        
                        try
                            ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, self.beliefsGMM{playerID,5} + covExtrapolation, self.Np ).';
                            ally_particles(4,:) = wrapAngle(ally_particles(4,:));
        %                     ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, self.beliefsGMM{playerID,5}, self.Np ).';

                        catch
                            disp('entrou no catch')                              
%                             disp('cov matrix from belief');
%                             self.beliefsGMM{playerID,5}
%                             disp('eigenvalues');
%                             eig(self.beliefsGMM{playerID,5})
%                             disp('sampling from 0.1*eye(3) covariance');
%                             ally_particles(2:4,:) = mvnrnd( self.beliefsGMM{playerID,4}, 0.1*eye(3), self.Np ).';
                        end
                    end
                    
                    for p = 1:self.Np
%                         auxProd = 1;
%                         auxProdLog = 0;
%                         factor_vec = zeros(1, self.Np);                        
                        factorLogMarginalVec = zeros(1, self.Np);
                        for p_ally = 1:self.Np
                            factorLogMarginalVec(p_ally) = pObsLog(obs_n_r_ra, input_Particles(2:4,p), ally_particles(2:4,p_ally), 0, self.inflateRho*self.rangeVariancePar, 0, self.inflatePhiCam*self.bearingVariance);
%                             factor_vec(p_ally) = pObs(obs_n_r_ra, input_Particles(2:4,p), ally_particles(2:4,p_ally), 0, self.rangeVariancePar, 0, self.bearingVariance);
                        end
                        max_exponent = max(factorLogMarginalVec);
                        S = sum( exp( factorLogMarginalVec - max_exponent) * ally_particles(1,:).' ) ;
                        
                        logWeightsAlliesParticles(i,p) = max_exponent + log(S);
                        
                        
%                         auxProdLog = auxProdLog + factorLog;
                        
%                         factor = ally_particles(1,:)*factor_vec.';
%                         auxProd = auxProd*factor;
                        
                        % TALVEZ O MAX N�O SEJA NECESS�RIO
                        % TROCAR O POBS
                        % TESTAR LOG-LIKELIHOOD
%                         auxProd = max(auxProd, 1e-10);
%                         self.mess_A_Particles(1,p) = input_Particles(1,p)*auxProd;

%                         self.mess_A_Particles(1,p) = max(self.mess_A_Particles(1,p)*auxProd, 1e-10);
                    end
                end
                
                logWeightsParticles = sum(logWeightsAlliesParticles, 1);
                for p = 1:self.Np
                    self.mess_A_Particles(1,p) = input_Particles(1,p) * 1/sum( exp(logWeightsParticles - logWeightsParticles(p) ) );
                end

                if sum(self.mess_A_Particles(1,:)) == 0
                    disp(['Robot ' num2str(self.ID) ': sum of weights zero in allies assimilation']);
                    self.mess_A_Particles(1,:) = 1/self.Np*ones(1,self.Np);
                else
                    self.mess_A_Particles(1,:) = self.mess_A_Particles(1,:)/sum(self.mess_A_Particles(1,:));
                end
            end
            
            self.mess_Trans_Particles = self.mess_A_Particles;
        end
        
        function self = resampleParticles(self, index, bRegularize)
            
            % Complete regularization: starting by resampling, then moving
            % the particles to the vicinity
            
            input_Particles = self.mess_Trans_Particles;

            resampled = zeros(4, self.Np);
            cumWeights = cumsum(input_Particles(1,:));
            
            % Removing any numerical imprecision that could lead to a pmf
            % that does not add up to one
            cumWeights = cumWeights*1/cumWeights(end);
            cumWeights(end) = 1;
            
            for p = 1:self.Np
                aux = rand; % between 0 and 1
                
                % finding the first element greater than aux: corresponds
                % to resampled particle
                auxvec = cumWeights - aux;
                posvec = find(auxvec > 0);
                resampled_ind = posvec(1);
                
                resampled(1,p) = 1/self.Np;
                resampled(2:4,p) = input_Particles(2:4, resampled_ind);
            end            
            
            if ~bRegularize
                self.mess_Trans_Particles = resampled;
            else
                % Regularization step: x, y and psi

                [~, covariance_Particles] = self.sampleStatistics(input_Particles);

                nx = size(input_Particles,1)-1;
                h = (4/(nx+2))^(1/(nx+4)) * self.Np^(-1/(nx+4));
                
%                 mineig = min(eig(covariance_Particles));
%                 mineig
                
                D = chol(covariance_Particles);

                stdnorm = mvnrnd(zeros(nx,1), eye(nx), self.Np).';
                regularized = resampled + [zeros(1,self.Np); h*D.'*stdnorm];
                
                % NOT WRAPPING ANGLE WHEN PARTICLES ARE DRAWN!
                regularized(4,:) = wrapAngle(regularized(4,:));

                % Selecting output
                self.mess_Trans_Particles = regularized;
            end
            
        end
        
        
        
        function self = evaluateNewBelief(self, index)
            self.mess_B_Particles = self.mess_Trans_Particles;
            
            mean_B = self.sampleStatistics(self.mess_B_Particles);
            
%             mean_B = self.mess_B_Particles(2:4,:) * (self.mess_B_Particles(1,:)).';
%             mean_B(3) = wrapAngle(mean_B(3));
            
            self.estimatedState(index,:) = mean_B.';
            
%             if self.ID == 1
%                 disp('real state:');
%                 disp(self.realData(index,:));
%                 disp('estimated state: ');
%                 disp(mean_B.');
%             end
        end
        
        
        
        function self = evaluateErrors(self, index)            
%             % evaluating expected position, calculating error
%             mean_B = self.mess_B_Particles(2:4,:)*(self.mess_B_Particles(1,:)).';
%             mean_B(3) = wrapAngle(mean_B(3));
%             self.estimationError(:,index) = mean_B - self.realData(index,:).';
%             self.estimationError(3,index) = wrapAngle(self.estimationError(3, index));

            error = self.estimatedState(index,:) - self.realData(index,:);
            error(3) = wrapAngle(error(3));
            self.estimationError(:,index) = error.';
            
%             if self.ID == 1
%                 disp('estimation error: ')
%                 disp(self.estimationError(:,index).');
%             end
            
        end
        
        function scatterParticles(self, index, plotIndex, strTitle)
            
            if nargin < 4
                strTitle = 'Title';
            end
                         
% %             if self.ID == 1
%                 figure(20+self.ID)
%                 subplot(2,2,plotIndex);
% %                 subplot(1,1,1);
%                 scatter3( [self.realData(index, 1), self.mess_Trans_Particles(2,:)], ... 
%                           [self.realData(index, 2), self.mess_Trans_Particles(3,:)], ...
%                           [self.realData(index, 3), self.mess_Trans_Particles(4,:)], ...
%                           [], ...
%                           [-1/self.Np, self.mess_Trans_Particles(1,:)] );
%                 colormap jet
%                 colorbar
%                 caxis([-1/self.Np,2/self.Np])
%                 xlim([-10, -6]); ylim([3, 5]);
%                 xlabel('x (m)'); ylabel('y (m)'); zlabel('psi (rad)');
%                 title(title_str{plotIndex});
%                 view([0, 0, 1])
% %             end
            
            figure(20+self.ID);
            subplot(2,2,plotIndex);
            x_vec = [self.realData(index,1), self.mess_Trans_Particles(2,:)];
            y_vec = [self.realData(index,2), self.mess_Trans_Particles(3,:)];
            psi_vec = [self.realData(index,3), self.mess_Trans_Particles(4,:)];
            
            w_vec = [-1/self.Np, self.mess_Trans_Particles(1,:)];
%             w_color = [0; 0.4470; 0.7410] * w_vec;
            
%             plot(x_vec(1), y_vec(1), 'x'); hold on;            
%             plot(x_vec(2:end), y_vec(2:end), 'o'); hold on;
            
            scatter(x_vec(1), y_vec(1), [], [0.8500 0.3250 0.0980], 'v', 'filled'); hold on;
            scatter(x_vec(2:end), y_vec(2:end), [], w_vec(2:end), 'o', 'filled'); hold on;
            
            for p = 1:self.Np + 1
                d = 0.25;
                plot( [x_vec(p), x_vec(p) + d*cos(psi_vec(p))], [y_vec(p), y_vec(p) + d*sin(psi_vec(p))], 'k-' )
            end
            hold off
            
            title(strTitle)
            xlim([self.x0 - 2.5, self.x0 + 2.5]); ylim([self.y0 - 2.5, self.y0 + 2.5]);
        end
        
        
%         function plotParticles(self, index)
%             figure(41)
%             subplot(2,2,self.ID);
%             plot(self.mess_P_Particles(2,:), self.mess_P_Particles(3,:), 'o');
%             hold all
%             plot(self.mess_L_Particles(2,:), self.mess_L_Particles(3,:), '*');
%             plot(self.mess_H_Particles(2,:), self.mess_H_Particles(3,:), 'h');
%             plot(self.mess_A_Particles(2,:), self.mess_A_Particles(3,:), 'x');
%             plot(self.realData(index, 1), self.realData(index, 2), 'ks', 'MarkerSize', 10);
%             drawField(self.landmarks);
%             legend('predicted', 'obsLandmarks', 'heard', 'obsAllies', 'ground truth')
%             hold off
%         end
        
        
        
        
%         function plotMeanParticles(self, index)
%             figure(43)
%             subplot(2,2,self.ID);
%             
%             % averaging particles to find their center of gravity
%             mean_P = self.mess_P_Particles(2:4,:)*(self.mess_P_Particles(1,:)).';
%             mean_L = self.mess_L_Particles(2:4,:)*(self.mess_L_Particles(1,:)).';
%             mean_H = self.mess_H_Particles(2:4,:)*(self.mess_H_Particles(1,:)).';
%             mean_A = self.mess_A_Particles(2:4,:)*(self.mess_A_Particles(1,:)).';           
%             
%             % plotting mean of particles: x and y
%             plot(mean_P(1), mean_P(2), 'o');
%             plot(mean_L(1), mean_L(2), '*');
%             plot(mean_H(1), mean_H(2), 'h');
%             plot(mean_A(1), mean_A(2), 'x');
%             plot(self.realData(index, 1), self.realData(index, 2), 'ks');
%             legend('predicted', 'obsLandmarks', 'heard', 'obsAllies', 'ground truth')
%             
%             % plotting orientation of particles: psi
%             plot([mean_P(1), mean_P(1) + 1*cos(mean_P(3))], [mean_P(2), mean_P(2) + 1*sin(mean_P(3))], 'k');
%             plot([mean_L(1), mean_L(1) + 1*cos(mean_L(3))], [mean_L(2), mean_L(2) + 1*sin(mean_L(3))], 'k');
%             plot([mean_H(1), mean_H(1) + 1*cos(mean_H(3))], [mean_H(2), mean_H(2) + 1*sin(mean_H(3))], 'k');
%             plot([mean_A(1), mean_A(1) + 1*cos(mean_A(3))], [mean_A(2), mean_A(2) + 1*sin(mean_A(3))], 'k');
%             plot([self.realData(index, 1), self.realData(index, 1) + 1*cos(self.realData(index,3))], [self.realData(index, 2), self.realData(index, 2) + 1*sin(self.realData(index,3))], 'k');
%             drawField(self.landmarks);
%             hold off;            
%         end
        
    end
end
