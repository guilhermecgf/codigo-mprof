% %% Script to plot the plots desired in the paper.
% Position and orientation error, bars per method, objects detected
clear all; close all
addpath('plotresults_functions')
% load('output_final/2020022-novomc/novomc-2627fev.mat');
% load('output_final/sim_cluster_bu/sim_cluster_bu513.mat');
% load('output_final/teste1/sim_cluster_bu553.mat');
% load('output_final/teste2/sim_cluster_bu692.mat');
% load('output_final/sim_itandroids/sim119.mat');
% load('output_final/sim_itandroids_2/sim13.mat');
% load('output_final/sim_itandroids_3/sim17.mat');
% load('output_final/sim_itandroids_4/sim19.mat');
% load('output_final/sim_itandroids_5/sim23.mat');
% load('output_final/sim_PL_only/sim641.mat'); % PL only, noise of mine, 1000 particles
% load('output_final/sim_PL_only/sim953.mat'); % PL only, noise itandroids, 1000 particles
% load('output_final/sim29/sim29.mat');
% load('output_final/sim31/sim31.mat'); 
load('output_final/sim421.mat'); 

obs_landmarks_average = squeeze(mean(obs_landmarks_total,1));
obs_players_average = squeeze(mean(obs_players_total,1));

%% Convert to cell format
mse_dist = cell(R,4);
mse_psi = cell(R,4);

for r = 1:R
    for s = 1:4
        mse_dist{r,s} = mse_dist_time_total(:,:,r,s);
        mse_psi{r,s} = mse_psi_time_total(:,:,r,s);
    end
end

%% If desired, remove outliers
boolRemoveOutliers = false;
if boolRemoveOutliers
    % k is number of standard deviations from the mean; internally,
    % function treats with median and absolute error
    k = 10;
    [mse_dist, mse_psi] = removeOutliers(mse_dist_time_total, mse_psi_time_total, err_x_time_total, err_y_time_total, err_psi_time_total, k, R, M, nIterations);
end

%% Another way of removing outliers: considering only MSE averaged over all time instants
boolRemoveOutliers = false;
if boolRemoveOutliers
    k = 4;
    [mse_dist, mse_psi, ind_no_outliers_dist, ind_no_outliers_psi] = removeOutliersNew(mse_dist, mse_psi, k);
    ind_no_outliers_dist, ind_no_outliers_psi
end

%%
% Para plotar, vamos deixar o plot do PLHA por �ltimo.
setupNamePlot = {'PL', 'PLH', 'PLA', 'PLHA'};
for r = 1:R
    aux = mse_dist{r,3};
    mse_dist{r,3} = mse_dist{r,4};
    mse_dist{r,4} = aux;
end

for r = 1:R
    aux = mse_psi{r,3};
    mse_psi{r,3} = mse_psi{r,4};
    mse_psi{r,4} = aux;
end

%% Plots finais para o artigo: dist�ncia
time_vec = T * [1:nIterations];
% sub_time = 2:(nIterations/2);
sub_time = 1:nIterations;

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 1.1];

% PLOT 1: en vs. tempo (erro m�dio, entre todos os rob�s, versus tempo),
% para cada m�todo

% conte�do est� em mse_dist{r,s}, que � uma matriz M vs.
% nIteration
en_dist_metodo = zeros(4, nIterations);
for s=1:4
    enr_dist = zeros(R, nIterations);
    for r=1:R
        
        aux = mse_dist{r,s};
        mse_dist{r,s} = aux;
        
        % mean over M        
        enr_dist(r,:) = mean(mse_dist{r,s}, 1);
    end
    % mean over R
    en_dist_metodo(s,:) = mean(enr_dist,1);
end

% reminder: what must be plotted is rmse
figure(1);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {':', '--', '-.', '-'};
for s=1:4
    plot(time_vec(sub_time), sqrt(en_dist_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
end
grid on; hold off;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Position RMSE (m)'); 
% title('Position error, averaged over all robots');
xlim(xlim_param); ylim(ylim_dist_param)

% PLOT 2: er para cada m�todo. 5 barras por m�todo
er_dist_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        mean_mc = mean(mse_dist{r,s}, 1);
        mean_mc_time = mean(mean_mc(sub_time));
%         er_dist_metodo(s,r) = mean(mean(mse_dist{r,s}));
        er_dist_metodo(s,r) = mean_mc_time;
    end
end

figure(2);
bar(sqrt(er_dist_metodo)); ylim([0, 1.1]);
ylabel('Position RMSE (m)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
% title('Position error, for each robot, averaged over all time instants.');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

% casos mais favor�veis: rob�s que veem poucos landmarks (2 e 3)

% PLOT 3: rob�(s) para os quais a coopera��o � mais importante
ticktype = {'x', 'v', 'o', 's'}; %per method
linetype = {'--', '-.', '-', '-', ':'}; % per robot
figure(3)
r1 = 2; % primeira simula��o
for s = [1,4]
    mean_mse = mean( mse_dist{r1,s}, 1);
    var_mse = var( mse_dist{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r1}], 'LineWidth', 1.5);
    hold all;
end

r2 = 5; % primeira simula��o
for s = [1,4]
    mean_mse = mean( mse_dist{r2,s}, 1);
    var_mse = var( mse_dist{r2,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r2}], 'LineWidth', 1.5);
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r2}], 'LineWidth', 1.5);
    hold all;
end
grid on; hold off; 
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], ['Robot ' num2str(r2) ' - ' setupNamePlot{1}], ['Robot ' num2str(r2) ' - ' setupNamePlot{4}]);
xlabel('Time (s)'); ylabel('Position RMSE (m)');
xlim(xlim_param); ylim(ylim_dist_param)
% title(['Position error for best and worst robot, averaged over all Monte Carlo realizations']);

% PLOT 4: n�mero m�dio de landmarks vistos pelo rob� mais e menos favoravel
figure(4);
% subplot(1,2,1);
total_camera_obs = [obs_landmarks_average(r1,sub_time); obs_players_average(r1,sub_time)].';
H = bar(time_vec(sub_time), total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
xlabel('Time (s)'); ylabel('Number of camera observations');
% title(['Average number of observed features, for all Monte Carlo realizations. Robot ' num2str(r1)]); 
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);

figure(5);
% subplot(1,2,2);
total_camera_obs = [obs_landmarks_average(r2,sub_time); obs_players_average(r2,sub_time)].';
H = bar(time_vec(sub_time), total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
xlabel('Time (s)'); ylabel('Number of camera observations');
% title(['Average number of observed features, for all Monte Carlo realizations. Robot ' num2str(r2)]); 
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);

%% Plots finais para o artigo: orientation

% PLOT 6: en vs. tempo (erro m�dio, entre todos os rob�s, versus tempo),
% para cada m�todo

% conte�do est� em mse_psi{r,s}, que � uma matriz M vs.
% nIteration
ylim_psi_param = [0, 0.25];

en_psi_metodo = zeros(4, nIterations);
for s=1:4
    enr_psi = zeros(R, nIterations);
    for r=1:R
        % mean over M
        enr_psi(r,:) = mean(mse_psi{r,s}, 1);
    end
    % mean over R
    en_psi_metodo(s,:) = mean(enr_psi,1);
end

% reminder: what must be plotted is rmse
figure(6);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {':', '--', '-.', '-'};
for s=1:4
    plot(time_vec(sub_time), sqrt(en_psi_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
end
hold off; grid on;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');
xlim(xlim_param); ylim(ylim_psi_param);

% PLOT 7: er para cada m�todo. 5 barras por m�todo
er_psi_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        mean_mc = mean(mse_psi{r,s}, 1);
        mean_mc_time = mean(mean_mc(sub_time));
%         er_psi_metodo(s,r) = mean(mean(mse_psi{r,s}));
        er_psi_metodo(s,r) = mean_mc_time;
    end
end

figure(7); ylim(ylim_psi_param);
bar(sqrt(er_psi_metodo));
ylabel('Orientation RMSE (rad)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

% % PLOT 8: er para cada rob�. 4 barras por rob�
% figure(8);
% H = bar(sqrt(er_psi_metodo.'));
% ax = gca;
% for k=1:4
%     auxcolor = lines(k);
%     set(H(k), 'facecolor', auxcolor(k,:), 'EdgeColor', 'none');
% end
% ylabel('Orientation RMSE (rad)'); xlabel('Robot')
% legend(setupNamePlot); ylim(ylim_psi_param);

% casos mais favor�veis: rob�s que veem poucos landmarks (2 e 3)
% PLOT 9: rob�(s) para os quais a coopera��o � mais importante
ticktype = {'x', 'v', 'o', 's'}; %per method
linetype = {'--', '-.', '-', '-', ':'}; % per robot
figure(9)
r1 = 2;
for s = [1,4]
    mean_mse = mean( mse_psi{r1,s}, 1);
    var_mse = var( mse_psi{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r1}], 'LineWidth', 1.5);
    hold all;
end

r2 = 5;
for s = [1,4]
    mean_mse = mean( mse_psi{r2,s}, 1);
    var_mse = var( mse_psi{r2,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r2}], 'LineWidth', 1.5);
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r2}], 'LineWidth', 1.5);
    hold all;
end
grid on; ylim(ylim_psi_param); xlim(xlim_param);
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], ['Robot ' num2str(r2) ' - ' setupNamePlot{1}], ['Robot ' num2str(r2) ' - ' setupNamePlot{4}]);
xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');