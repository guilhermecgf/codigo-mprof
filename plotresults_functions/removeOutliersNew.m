function [ mse_dist, mse_psi, ind_non_outliers_dist, ind_non_outliers_psi ] = removeOutliersNew( mse_dist_all, mse_psi_all, k, flag_median )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 4
        flag_median = true;
    end
    
    if nargin < 3
        k = 3;
    end
    
    R = size(mse_dist_all,1);
    
    mse_dist = cell(R,4);
    ind_non_outliers_dist = cell(R,4);
    
    mse_psi = cell(R,4);
    ind_non_outliers_psi = cell(R,4);
    
    for r = 1:R
        for s = 1:4
            aux_dist = mse_dist_all{r,s};
            vec_dist = mean(aux_dist,2);
            
            aux_psi = mse_psi_all{r,s};
            vec_psi = mean(aux_psi,2);
            
            if flag_median
                average_dist = median(vec_dist);
                deviation_dist = 1.5*mad(vec_dist,1);
                
                average_psi = median(vec_psi);
                deviation_psi = 1.5*mad(vec_psi,1);
            else        
                average_dist = mean(vec_dist);
                deviation_dist = std(vec_dist);
                
                average_psi = mean(vec_psi);
                deviation_psi = std(vec_psi,1);
            end

            ind_non_outliers_dist{r,s} = find( abs(vec_dist - average_dist) < k*deviation_dist ) ;
            ind_non_outliers_psi{r,s} = find( abs(vec_psi - average_psi) < k*deviation_psi ) ;
        end
    end
    
    for r = 1:R
        for s = 1:4
            aux_dist = mse_dist_all{r,s};
            mse_dist{r,s} = aux_dist(ind_non_outliers_dist{r,s}, :);
            
            aux_psi = mse_psi_all{r,s};
            mse_psi{r,s} = aux_psi(ind_non_outliers_psi{r,s}, :);
        end
    end

end

