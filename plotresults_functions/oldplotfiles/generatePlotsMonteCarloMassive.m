%% Loading variables
% For Massive amount of Monte Carlo Simulations, reading one file at a time
clear all; close all

%------------------------------------------------------------------------------
% config 1. R = 5, M = 1000, Np = 600, belief = extrap, cam = 3, time = 6, T = 0.2 
% new monte carlo calc
% estID = 5;
estID = 127;
data = cell(1,4);
% m_vec = [1:299, 301:799, 801:999];
m_vec = 1:3;
M = length(m_vec);

setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

for indm = 1:M
    m = m_vec(indm)
    
%     strbase = ['output_final/light/0-20200220-' num2str(estID)];
    strbase = ['output_final/20200226-' num2str(estID)];

%     data{1} = load([strbase '_PL_MC' num2str(m,'%03d') 'light.mat']);
%     data{2} = load([strbase '_PLH_MC' num2str(m,'%03d') 'light.mat']);
%     data{3} = load([strbase '_PLHA_MC' num2str(m,'%03d') 'light.mat']);
%     data{4} = load([strbase '_PLA_MC' num2str(m,'%03d') 'light.mat']);

    data{1} = load([strbase '_PL_MCtraj' num2str(m,'%03d') '.mat']);
    data{2} = load([strbase '_PLH_MCtraj' num2str(m,'%03d') '.mat']);
    data{3} = load([strbase '_PLHA_MCtraj' num2str(m,'%03d') '.mat']);
    data{4} = load([strbase '_PLA_MCtraj' num2str(m,'%03d') '.mat']);
    
    if m == 1        
        R = data{1}.simConfig.R;
        nIterations = data{1}.simConfig.nIterations;
        T = data{1}.simConfig.T;
        
        time_vec = T * [1:nIterations];
        
        obs_landmarks = zeros(R, nIterations);
        obs_players = zeros(R, nIterations);
        
        for r=1:R
            for i=2:nIterations
                try
                    obs_landmarks(r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.goalsID) + length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.cornersID);
                    obs_players(r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.playersID);
                catch
                    %disp(['no camera observations at time instant ' num2str(i)]);
                end
            end
        end        
        
        err_x_time = zeros(M, nIterations, R, 4);
        err_y_time = zeros(M, nIterations, R, 4);
        err_psi_time = zeros(M, nIterations, R, 4);
        
        mse_x_time = zeros(M, nIterations, R, 4);
        mse_y_time = zeros(M, nIterations, R, 4);
        mse_psi_time = zeros(M, nIterations, R, 4);
        mse_dist_time = zeros(M, nIterations, R, 4);
    end
    
    for r = 1:R
        for s = 1:4
            err_x = data{s}.TeamEstSave{r,s}.estimationError(1,:);
            mse_x_time(indm, :, r, s) = err_x.^2;
            
            err_y = data{s}.TeamEstSave{r,s}.estimationError(2,:);
            mse_y_time(indm, :, r, s) = err_y.^2;
            
            mse_dist_time(indm, :, r, s) = mse_x_time(indm, :, r, s) + mse_y_time(indm, :, r, s);
            
            err_psi = data{s}.TeamEstSave{r,s}.estimationError(3,:);
            mse_psi_time(indm, :, r, s) = err_psi.^2;
        end
    end
end

%% Generating plots
% clear all;
% load('output_final/light/data_MonteCarlo_light.mat');


% % choosing only a subset of all MC realizations
subset_mc = 1:3;
mse_dist_time_sub = mse_dist_time(subset_mc, :, :, :);
mse_psi_time_sub = mse_psi_time(subset_mc, :, :, :);

mse_dist_time_mean = mean(mse_dist_time_sub, 2); 
mse_psi_time_mean = mean(mse_psi_time_sub, 2);

mse_dist_mc_time_mean = squeeze(mean(mse_dist_time_mean, 1));
mse_psi_mc_time_mean = squeeze(mean(mse_psi_time_mean, 1));

mse_dist_mc_time_var = squeeze(var(mse_dist_time_mean, 0, 1));
mse_psi_mc_time_var = squeeze(var(mse_psi_time_mean, 0, 1));

% mse_dist_averaged = squeeze(mean(mean(mse_dist_time_sub,1),2));
% mse_psi_averaged = squeeze(mean(mean(mse_psi_time_sub,1),2));

mse_dist_averaged = squeeze(mean(mean(mse_dist_time,1),2));
mse_psi_averaged = squeeze(mean(mean(mse_psi_time,1),2));

% plotting R figures, each with 4 subfigures (dist, psi, number of landmarks, number of allies),
% each with the four methods
for r = 1:R
    figure(50+r);
    subplot(1,3,1);
    for s = 1:4
        stem(s, mse_dist_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
    end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist');
    legend(setupName); grid on;
    ylim([0, 1]); xlim([0,5]);
    
    subplot(1,3,2);
    for s = 1:4
        stem(s, mse_psi_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
    end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi');
    legend(setupName); grid on;
    ylim([0, 0.05]); xlim([0,5]);
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
end

% plotting R figures, each with 4 subfigures (dist, psi, number of landmarks, number of allies),
% each with the four methods
for r = 1:R
    figure(30+r);
    subplot(1,3,1);
    errorbar(1:4, mse_dist_mc_time_mean(r,:), mse_dist_mc_time_var(r,:));
%     for s = 1:4
%         stem(s, mse_dist_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist');
    legend(setupName); grid on;
    ylim([0, 1]); xlim([0,5]);
    
    subplot(1,3,2);
    errorbar(1:4, mse_psi_mc_time_mean(r,:), mse_psi_mc_time_var(r,:));
%     for s = 1:4
%         stem(s, mse_psi_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi');
    legend(setupName); grid on;
    ylim([0, 0.05]); xlim([0,5]);
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
end

% plotting mse versus time for some agents
r_vec = 1:5;
for r = r_vec
    mse_dist_time_sub_r = mse_dist_time_sub(:,:,r,:);
    mse_psi_time_sub_r = mse_psi_time_sub(:,:,r,:);
    
    mse_dist_time_sub_r_mean = squeeze(mean(mse_dist_time_sub_r, 1));
    mse_psi_time_sub_r_mean = squeeze(mean(mse_psi_time_sub_r, 1));
    
    mse_dist_time_sub_r_var = squeeze(var(mse_dist_time_sub_r, 0, 1));
    mse_psi_time_sub_r_var = squeeze(var(mse_psi_time_sub_r, 0, 1));
        
    figure(100*r);
    for s = 1:4
        subplot(2,3,s+floor(s/3));
        errorbar(time_vec, mse_dist_time_sub_r_mean(:,s), mse_dist_time_sub_r_var(:,s) );
        legend(setupName{s});
        xlabel('time (s)'); ylabel('mse dist (m2)'); ylim([0,1]);        
    end    
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
        
    figure(100*r+1);    
    for s = 1:4
        subplot(2,3,s+floor(s/3));
        errorbar(time_vec, mse_psi_time_sub_r_mean(:,s), mse_psi_time_sub_r_var(:,s) );
        legend(setupName{s});
        xlabel('time (s)'); ylabel('mse psi (rad2)'); ylim([0,0.1]);        
    end
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
end
