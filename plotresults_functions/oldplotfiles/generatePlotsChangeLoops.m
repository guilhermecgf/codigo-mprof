%% Loading variables
clear all; close all

% 25 time instants, 50 particles. Antes de somar 1e-8*I 
% data{1} = load('output_final/20191105T120155_PL.mat');
% data{2} = load('output_final/20191105T120155_PLH.mat');
% data{3} = load('output_final/20191105T120156_PLHA.mat');
% data{4} = load('output_final/20191105T120156_PLA.mat');

% 25 time instants, 50 particles. Depois de somar 1e-8*I
% (PLHA e PLA, que estavam quebrando, melhoraram. Os que n�o haviam
% quebrado tiveram uma leve piora)
% data{1} = load('output_final/20191105T193255_PL.mat');
% data{2} = load('output_final/20191105T193255_PLH.mat');
% data{3} = load('output_final/20191105T193256_PLHA.mat');
% data{4} = load('output_final/20191105T193256_PLA.mat');

% % 150 time instants, 50 particles. Depois de somar 1e-8*I
% data{1} = load('output_final/20191105T195506_PL.mat');
% data{2} = load('output_final/20191105T195507_PLH.mat');
% data{3} = load('output_final/20191105T195509_PLHA.mat');
% data{4} = load('output_final/20191105T195511_PLA.mat');

% % 150 time instants, 50 particles. Depois de somar 1e-8*I. Com most recent
% % belief
% data{1} = load('output_final/20191105T201154_PL.mat');
% data{2} = load('output_final/20191105T201155_PLH.mat');
% data{3} = load('output_final/20191105T201157_PLHA.mat');
% data{4} = load('output_final/20191105T201159_PLA.mat');

% % 150 time instants, 50 particles. Sem 1e-8*I, com regularization. Com most recent
% % belief
% data{1} = load('output_final/20191127T114738_PL.mat');
% data{2} = load('output_final/20191127T114739_PLH.mat');
% data{3} = load('output_final/20191127T114741_PLHA.mat');
% data{4} = load('output_final/20191127T114743_PLA.mat');

% % 150 time instants, 400 particles. Depois de somar 1e-8*I. Sem most recent
% % belief. Approx 2h30
% data{1} = load('output_final/20191106T004539_PL.mat');
% data{2} = load('output_final/20191106T004551_PLH.mat');
% data{3} = load('output_final/20191106T004700_PLHA.mat');
% data{4} = load('output_final/20191106T004758_PLA.mat');

% % 150 time instants, 800 particles. Depois de somar 1e-8*I. Sem most recent
% % belief. Approx 10h
% data{1} = load('output_final/20191106T162845_PL.mat');
% data{2} = load('output_final/20191106T162929_PLH.mat');
% data{3} = load('output_final/20191106T163358_PLHA.mat');
% data{4} = load('output_final/20191106T163747_PLA.mat');

% % 150 time instants, 400 particles. Depois de somar 1e-8*I. Com most recent
% % belief. Approx 2h30
% data{1} = load('output_final/20191107T015239_PL.mat');
% data{2} = load('output_final/20191107T015250_PLH.mat');
% data{3} = load('output_final/20191107T015401_PLHA.mat');
% data{4} = load('output_final/20191107T015459_PLA.mat');

% % 150 time instants, 800 particles. Depois de somar 1e-8*I. Com most recent
% % belief. Approx 10h
% data{1} = load('output_final/20191107T163133_PL.mat');
% data{2} = load('output_final/20191107T163216_PLH.mat');
% data{3} = load('output_final/20191107T163648_PLHA.mat');
% data{4} = load('output_final/20191107T164039_PLA.mat');

% % % 150 time instants, 800 particles. Depois de somar 1e-8*I. Com most recent
% % % belief. Approx 10h. sem resampling
% data{1} = load('output_final/20191107T163133_PL.mat');
% data{2} = load('output_final/20191107T163216_PLH.mat');
% data{3} = load('output_final/20191107T163648_PLHA.mat');
% data{4} = load('output_final/20191107T164039_PLA.mat');

% % 150 time instants, 400 particles. Depois de somar 1e-8*I. Com most recent
% % belief. R = 8.
% data{1} = load('output_final/20191113T093511_PL.mat');
% data{2} = load('output_final/20191113T093536_PLH.mat');
% data{3} = load('output_final/20191113T093701_PLHA.mat');
% data{4} = load('output_final/20191113T093800_PLA.mat');

% % 150 time instants, 400 particles. Depois de somar 1e-8*I. Semm most recent
% % belief. R = 8.
% data{1} = load('output_final/20191114T044231_PL.mat');
% data{2} = load('output_final/20191114T044256_PLH.mat');
% data{3} = load('output_final/20191114T044417_PLHA.mat');
% data{4} = load('output_final/20191114T044518_PLA.mat');

% % 150 time instants, 400 particles. Depois de somar 1e-8*I. Com most recent
% % belief. R = 8. Camera every 3 time instants
% data{1} = load('output_final/20191121T035223_PL.mat');
% data{2} = load('output_final/20191121T035254_PLH.mat');
% data{3} = load('output_final/20191121T035628_PLHA.mat');
% data{4} = load('output_final/20191121T035943_PLA.mat');

% 150 time instants, 800 particles. Depois de somar 1e-8*I. Com most recent
% belief. R = 8. Camera every 3 time instants
data{1} = load('output_final/20191121T232405_PL.mat');
data{2} = load('output_final/20191121T232539_PLH.mat');
data{3} = load('output_final/20191121T234011_PLHA.mat');
data{4} = load('output_final/20191121T235300_PLA.mat');

% % 150 time instants, 400 particles. Com regularization. Com most recent
% % belief. R = 8. Camera every 3 instants
% data{1} = load('output_final/20191205T020511_PL.mat');
% data{2} = load('output_final/20191205T020542_PLH.mat');
% data{3} = load('output_final/20191205T020926_PLHA.mat');
% data{4} = load('output_final/20191205T021244_PLA.mat');

% % 150 time instants, 800 particles. Com regularization. Com most recent
% % belief. R = 8. Camera every 3 instants
% data{1} = load('output_final/20191206T000906_PL.mat');
% data{2} = load('output_final/20191206T001049_PLH.mat');
% data{3} = load('output_final/20191206T002527_PLHA.mat');
% data{4} = load('output_final/20191206T003828_PLA.mat');


% data{1} = load('output_final/20191204T210358_PLHA.mat');
% data{1} = load('output_final/20191204T211550_PLHA.mat');


%% General information from read data
setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

% setup = [true, true, false, false; 
%          true, true, true, false;];
% setupName = {'PL', 'PLH'};

% setup = [true, true, true, true];
% setupName = {'PLHA'};
     
if size(setup,1) ~= length(data)
    error('there must be one data set for each estimation setup');
end

for s = 2:size(setup,1)
    if data{s}.simConfig.R ~= data{s-1}.simConfig.R
        error('scenarios have different number of agents');
    end
    
    if data{s}.simConfig.T ~= data{s-1}.simConfig.T
        error('scenarios have different sampling time');
    end
    
    if data{s}.simConfig.nIterations ~= data{s-1}.simConfig.nIterations
        error('scenarios have different number of iterations');
    end
end

time_vec = data{1}.simConfig.T * [1:data{1}.simConfig.nIterations];

%% Dashboard 1, with x-y-psi and number of observed allies

mse = zeros(data{1}.simConfig.R, 3, length(data));

for r = 1:data{1}.simConfig.R
    for s = 1:size(setup,1)
        data{s}.obs_landmarks = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
        data{s}.obs_players = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
    
        % counting number of observed features by each robot
        for i = 1:data{s}.simConfig.nIterations
            try
                data{s}.obs_landmarks(r,i) = length(data{s}.Team{r}.measurementCamera{i}.goalsID) + length(data{s}.Team{r}.measurementCamera{i}.cornersID);
            catch
%                 disp('non-existent field in struct, skipping')
            end
            
            try
                data{s}.obs_players(r,i) = length(data{s}.Team{r}.measurementCamera{i}.playersID);
            catch
%                 disp('non-existent field in struct, skipping')
            end
        end
        
        % checking whether the robots had seen the same number of obstacles
        % (which is necessary, but not sufficient, to guarantee we are
        % talking about the same scenario)
        if s > 1
            if data{s}.obs_landmarks ~= data{s-1}.obs_landmarks
                error('the same robot has not seen the same observations in different loaded data');
            end
        end
        
        % plotting x data of each estimation method in the setup
        figure(10+r);
        subplot(6,3,[1,2,4,5]);
        plot(time_vec, data{s}.TeamEst{r,s}.estimationError(1,:));
        hold all; grid on;
        mse(r,1,s) = mean(data{s}.TeamEst{r,s}.estimationError(1,:).^2);
        if s == length(data)
            legstr = {};
            for s = 1:length(data)
                legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,1,s)) ];
            end
            xlabel('time (s)'); ylabel('error in x (m)'); legend(legstr); ylim([-2,2]);
            title(['agent ' num2str(r)]);
        end
        
        subplot(6,3,[7,8,10,11]);
        plot(time_vec, data{s}.TeamEst{r,s}.estimationError(2,:))
        hold all; grid on;
        mse(r,2,s) = mean(data{s}.TeamEst{r,s}.estimationError(2,:).^2);
        if s == length(data)
            legstr = {};
            for s = 1:length(data)
                legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,2,s)) ];
            end
            xlabel('time (s)'); ylabel('error in y (m)'); legend(legstr); ylim([-2,2]);
        end
        
        subplot(6,3,[13,14,16,17]);
        plot(time_vec, data{s}.TeamEst{r,s}.estimationError(3,:))
        hold all; grid on;
        mse(r,3,s) = mean(data{s}.TeamEst{r,s}.estimationError(3,:).^2);
        if s == length(data)
            legstr = {};
            for s = 1:length(data)
                legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,3,s)) ];
            end
            xlabel('time (s)'); ylabel('error in psi (rad)'); legend(legstr); ylim([-0.5,0.5]);
        end
    end
    
    subplot(6,3,[3,6,9]);
    stem(time_vec, data{1}.obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('number of observed landmarks'); ylim([0,8]);
    
    subplot(6,3,[12,15,18]);
    stem(time_vec, data{1}.obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('number of observed players'); ylim([0,data{1}.simConfig.R]);
end

%% Dashboard 2, with d = sqrt(x^2 + y^2) and quality of allies' beliefs

rmse = zeros(data{1}.simConfig.R, 2, length(data));

for r = 1:data{1}.simConfig.R
            
    figure(30+r)
    
    for s = 1:size(setup,1)
        data{s}.obs_landmarks = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
    
        % counting number of observed features by each robot
        for i = 1:data{s}.simConfig.nIterations
            try
                data{s}.obs_landmarks(r,i) = length(data{s}.Team{r}.measurementCamera{i}.goalsID) + length(data{s}.Team{r}.measurementCamera{i}.cornersID);
            catch
            end
        end
        
        % checking whether the robots had seen the same number of obstacles
        % (which is necessary, but not sufficient, to guarantee we are
        % talking about the same scenario)
        if s > 1
            if data{s}.obs_landmarks ~= data{s-1}.obs_landmarks
                error('the same robot has not seen the same observations in different loaded data');
            end
        end
        
        subplot(8,4,[1,2,5,6,9,10,13,14]);
        rmse_time = sqrt(mean(data{s}.TeamEst{r,s}.estimationError(1:2,:).^2, 1));
        plot(time_vec, rmse_time);
        hold all; grid on
        rmse(r,1,s) = mean(rmse_time);
        if s == length(data)
            legstr = {};
            for s = 1:length(data)
                legstr{s} = [setupName{s} ' - RMSE: ' num2str(rmse(r,1,s)) ];
            end
            xlabel('time (s)'); ylabel('distance error (m)'); legend(legstr); ylim([0,2]);
            title(['agent ' num2str(r)]);
        end        
        
        subplot(8,4,[17,18,21,22,25,26,29,30]);
        plot(time_vec, data{s}.TeamEst{r,s}.estimationError(3,:));
        hold all; grid on
        rmse(r,2,s) = sqrt(mean(data{s}.TeamEst{r,s}.estimationError(3,:).^2));
        if s == length(data)
            legstr = {};
            for s = 1:length(data)
                legstr{s} = [setupName{s} ' - RMSE: ' num2str(rmse(r,2,s)) ];
            end
            xlabel('time (s)'); ylabel('error in psi (rad)'); legend(legstr); ylim([-0.5,0.5]);
        end
    end
    
    subplot(8,4,[3,4,7,8]);
    stem(time_vec, data{1}.obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);
    
    colormap jet
    colorbar
end

%% Plotting the quality of the belief heard from the broadcasting ally
for r = 1:data{1}.simConfig.R
    figure(30+r)
    
    cases = {'PLHA', 'PLH'};
    for c = 1:size(cases, 2)
        s = find(ismember(setupName, cases{c}));
        
        heard_ally = zeros(data{s}.simConfig.R, length(time_vec));
        errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
        for i = 2:length(time_vec)
            broadcaster = data{s}.TeamEst{r,s}.heardLocal(i,2);
            if broadcaster ~= 0
                heard_ally(r, i) = broadcaster;
                errorBeliefReal(r, i) = sqrt(mean(data{s}.TeamEst{broadcaster,s}.estimationError(1:2,i).^2));
            end
        end
        
        subplot_tiles = [11,15,19] + (c-1);
        subplot(8,4,subplot_tiles);        
        %preparing scatter plot
        x_sc = []; y_sc = []; z_sc = [];
        for i = 1:length(time_vec)
            for r1 = 1:data{s}.simConfig.R
                if heard_ally(r1,i) > 0
                    x_sc = [x_sc, time_vec(i)];
                    y_sc = [y_sc, heard_ally(r1,i)];
                    z_sc = [z_sc, errorBeliefReal(r1,i)];
                end
            end
        end
        
        scatter3(x_sc, y_sc, z_sc, [], z_sc, 'filled')
        caxis([0,1])
        xlabel('time (s)'); ylabel(['heard ally, ' cases{c}]); zlabel('ally self position error (m)')
        ylim([0,data{s}.simConfig.R]); zlim([0,1])
%         title(['robot: ', num2str(r), ', method: ', cases{c}])
        
    end
end

%% Plotting the quality of the belief used to assimilate the ally camera measurement
% Circle color represents the distance error; the belief standard
% deviation, if saved, could be in dashed line

for r = 1:data{1}.simConfig.R
    figure(30+r)
    
    cases = {'PLHA', 'PLA'};
    for c = 1:size(cases, 2)
        s = find(ismember(setupName, cases{c}));
    
        observed_allies = zeros(data{s}.simConfig.R, length(time_vec));
        errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
        for i = 2:length(time_vec)
            try
                totalAllies = length(data{s}.Team{r}.measurementCamera{i}.playersID);
            catch
                totalAllies = 0;
            end
            for a = 1:totalAllies
                foundAlly = data{s}.Team{r}.measurementCamera{i}.playersID(a);
                observed_allies(foundAlly, i) = foundAlly;
                
                % estimation error (rmse) of the position of agent "a" at
                % time i
                errorBeliefReal(foundAlly, i) = sqrt(mean(data{s}.TeamEst{foundAlly,s}.estimationError(1:2,i).^2));
            end
        end
        
%         % evaluating the error between: the belief agent "r" has about "a" at
%         % time i (I don't have it saved...); and the real position of agent "a" at time i
%         % alternately: the rmse of the position of agent "a" at time i
%         errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
%         for i = 2:length(time_vec)
%             totalAllies = length(data{s}.Team{r}.measurementCamera{i}.playersID);
%             for a = 1:totalAllies
%                 foundAlly = data{s}.Team{r}.measurementCamera{i}.playersID(a);
%                 
% %                 beliefRfromA_mean = data{s}.TeamEst{r,s}.notSelfbeliefHistory{4}(i, 1:2);
% %                 timeOfBelief = data{s}.TeamEst{r,s}.beliefHistory{1}(i);
% %                 realPositionA = data{s}.TeamEst{foundAlly,s}.realData(i, 1:2);
% %                 errorBeliefReal(foundAlly, i) = norm(beliefRfromA_mean - realPositionA);
% 
%                 % estimation error (rmse) of the position of agent "a" at
%                 % time i
%                 errorBeliefReal(foundAlly, i) = sqrt(mean(data{s}.TeamEst{foundAlly,s}.estimationError(1:2,i).^2));
%             end
%         end
        
        subplot_tiles = [23,27,31] + (c-1);
        subplot(8,4,subplot_tiles);
        
        %preparing scatter plot
        x_sc = []; y_sc = []; z_sc = [];
        for i = 1:length(time_vec)
            for r1 = 1:data{s}.simConfig.R
                if observed_allies(r1,i) > 0
                    x_sc = [x_sc, time_vec(i)];
                    y_sc = [y_sc, observed_allies(r1,i)];
                    z_sc = [z_sc, errorBeliefReal(r1,i)];
                end
            end
        end
        
        scatter3(x_sc, y_sc, z_sc, [], z_sc, 'filled')
        caxis([0,1])
        xlabel('time (s)'); ylabel(['observed ally, ' cases{c}]); zlabel('ally self position error (m)')
        ylim([0,data{s}.simConfig.R]); zlim([0,1])
%         colormap jet
%         colorbar
    end
    
end

%% plotting overall RMSE information
figure(30);
measurementStr = {'distance', 'pose'};
for state = 1:2
    legstr = {};
    for s = 1:length(data)
        subplot(1,2,state);
        stem(s, mean(rmse(:,state,s)), 's'); hold all; grid on;
%         semilogy(s, mean(rmse(:,state,s)), 's'); hold all; grid on;
        legstr{s} = setupName{s};
    end
    xlim([0,length(data)+1]);
    ylim([0, 0.5]); ylabel(['rmse, '  measurementStr{state}]); legend(legstr);
end

%% plotting overall MSE information
figure(10);
for state = 1:3
    legstr = {};
    for s = 1:length(data)
        subplot(1,3,state);
        semilogy(s, mean(mse(:,state,s)), 's'); hold all; grid on;
        legstr{s} = setupName{s};
    end
    xlim([0,length(data)+1]); 
    ylim([1e-3, 1]); ylabel(['mse, state ' num2str(state)]); legend(legstr);
end

%% Plotting eigenvalues representing the numeric state of the filter
% covariance matrix info plot (max eigen, min eigen, rcond)
maxEigVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));
minEigVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));
condVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));

for r = 1:data{1}.simConfig.R
    for s = 1:size(setup,1)
        for i = 1:data{1}.simConfig.nIterations
            cov = squeeze(data{s}.TeamEst{r,s}.beliefHistory{5}(i,:,:));
            cov = (cov + cov.')/2;
            eigens = eig( cov );
            maxEigVec(r,i,s) = max(eigens);
            minEigVec(r,i,s) = min(eigens);
            condVec(r,i,s) = maxEigVec(r,i,s)/minEigVec(r,i,s);
            if isnan(condVec(r,i,s))
                condVec(r,i,s) = 0;
            end                
        end
    end    
end

for r = 1:data{1}.simConfig.R
    figure(70+r);
    
    for s = 1:size(setup,1)
        subplot(3,1,1);
        semilogy( time_vec, squeeze(maxEigVec(r,:,s)), 'x' );
        hold all; grid on;
        if s == size(setup,1)
            xlabel('time (s)'); ylabel('max eigenvalue'); legend(setupName);
            title(['agent: ' num2str(r)]);
        end
        
        subplot(3,1,2)
        semilogy( time_vec, squeeze(minEigVec(r,:,s)), 'x' );
        hold all; grid on;
        if s == size(setup,1)
            xlabel('time (s)'); ylabel('min eigenvalue'); legend(setupName);
        end
        
        subplot(3,1,3)
        semilogy( time_vec, squeeze(condVec(r,:,s)), 'x' );
        hold all; grid on;
        if s == size(setup,1)
            xlabel('time (s)'); ylabel('ratio min/max eigenvalues'); legend(setupName);
        end
        
    end
end

