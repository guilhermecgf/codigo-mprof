clear all; close all

% % 100 particles, 300 time instants, broadcast of scheduled belief;
% resampling only at the end
% pl = load('output_final/20191102T221609_PL.mat');
% plh = load('output_final/20191102T221806_PLH.mat');
% plha = load('output_final/20191102T223047_PLHA.mat');
% pla = load('output_final/20191102T224157_PLA.mat');

% % 400 particles, 300 time instants, broadcast of scheduled belief;
% resampling only at the end
% pl = load('output_final/20191102T232342_PL.mat');
% plh = load('output_final/20191102T234909_PLH.mat');
% plha = load('output_final/20191103T030749_PLHA.mat');
% pla = load('output_final/20191103T060021_PLA.mat');

% % 100 particles, 50 time instants, broadcast of most recent belief; resampling after each assimilation
% pl = load('output_final/20191103T171744_PL.mat');
% plh = load('output_final/20191103T172422_PLH.mat');
% plha = load('output_final/20191103T183140_PLHA.mat');
% pla = load('output_final/20191103T191238_PLA.mat');

% % 300 particles, 10 time instants, broadcast of most recent belief; resampling after each assimilation
% pl = load('output_final/20191103T234333_PL.mat');
% plh = load('output_final/20191103T234406_PLH.mat');
% plha = load('output_final/20191103T234740_PLHA.mat');
% pla = load('output_final/20191103T235100_PLA.mat');

% % 100 particles, 150 time instants, broadcast of scheduled belief; resampling after each assimilation; fixed eigenvalues
% pl = load('output_final/20191104T001457_PL.mat');
% plh = load('output_final/20191104T001604_PLH.mat');
% plha = load('output_final/20191104T002121_PLHA.mat');
% pla = load('output_final/20191104T002555_PLA.mat');

% % 400 particles, 150 time instants, broadcast of scheduled belief; resampling after each assimilation; fixed eigenvalues
% pl = load('output_final/20191104T003124_PL.mat');
% plh = load('output_final/20191104T004448_PLH.mat');
% plha = load('output_final/20191104T020416_PLHA.mat');
% pla = load('output_final/20191104T031056_PLA.mat');

% % % 750 particles, 150 time instants, broadcast of scheduled belief; resampling after each assimilation; fixed eigenvalues
% pl = load('output_final/20191104T060644_PL.mat');
% plh = load('output_final/20191104T193517_PLH.mat');
% % those two are here just for completing the plot
% % plha = load('output_final/20191104T020416_PLHA.mat');
% % pla = load('output_final/20191104T031056_PLA.mat');

% % 600 particles, 150 time instants, broadcast of scheduled belief; resampling after each assimilation; fixed eigenvalues
pl = load('output_final/20191104T235110_PL.mat');
plh = load('output_final/20191105T002047_PLH.mat');
plha = load('output_final/20191105T032037_PLHA.mat');
pla = load('output_final/20191105T055224_PLA.mat');


mse_pl = zeros(3, pl.simConfig.R);
mse_plh = zeros(3, plh.simConfig.R);
mse_plha = zeros(3, plha.simConfig.R);
mse_pla = zeros(3, pla.simConfig.R);

% couting number of observed features
obs_landmarks = zeros(plha.simConfig.R, plha.simConfig.nIterations);
obs_players = zeros(plha.simConfig.R, plha.simConfig.nIterations);

for r = 1:plha.simConfig.R
    error_PL = pl.Team{r}.estimationError;
    error_PLH = plh.Team{r}.estimationError;
    error_PLHA = plha.Team{r}.estimationError;
    error_PLA = pla.Team{r}.estimationError;
    
    time_vec = plha.Team{1}.T * [1:plha.Team{1}.nIterations];
    
    % counting number of observed features
    for i = 2:plha.simConfig.nIterations
        obs_landmarks(r, i) = length(plha.Team{r}.measurementCamera{i}.goalsID) + length(plha.Team{r}.measurementCamera{i}.cornersID);
        obs_players(r, i) = length(plha.Team{r}.measurementCamera{i}.playersID);   
    end
    
    figure(10+r);
    
    subplot(6,3,[1,2,4,5]);
    % sequence of markers, if to be used: *, h, x, d
    plot(time_vec, error_PL(1,:), '-'); hold all; grid on;
    plot(time_vec, error_PLH(1,:), '-');
    plot(time_vec, error_PLHA(1,:), '-');
    plot(time_vec, error_PLA(1,:), '-');
    ylabel('error x (m)'); xlabel('time t (s)'); ylim([-2, 2]);
    title(['robot ' num2str(r)]);
        
    mse_pl(1,r) = mean(error_PL(1,:).^2);
    mse_plh(1,r) = mean(error_PLH(1,:).^2);
    mse_plha(1,r) = mean(error_PLHA(1,:).^2);
    mse_pla(1,r) = mean(error_PLA(1,:).^2);
    
    legend(['PL - MSE:' num2str(mse_pl(1,r), 4)], ['PLH - MSE:' num2str(mse_plh(1,r), 4)], ['PLHA - MSE:' num2str(mse_plha(1,r), 4)], ['PLA - MSE:' num2str(mse_pla(1,r), 4)])
    
    subplot(6,3,[7,8,10,11]);
    plot(time_vec, error_PL(2,:), '-'); hold all; grid on;
    plot(time_vec, error_PLH(2,:), '-');
    plot(time_vec, error_PLHA(2,:), '-');
    plot(time_vec, error_PLA(2,:), '-');
    ylabel('error y (m)'); xlabel('time t (s)'); ylim([-2, 2]);
    
    mse_pl(2,r) = mean(error_PL(2,:).^2);
    mse_plh(2,r) = mean(error_PLH(2,:).^2);
    mse_plha(2,r) = mean(error_PLHA(2,:).^2);
    mse_pla(2,r) = mean(error_PLA(2,:).^2);
    
    legend(['PL - MSE:' num2str(mse_pl(2,r), 4)], ['PLH - MSE:' num2str(mse_plh(2,r), 4)], ['PLHA - MSE:' num2str(mse_plha(2,r), 4)], ['PLA - MSE:' num2str(mse_pla(2,r), 4)])
    
    subplot(6,3,[13,14,16,17]);
    plot(time_vec, error_PL(3,:), '-'); hold all; grid on;
    plot(time_vec, error_PLH(3,:), '-');
    plot(time_vec, error_PLHA(3,:), '-');
    plot(time_vec, error_PLA(3,:), '-');
    ylabel('error psi (rad)'); xlabel('time t (s)'); ylim([-0.5, 0.5]);
    
    mse_pl(3,r) = mean(error_PL(3,:).^2);
    mse_plh(3,r) = mean(error_PLH(3,:).^2);
    mse_plha(3,r) = mean(error_PLHA(3,:).^2);
    mse_pla(3,r) = mean(error_PLA(3,:).^2);
    
    legend(['PL - MSE:' num2str(mse_pl(3,r), 4)], ['PLH - MSE:' num2str(mse_plh(3,r), 4)], ['PLHA - MSE:' num2str(mse_plha(3,r), 4)], ['PLA - MSE:' num2str(mse_pla(3,r), 4)])
    
    subplot(6,3,[3,6,9]);
    plot(time_vec, obs_landmarks(r, :));
    ylabel('number of observed landmarks'); xlabel('time');
    ylim([0,8]);
    
    subplot(6,3,[12,15,18]);
    plot(time_vec, obs_players(r, :));
    ylabel('number of observed players'); xlabel('time');
    ylim([0, plha.simConfig.R])
    
end

figure(10+7);
subplot(1,3,1);
stem(1, mean( mse_pl(1,:) ) ); hold all
stem(2, mean( mse_plh(1,:) ) );
stem(3, mean( mse_plha(1,:) ) );
stem(4, mean( mse_pla(1,:) ) );
legend('PL', 'PLH', 'PLHA', 'PLA')
ylabel('MSE'); title('dimension x');
xlim([0,5]);

subplot(1,3,2);
stem(1, mean( mse_pl(2,:) ) ); hold all
stem(2, mean( mse_plh(2,:) ) );
stem(3, mean( mse_plha(2,:) ) );
stem(4, mean( mse_pla(2,:) ) );
legend('PL', 'PLH', 'PLHA', 'PLA')
ylabel('MSE'); title('dimension y');
xlim([0,5]);

subplot(1,3,3);
stem(1, mean( mse_pl(3,:) ) ); hold all
stem(2, mean( mse_plh(3,:) ) );
stem(3, mean( mse_plha(3,:) ) );
stem(4, mean( mse_pla(3,:) ) );
legend('PL', 'PLH', 'PLHA', 'PLA')
ylabel('MSE'); title('dimension psi');
xlim([0,5]);
