%% Loading variables
clear all; close all

% % config 1. R = 5, M = 7, Np = 400, belief = recent, cam = 1, time = 6, T = 0.2 
% simID = 241;
% M = 7;
% R = 5;
% data = cell(4,M);
% for m=1:M
%     strbase = ['output_final/20200206-' num2str(simID)];
%     data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
%     data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
%     data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
%     data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
% end
% R = data{1,1}.simConfig.R;
% nIterations = data{1,1}.simConfig.nIterations;

% %------------------------------------------------------------------------------
% % config 2. R = 5, M = 8, Np = 800, belief = recent, cam = 1, time = 6, T = 0.2 
% simID = 370;
% M = 8;
% data = cell(4,M);
% for m=1:M
%     if m<=3
%         strbase = ['output_final/20200207-' num2str(simID)];
%     elseif m<=5
%         strbase = ['output_final/20200208-' num2str(simID)];
%     elseif m<=8
%         strbase = ['output_final/20200210-' num2str(simID)];
%     end
%     data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
%     data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
%     data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
%     data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
% end
% R = data{1,1}.simConfig.R;
% nIterations = data{1,1}.simConfig.nIterations;

% %------------------------------------------------------------------------------
% % config 3. R = 5, M = 10, Np = 600, belief = recent, cam = 3, time = 6, T = 0.2 
% simID = 815;
% M = 10;
% data = cell(4,M);
% for m=1:M
%     if m<=3
%         strbase = ['output_final/20200213-' num2str(simID)];
%     end
%     data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
%     data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
%     data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
%     data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
% end
% R = data{1,1}.simConfig.R;
% nIterations = data{1,1}.simConfig.nIterations;

% % %------------------------------------------------------------------------------
% % config 4. R = 5, M = 15, Np = 600, belief = old, cam = 3, time = 6, T = 0.2 
% simID = 815; % due to different number of Monte Carlo simulations, the
% trajectories here are different than those in the previous scenario.
% M = 15;
% data = cell(4,M);
% for m=1:M
%     if m<=3
%         strbase = ['output_final/20200214-' num2str(simID)];
%     end
%     data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
%     data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
%     data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
%     data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
% end
% R = data{1,1}.simConfig.R;
% nIterations = data{1,1}.simConfig.nIterations;

% %------------------------------------------------------------------------------
% % config 5. R = 5, M = 3, Np = 10, belief = recent, cam = 3, time = 6, T = 0.2 
% simID = 1;
% M = 3;
% data = cell(4,M);
% for m=1:M
%     if m<=3
%         strbase = ['output_final/20200216-' num2str(simID)];
%     end
%     data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
%     data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
%     data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
%     data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
% end
% R = data{1,1}.simConfig.R;
% nIterations = data{1,1}.simConfig.nIterations;

%------------------------------------------------------------------------------
% config 6. R = 5, M = 1000, Np = 10, belief = extrap, cam = 3, time = 6, T = 0.2 
% new monte carlo calc
simID = 5;
M = 2;
data = cell(4,M);
for m=1:M
    if m<=3
        strbase = ['output_final/20200220-' num2str(simID)];
    end
    data{1,m} = load([strbase '_PL_MC' num2str(m,'%03d') '.mat']);
    data{2,m} = load([strbase '_PLH_MC' num2str(m,'%03d') '.mat']);
    data{3,m} = load([strbase '_PLHA_MC' num2str(m,'%03d') '.mat']);
    data{4,m} = load([strbase '_PLA_MC' num2str(m,'%03d') '.mat']);
end
R = data{1,1}.simConfig.R;
nIterations = data{1,1}.simConfig.nIterations;

%% General information from read data
setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

% setup = [true, true, false, false; 
%          true, true, true, false;];
% setupName = {'PL', 'PLH'};

% setup = [true, true, true, true];
% setupName = {'PLHA'};
     
if size(setup,1) ~= size(data,1)
    error('there must be one data set for each estimation setup');
end

% for s = 2:size(setup,1)
%     if data{s}.simConfig.R ~= data{s-1}.simConfig.R
%         error('scenarios have different number of agents');
%     end
%     
%     if data{s}.simConfig.T ~= data{s-1}.simConfig.T
%         error('scenarios have different sampling time');
%     end
%     
%     if data{s}.simConfig.nIterations ~= data{s-1}.simConfig.nIterations
%         error('scenarios have different number of iterations');
%     end
% end

time_vec = data{1}.simConfig.T * [1:data{1}.simConfig.nIterations];

%% First test: whether all Monte Carlo simulations present reasonable values, for a single agent

% count how many features each agent observes. 
% Single Monte Carlo iteration, single setup
m = 1; s = 1;
obs_landmarks = zeros(R, nIterations);
obs_players = zeros(R, nIterations);
for r=1:R
    for i=2:nIterations
        try
            obs_landmarks(r,i) = length(data{s,m}.TeamEst{r,1}.measurementCamera{i}.goalsID) + length(data{s,m}.TeamEst{r,1}.measurementCamera{i}.cornersID);
            obs_players(r,i) = length(data{s,m}.TeamEst{r,1}.measurementCamera{i}.playersID);
        catch
            disp(['no camera observations at time instant ' num2str(i)]);
        end
    end
end        

% for m = 1:M    
%     for r=1:R
%         figure(r);
%         subplot(3,3,m);
%         for s = 1:size(setup,1)
%             plot(time_vec, data{s,m}.TeamEst{r,s}.estimationError(1,:));
%             xlabel('time (s)');
%             ylabel('error in x (m)');
%             grid on; hold all
%         end
%         legend(setupName);
%         
%         subplot(3,3,8);
%         stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
%         xlabel('time (s)'); ylabel('number of observed landmarks'); ylim([0,8]);
% 
%         subplot(3,3,9);
%         stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
%         xlabel('time (s)'); ylabel('number of observed players'); ylim([0,8]);
%     end
% end

%% Evaluating and plotting MSE for each agent and Monte Carlo realization

% MSE for each time instant
mse_x_time = zeros(4, nIterations, R, M);
mse_y_time = zeros(4, nIterations, R, M);
mse_psi_time = zeros(4, nIterations, R, M);
mse_dist_time = zeros(4, nIterations, R, M);
for m = 1:M
    for r = 1:R
        for s = 1:4
            err_x = data{s,m}.TeamEst{r,s}.estimationError(1,:);
            mse_x_time(s, :, r, m) = err_x.^2;
            
            err_y = data{s,m}.TeamEst{r,s}.estimationError(2,:);
            mse_y_time(s, :, r, m) = err_y.^2;
            
            mse_dist_time(s, :, r, m) = mse_x_time(s, :, r, m) + mse_y_time(s, :, r, m);
            
            err_psi = data{s,m}.TeamEst{r,s}.estimationError(3,:);
            mse_psi_time(s, :, r, m) = err_psi.^2;
        end
    end
end
mse_dist_time = mse_x_time + mse_y_time;

% generating plots
for m = 1:M
    for r = 1:R
        
        if m <= 8
        % distance
        figure(10+r)
        subplot(3,3,m);
        for s = 1:4
            plot(time_vec, mse_dist_time(s,:,r,m));
            xlabel('time (s)');
            ylabel('combined mse in x-y (m^2)');
            grid on; hold all
        end
        legend(setupName);
        end
        
        subplot(6,3,15);
        stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
        xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);
        
        subplot(6,3,18);
        stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
        xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
        
        if m<=8
        % orientation
        figure(20+r)
        subplot(3,3,m);
        for s = 1:4
            plot(time_vec, mse_psi_time(s,:,r,m));
            xlabel('time (s)');
            ylabel('mse in \psi (rad^2)');
            grid on; hold all
        end
        legend(setupName);
        end
        
        subplot(6,3,15);
        stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
        xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);
        
        subplot(6,3,18);
        stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
        xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
        
    end
end

mse_dist_averaged = mean(mean(mse_dist_time,4),2);
mse_psi_averaged = mean(mean(mse_psi_time,4),2);

% plotting R figures, each with 2 subfigures (dist,psi), each with the four
% methods
for r = 1:R
    figure(50+r);
    subplot(1,3,1);
    for s = 1:4
        stem(s, mse_dist_averaged(s,r), 'filled', 'MarkerSize', 5); hold all;
    end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist');
    legend(setupName); grid on;
    ylim([0, 1]); xlim([0,5]);
    
    subplot(1,3,2);
    for s = 1:4
        stem(s, mse_psi_averaged(s,r), 'filled', 'MarkerSize', 5); hold all;
    end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi');
    legend(setupName); grid on;
    ylim([0, 0.05]); xlim([0,5]);
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
end


% mse_x_montecarlo = zeros(R, 4, M);
% for m = 1:M
%     for s = 1:4
%         for r = 1:R
%             err = data{s,m}.TeamEst{r,s}.estimationError(1,:);
%             mse_x_montecarlo(r,s,m) = mean(err.^2);
%         end
%     end
% end
% mse_x = mean(mse_x_montecarlo(:,:,1:end), 3);
% 
% mse_y_montecarlo = zeros(R, 4, M);
% for m = 1:M
%     for s = 1:4
%         for r = 1:R
%             err = data{s,m}.TeamEst{r,s}.estimationError(2,:);
%             mse_y_montecarlo(r,s,m) = mean(err.^2);
%         end
%     end
% end
% mse_y = mean(mse_y_montecarlo(:,:,1:end), 3);
% 
% mse_psi_montecarlo = zeros(R, 4, M);
% for m = 1:M
%     for s = 1:4
%         for r = 1:R
%             err = data{s,m}.TeamEst{r,s}.estimationError(3,:);
%             mse_psi_montecarlo(r,s,m) = mean(err.^2);
%         end
%     end
% end
% mse_psi = mean(mse_psi_montecarlo, 3);
% 
% % plotting R figures, each with 3 subfigures (x,y,psi), each with the four
% % methods
% for r = 1:R
%     figure(100+r);
%     subplot(1,3,1);
%     plot(1:4, mse_x(r,:));
%     xlabel('method'); ylabel('mse'); title('MC average, in x');
%     
%     subplot(1,3,2);
%     plot(1:4, mse_y(r,:));
%     xlabel('method'); ylabel('mse'); title('MC average, in y');
%     
%     subplot(1,3,3);
%     plot(1:4, mse_psi(r,:));
%     xlabel('method'); ylabel('mse'); title('MC average, in psi');
% end



%% Old plots, without Monte Carlo 

% %% Dashboard 1, with x-y-psi and number of observed allies
% 
% mse = zeros(data{1}.simConfig.R, 3, length(data));
% 
% for r = 1:data{1}.simConfig.R
%     for s = 1:size(setup,1)
%         data{s}.obs_landmarks = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
%         data{s}.obs_players = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
%     
%         % counting number of observed features by each robot
%         for i = 1:data{s}.simConfig.nIterations
%             try
%                 data{s}.obs_landmarks(r,i) = length(data{s}.Team{r}.measurementCamera{i}.goalsID) + length(data{s}.Team{r}.measurementCamera{i}.cornersID);
%             catch
% %                 disp('non-existent field in struct, skipping')
%             end
%             
%             try
%                 data{s}.obs_players(r,i) = length(data{s}.Team{r}.measurementCamera{i}.playersID);
%             catch
% %                 disp('non-existent field in struct, skipping')
%             end
%         end
%         
%         % checking whether the robots had seen the same number of obstacles
%         % (which is necessary, but not sufficient, to guarantee we are
%         % talking about the same scenario)
%         if s > 1
%             if data{s}.obs_landmarks ~= data{s-1}.obs_landmarks
%                 error('the same robot has not seen the same observations in different loaded data');
%             end
%         end
%         
%         % plotting x data of each estimation method in the setup
%         figure(10+r);
%         subplot(6,3,[1,2,4,5]);
%         plot(time_vec, data{s}.TeamEst{r,s}.estimationError(1,:));
%         hold all; grid on;
%         mse(r,1,s) = mean(data{s}.TeamEst{r,s}.estimationError(1,:).^2);
%         if s == length(data)
%             legstr = {};
%             for s = 1:length(data)
%                 legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,1,s)) ];
%             end
%             xlabel('time (s)'); ylabel('error in x (m)'); legend(legstr); ylim([-2,2]);
%             title(['agent ' num2str(r)]);
%         end
%         
%         subplot(6,3,[7,8,10,11]);
%         plot(time_vec, data{s}.TeamEst{r,s}.estimationError(2,:))
%         hold all; grid on;
%         mse(r,2,s) = mean(data{s}.TeamEst{r,s}.estimationError(2,:).^2);
%         if s == length(data)
%             legstr = {};
%             for s = 1:length(data)
%                 legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,2,s)) ];
%             end
%             xlabel('time (s)'); ylabel('error in y (m)'); legend(legstr); ylim([-2,2]);
%         end
%         
%         subplot(6,3,[13,14,16,17]);
%         plot(time_vec, data{s}.TeamEst{r,s}.estimationError(3,:))
%         hold all; grid on;
%         mse(r,3,s) = mean(data{s}.TeamEst{r,s}.estimationError(3,:).^2);
%         if s == length(data)
%             legstr = {};
%             for s = 1:length(data)
%                 legstr{s} = [setupName{s} ' - MSE: ' num2str(mse(r,3,s)) ];
%             end
%             xlabel('time (s)'); ylabel('error in psi (rad)'); legend(legstr); ylim([-0.5,0.5]);
%         end
%     end
%     
%     subplot(6,3,[3,6,9]);
%     stem(time_vec, data{1}.obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
%     xlabel('time (s)'); ylabel('number of observed landmarks'); ylim([0,8]);
%     
%     subplot(6,3,[12,15,18]);
%     stem(time_vec, data{1}.obs_players(r,:), 'filled', 'MarkerSize', 3);
%     xlabel('time (s)'); ylabel('number of observed players'); ylim([0,data{1}.simConfig.R]);
% end
% 
% %% Dashboard 2, with d = sqrt(x^2 + y^2) and quality of allies' beliefs
% 
% rmse = zeros(data{1}.simConfig.R, 2, length(data));
% 
% for r = 1:data{1}.simConfig.R
%             
%     figure(30+r)
%     
%     for s = 1:size(setup,1)
%         data{s}.obs_landmarks = zeros(data{s}.simConfig.R, data{s}.simConfig.nIterations);
%     
%         % counting number of observed features by each robot
%         for i = 1:data{s}.simConfig.nIterations
%             try
%                 data{s}.obs_landmarks(r,i) = length(data{s}.Team{r}.measurementCamera{i}.goalsID) + length(data{s}.Team{r}.measurementCamera{i}.cornersID);
%             catch
%             end
%         end
%         
%         % checking whether the robots had seen the same number of obstacles
%         % (which is necessary, but not sufficient, to guarantee we are
%         % talking about the same scenario)
%         if s > 1
%             if data{s}.obs_landmarks ~= data{s-1}.obs_landmarks
%                 error('the same robot has not seen the same observations in different loaded data');
%             end
%         end
%         
%         subplot(8,4,[1,2,5,6,9,10,13,14]);
%         rmse_time = sqrt(mean(data{s}.TeamEst{r,s}.estimationError(1:2,:).^2, 1));
%         plot(time_vec, rmse_time);
%         hold all; grid on
%         rmse(r,1,s) = mean(rmse_time);
%         if s == length(data)
%             legstr = {};
%             for s = 1:length(data)
%                 legstr{s} = [setupName{s} ' - RMSE: ' num2str(rmse(r,1,s)) ];
%             end
%             xlabel('time (s)'); ylabel('distance error (m)'); legend(legstr); ylim([0,2]);
%             title(['agent ' num2str(r)]);
%         end        
%         
%         subplot(8,4,[17,18,21,22,25,26,29,30]);
%         plot(time_vec, data{s}.TeamEst{r,s}.estimationError(3,:));
%         hold all; grid on
%         rmse(r,2,s) = sqrt(mean(data{s}.TeamEst{r,s}.estimationError(3,:).^2));
%         if s == length(data)
%             legstr = {};
%             for s = 1:length(data)
%                 legstr{s} = [setupName{s} ' - RMSE: ' num2str(rmse(r,2,s)) ];
%             end
%             xlabel('time (s)'); ylabel('error in psi (rad)'); legend(legstr); ylim([-0.5,0.5]);
%         end
%     end
%     
%     subplot(8,4,[3,4,7,8]);
%     stem(time_vec, data{1}.obs_landmarks(r,:), 'filled', 'MarkerSize', 3);
%     xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);
%     
%     colormap jet
%     colorbar
% end
% 
% %% Plotting the quality of the belief heard from the broadcasting ally
% for r = 1:data{1}.simConfig.R
%     figure(30+r)
%     
%     cases = {'PLHA', 'PLH'};
%     for c = 1:size(cases, 2)
%         s = find(ismember(setupName, cases{c}));
%         
%         heard_ally = zeros(data{s}.simConfig.R, length(time_vec));
%         errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
%         for i = 2:length(time_vec)
%             broadcaster = data{s}.TeamEst{r,s}.heardLocal(i,2);
%             if broadcaster ~= 0
%                 heard_ally(r, i) = broadcaster;
%                 errorBeliefReal(r, i) = sqrt(mean(data{s}.TeamEst{broadcaster,s}.estimationError(1:2,i).^2));
%             end
%         end
%         
%         subplot_tiles = [11,15,19] + (c-1);
%         subplot(8,4,subplot_tiles);        
%         %preparing scatter plot
%         x_sc = []; y_sc = []; z_sc = [];
%         for i = 1:length(time_vec)
%             for r1 = 1:data{s}.simConfig.R
%                 if heard_ally(r1,i) > 0
%                     x_sc = [x_sc, time_vec(i)];
%                     y_sc = [y_sc, heard_ally(r1,i)];
%                     z_sc = [z_sc, errorBeliefReal(r1,i)];
%                 end
%             end
%         end
%         
%         scatter3(x_sc, y_sc, z_sc, [], z_sc, 'filled')
%         caxis([0,1])
%         xlabel('time (s)'); ylabel(['heard ally, ' cases{c}]); zlabel('ally self position error (m)')
%         ylim([0,data{s}.simConfig.R]); zlim([0,1])
% %         title(['robot: ', num2str(r), ', method: ', cases{c}])
%         
%     end
% end
% 
% %% Plotting the quality of the belief used to assimilate the ally camera measurement
% % Circle color represents the distance error; the belief standard
% % deviation, if saved, could be in dashed line
% 
% for r = 1:data{1}.simConfig.R
%     figure(30+r)
%     
%     cases = {'PLHA', 'PLA'};
%     for c = 1:size(cases, 2)
%         s = find(ismember(setupName, cases{c}));
%     
%         observed_allies = zeros(data{s}.simConfig.R, length(time_vec));
%         errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
%         for i = 2:length(time_vec)
%             try
%                 totalAllies = length(data{s}.Team{r}.measurementCamera{i}.playersID);
%             catch
%                 totalAllies = 0;
%             end
%             for a = 1:totalAllies
%                 foundAlly = data{s}.Team{r}.measurementCamera{i}.playersID(a);
%                 observed_allies(foundAlly, i) = foundAlly;
%                 
%                 % estimation error (rmse) of the position of agent "a" at
%                 % time i
%                 errorBeliefReal(foundAlly, i) = sqrt(mean(data{s}.TeamEst{foundAlly,s}.estimationError(1:2,i).^2));
%             end
%         end
%         
% %         % evaluating the error between: the belief agent "r" has about "a" at
% %         % time i (I don't have it saved...); and the real position of agent "a" at time i
% %         % alternately: the rmse of the position of agent "a" at time i
% %         errorBeliefReal = zeros(data{s}.simConfig.R, length(time_vec));
% %         for i = 2:length(time_vec)
% %             totalAllies = length(data{s}.Team{r}.measurementCamera{i}.playersID);
% %             for a = 1:totalAllies
% %                 foundAlly = data{s}.Team{r}.measurementCamera{i}.playersID(a);
% %                 
% % %                 beliefRfromA_mean = data{s}.TeamEst{r,s}.notSelfbeliefHistory{4}(i, 1:2);
% % %                 timeOfBelief = data{s}.TeamEst{r,s}.beliefHistory{1}(i);
% % %                 realPositionA = data{s}.TeamEst{foundAlly,s}.realData(i, 1:2);
% % %                 errorBeliefReal(foundAlly, i) = norm(beliefRfromA_mean - realPositionA);
% % 
% %                 % estimation error (rmse) of the position of agent "a" at
% %                 % time i
% %                 errorBeliefReal(foundAlly, i) = sqrt(mean(data{s}.TeamEst{foundAlly,s}.estimationError(1:2,i).^2));
% %             end
% %         end
%         
%         subplot_tiles = [23,27,31] + (c-1);
%         subplot(8,4,subplot_tiles);
%         
%         %preparing scatter plot
%         x_sc = []; y_sc = []; z_sc = [];
%         for i = 1:length(time_vec)
%             for r1 = 1:data{s}.simConfig.R
%                 if observed_allies(r1,i) > 0
%                     x_sc = [x_sc, time_vec(i)];
%                     y_sc = [y_sc, observed_allies(r1,i)];
%                     z_sc = [z_sc, errorBeliefReal(r1,i)];
%                 end
%             end
%         end
%         
%         scatter3(x_sc, y_sc, z_sc, [], z_sc, 'filled')
%         caxis([0,1])
%         xlabel('time (s)'); ylabel(['observed ally, ' cases{c}]); zlabel('ally self position error (m)')
%         ylim([0,data{s}.simConfig.R]); zlim([0,1])
% %         colormap jet
% %         colorbar
%     end
%     
% end
% 
% %% plotting overall RMSE information
% figure(30);
% measurementStr = {'distance', 'pose'};
% for state = 1:2
%     legstr = {};
%     for s = 1:length(data)
%         subplot(1,2,state);
%         stem(s, mean(rmse(:,state,s)), 's'); hold all; grid on;
% %         semilogy(s, mean(rmse(:,state,s)), 's'); hold all; grid on;
%         legstr{s} = setupName{s};
%     end
%     xlim([0,length(data)+1]);
%     ylim([0, 0.5]); ylabel(['rmse, '  measurementStr{state}]); legend(legstr);
% end
% 
% %% plotting overall MSE information
% figure(10);
% for state = 1:3
%     legstr = {};
%     for s = 1:length(data)
%         subplot(1,3,state);
%         semilogy(s, mean(mse(:,state,s)), 's'); hold all; grid on;
%         legstr{s} = setupName{s};
%     end
%     xlim([0,length(data)+1]); 
%     ylim([1e-3, 1]); ylabel(['mse, state ' num2str(state)]); legend(legstr);
% end
% 
% %% Plotting eigenvalues representing the numeric state of the filter
% % covariance matrix info plot (max eigen, min eigen, rcond)
% maxEigVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));
% minEigVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));
% condVec = zeros(data{1}.simConfig.R, data{1}.simConfig.nIterations, length(data));
% 
% for r = 1:data{1}.simConfig.R
%     for s = 1:size(setup,1)
%         for i = 1:data{1}.simConfig.nIterations
%             cov = squeeze(data{s}.TeamEst{r,s}.beliefHistory{5}(i,:,:));
%             cov = (cov + cov.')/2;
%             eigens = eig( cov );
%             maxEigVec(r,i,s) = max(eigens);
%             minEigVec(r,i,s) = min(eigens);
%             condVec(r,i,s) = maxEigVec(r,i,s)/minEigVec(r,i,s);
%             if isnan(condVec(r,i,s))
%                 condVec(r,i,s) = 0;
%             end                
%         end
%     end    
% end
% 
% for r = 1:data{1}.simConfig.R
%     figure(70+r);
%     
%     for s = 1:size(setup,1)
%         subplot(3,1,1);
%         semilogy( time_vec, squeeze(maxEigVec(r,:,s)), 'x' );
%         hold all; grid on;
%         if s == size(setup,1)
%             xlabel('time (s)'); ylabel('max eigenvalue'); legend(setupName);
%             title(['agent: ' num2str(r)]);
%         end
%         
%         subplot(3,1,2)
%         semilogy( time_vec, squeeze(minEigVec(r,:,s)), 'x' );
%         hold all; grid on;
%         if s == size(setup,1)
%             xlabel('time (s)'); ylabel('min eigenvalue'); legend(setupName);
%         end
%         
%         subplot(3,1,3)
%         semilogy( time_vec, squeeze(condVec(r,:,s)), 'x' );
%         hold all; grid on;
%         if s == size(setup,1)
%             xlabel('time (s)'); ylabel('ratio min/max eigenvalues'); legend(setupName);
%         end
%         
%     end
% end

