clear all;

addpath('drawfield_functions');

filename = 'output_final/138_PL_MCtraj001.mat';
load(filename)

fieldfile = 'fieldParamsSoccer3D.txt';
field = readFieldParams(fieldfile);
landmarks = generateLandmarkList(field);
figure(2); close; figure(2) 
drawField(landmarks); hold on;

r1data = savedvariable.TeamEstSave{1,1};

figure(2);

time = 3:3:75;
time = [51,75];

plot(r1data.realData(time,1), r1data.realData(time,2), 'x' ); hold on

for n = time
    x = r1data.realData(n,1);
    y = r1data.realData(n,2);
    psi = r1data.realData(n,3);
    d = 0.5;
    plot([x, x+d*cos(psi)], [y, y+d*sin(psi)]);
end

for n = time
    disp(['n = ', num2str(n)])
            
    x1 = r1data.realData(n,:).'; 
    
    disp('Corners and Goals')
    for c = 1:length(r1data.observedGlobal{n}.cornersID)                
        disp('Corner ID: ')
        cornerID = r1data.observedGlobal{n}.cornersID(c);
        disp(cornerID)
        
        x2 = landmarks.cornerList(cornerID,:).';
        
        disp('Ground truth robot: ')
        disp( x1.' );
        
        disp('Ground truth landmark: ')
        disp( x2.' );
        
        disp('Measurement with noise, in global: ')
        disp(r1data.observedGlobal{n}.corners(c,:))
        
        disp('Measurement without noise, in local: ')
        disp( [hRho(x1, x2), hPhi(x1, x2)] );
                
        obs12 = r1data.measurementCamera{n}.corners(c,:).';
        
        disp('Measurements with noise, in local: ')
        disp( obs12 );
        
        disp('Likelihood (using GT position of robot): ');
        disp(pObs(obs12, x1, x2, 0, r1data.rangeVariancePar, 0, r1data.bearingVariance));
    end
    
    for g = 1:length(r1data.observedGlobal{n}.goalsID)
        disp('Goal ID: ')
        goalID = r1data.observedGlobal{n}.goalsID(g);
        disp(goalID)
        
        x2 = landmarks.goalList(goalID,:).';
        
        disp('Ground truth robot: ')
        disp( x1.' );
        
        disp('Ground truth landmark: ')
        disp( x2.' );
        
        disp('Measurement with noise, in global: ')
        disp(r1data.observedGlobal{n}.goals(g,:))
        
        disp('Measurement without noise, in local: ')
        disp( [hRho(x1, x2), hPhi(x1, x2)] );
                
        obs12 = r1data.measurementCamera{n}.goals(g,:).';
        
        disp('Measurements with noise, in local: ')
        disp( obs12 );
        
        disp('Likelihood (using GT position of robot): ');
        disp(pObs(obs12, x1, x2, 0, r1data.rangeVariancePar, 0, r1data.bearingVariance));
    end
    
end