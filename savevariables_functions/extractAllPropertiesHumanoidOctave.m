%% function to return a struct containing only the properties of object Humanoid.
%% To be used only in Octave, due to a limitation in how it deals with saving objects to a file

function structProperties = extractAllPropertiesHumanoidOctave(humanoidObj)
 
        structProperties.ID = humanoidObj.ID;
        structProperties.x0 = humanoidObj.x0;
        structProperties.y0 = humanoidObj.y0;
        structProperties.psi0 = humanoidObj.psi0;
        structProperties.v_x = humanoidObj.v_x;
        structProperties.v_y = humanoidObj.v_y;
        structProperties.v_psi = humanoidObj.v_psi;
        structProperties.rangeVariancePar = humanoidObj.rangeVariancePar;
        structProperties.bearingVariance = humanoidObj.bearingVariance;
        structProperties.DoAVariance = humanoidObj.DoAVariance;
        structProperties.odometryXVariance = humanoidObj.odometryXVariance;
        structProperties.odometryYVariance = humanoidObj.odometryYVariance;
        structProperties.odometryPsiVariance = humanoidObj.odometryPsiVariance;
        structProperties.odometryCovariance = humanoidObj.odometryCovariance;
        structProperties.visionAngle = humanoidObj.visionAngle;
        structProperties.realData = humanoidObj.realData;
        structProperties.odometryData = humanoidObj.odometryData;
        structProperties.displace = humanoidObj.displace;
        structProperties.displaceInLocal = humanoidObj.displaceInLocal;
        structProperties.u = humanoidObj.u;
        structProperties.landmarks = humanoidObj.landmarks;
        structProperties.M = humanoidObj.M;
        structProperties.heardGlobal = humanoidObj.heardGlobal;
        structProperties.heardLocal = humanoidObj.heardLocal;
        structProperties.observedGlobal = humanoidObj.observedGlobal;
        structProperties.measurementCamera = humanoidObj.measurementCamera;
        structProperties.time = humanoidObj.time;
        structProperties.T = humanoidObj.T;
        structProperties.nIterations = humanoidObj.nIterations;
        structProperties.R = humanoidObj.R;
        structProperties.config = humanoidObj.config;
        structProperties.Np = humanoidObj.Np;
        structProperties.beliefsGMM = humanoidObj.beliefsGMM;
        structProperties.mess_B_Particles = humanoidObj.mess_B_Particles;
        structProperties.mess_P_Particles = humanoidObj.mess_P_Particles;
        structProperties.mess_L_Particles = humanoidObj.mess_L_Particles;
        structProperties.mess_H_Particles = humanoidObj.mess_H_Particles;
        structProperties.mess_A_Particles = humanoidObj.mess_A_Particles;
        structProperties.mess_Trans_Particles = humanoidObj.mess_Trans_Particles;         
        structProperties.estimationError = humanoidObj.estimationError;        
        structProperties.beliefHistory = humanoidObj.beliefHistory;
 
 end