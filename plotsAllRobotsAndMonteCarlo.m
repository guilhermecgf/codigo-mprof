%% Script to plot the plots desired in the paper.
clear all; close all

addpath('plotresults_functions')
% load('output_final/2020022-novomc/novomc-2627fev.mat');
% load('output_final/sim_cluster_bu/sim_cluster_bu513.mat');
% load('output_final/teste1/sim_cluster_bu553.mat');
% load('output_final/teste2/sim_cluster_bu692.mat');
% load('output_final/sim_itandroids/sim119.mat');
% load('output_final/sim_itandroids_2/sim13.mat');
% load('output_final/sim_itandroids_3/sim17.mat');
% load('output_final/sim_itandroids_4/sim19.mat');
% load('output_final/sim_itandroids_5/sim23.mat');
% load('output_final/sim_PL_only/sim641.mat'); % PL only, noise of mine, 1000 particles
% load('output_final/sim_PL_only/sim953.mat'); % PL only, noise itandroids, 1000 particles

% load('output_final/sim29/sim29.mat');
% load('output_final/sim31/sim31.mat');
load('output_final/sim421.mat');


obs_landmarks_average = squeeze(mean(obs_landmarks_total,1));
obs_players_average = squeeze(mean(obs_players_total,1));

%% Convert to cell format
mse_dist = cell(R,4);
mse_psi = cell(R,4);

for r = 1:R
    for s = 1:4
        mse_dist{r,s} = mse_dist_time_total(:,:,r,s);
        mse_psi{r,s} = mse_psi_time_total(:,:,r,s);
    end
end

%% If desired, remove outliers
boolRemoveOutliers = false;
if boolRemoveOutliers
    % k is number of standard deviations from the mean; internally,
    % function treats with median and absolutw error
    k = 2;
    [mse_dist, mse_psi] = removeOutliers(mse_dist_time_total, mse_psi_time_total, err_x_time_total, err_y_time_total, err_psi_time_total, k, R, M, nIterations);
end

%% Another way of removing outliers: considering only MSE averaged over all time instants
boolRemoveOutliers = false;
if boolRemoveOutliers
    k = 4;
    [mse_dist, mse_psi, ind_no_outliers_dist, ind_no_outliers_psi] = removeOutliersNew(mse_dist, mse_psi, k);       
    ind_no_outliers_dist, ind_no_outliers_psi
end

%%
% Para plotar, vamos deixar o plot do PLHA por �ltimo.
setupNamePlot = {'PL', 'PLH', 'PLA', 'PLHA'};
for r = 1:R
    aux = mse_dist{r,3};
    mse_dist{r,3} = mse_dist{r,4};
    mse_dist{r,4} = aux;
end

for r = 1:R
    aux = mse_psi{r,3};
    mse_psi{r,3} = mse_psi{r,4};
    mse_psi{r,4} = aux;
end

%% Plotting all robots, error vs. time, all methods in one plot

linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 1.4];

figure(20);
for r = 1:R
    
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( mse_dist{r,s}, 1);
        var_mse = var( mse_dist{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;
    %     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
    %     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);

        plot(time_vec, mean_mse, [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
%     xlabel('Time (s)'); ylabel('Position RMSE (m)');
    xlabel('Time (s)'); ylabel('Position MSE (m^2)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
    subplot(2,5,r+5);
%     total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    if R == 1
        total_camera_obs = [obs_landmarks_average, obs_players_average];
    else
        total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    end
    H = bar(time_vec, total_camera_obs, 'stacked');
    colors = [0, 0.75, 0; 0.75, 0, 0.75];
    for k = 1:2
        set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
    end
    xlabel('Time (s)'); ylabel('Number of camera observations');
    legend('Landmarks', 'Players'); ylim([0,8]);

end

%% Plotting each robot with the four methods and all Monte Carlos
for r = 1:R
    figure(20+r); close
end

for r = 1:R
    figure(20+r);
    for s = 1:4
        aux = mse_dist{r,s};
        for m = 1:size(aux,1)
            if m <=10
                subplot(2,5,m);
                plot(time_vec, aux(m,:));
                ylim([0, 1]);
                xlim([0, time_vec(end)]);            
                xlabel('Time (s)'); ylabel('Position MSE (m^2)');
                hold all
            end
        end
    end
end

%% Plotting all robots, error vs. time, all methods in one plot

linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 0.1];

figure(30);
for r = 1:R
    
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( mse_psi{r,s}, 1);
        var_mse = var( mse_psi{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;
    %     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
    %     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);

        plot(time_vec, mean_mse, [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
%     xlabel('Time (s)'); ylabel('Position RMSE (m)');
    xlabel('Time (s)'); ylabel('Orientation MSE (rad^2)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
    subplot(2,5,r+5);
%     total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    if R == 1
        total_camera_obs = [obs_landmarks_average, obs_players_average];
    else
        total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    end
    H = bar(time_vec, total_camera_obs, 'stacked');
    colors = [0, 0.75, 0; 0.75, 0, 0.75];
    for k = 1:2
        set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
    end
    xlabel('Time (s)'); ylabel('Number of camera observations');
    legend('Landmarks', 'Players'); ylim([0,8]);

end

%% Plotting each robot with the four methods and all Monte Carlos
for r = 1:R
    figure(30+r); close
end

for r = 1:R
    figure(30+r);
    for s = 1:4
        aux = mse_psi{r,s};
        for m = 1:size(aux,1)
            if m <=10
                subplot(2,5,m);
                plot(time_vec, aux(m,:));
                ylim(ylim_dist_param);
                xlim([0, time_vec(end)]);            
                xlabel('Time (s)'); ylabel('Orientation MSE (rad^2)');
                hold all
            end
        end
    end
end

% %% Plotting the MSE, averaged over time, for each robot, monte carlo, setup
% % subplot(2,3,r)
% figure(40); close; figure(40);
% 
% for r = 1:R
%     subplot(2,3,r);
%     for s = 1:4
%         aux = mse_dist{r,s};
%         plot(1:M, mean(aux,2));
%         hold on;
%     end
% %     ylim([0, 1]);
%     xlabel('Monte Carlo realization'); ylabel('Position MSE (m^2)');
%     title(['Robot ' num2str(r)]);
%     legend(setupNamePlot)
% end


