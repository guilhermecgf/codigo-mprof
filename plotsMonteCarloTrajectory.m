%% Loading variables
% For Massive amount of Monte Carlo Simulations, reading one file at a time
clear all; close all
addpath('plotresults_functions')
%% ------------------------------------------------------------------------------
% First set of simulations (from ITAndroids computer)
% config 1. R = 5, M = 1000, Np = 600, belief = extrap, cam = 3, time = 6, T = 0.2 
% new monte carlo calc
setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

estID_vec = [135, 161, 163, 165, 199, 655, 689, 695, 886, 928];
% estID_vec = [135, 161];
mc_indices = cell(1, length(estID_vec));
mc_indices{1} = 1:50;
mc_indices{2} = 1:50;
mc_indices{3} = 1:50;
mc_indices{4} = 1:50;
mc_indices{5} = 1:50;
mc_indices{6} = 1:50;
mc_indices{7} = 1:50;
mc_indices{8} = 1:50;
mc_indices{9} = 1:50;
mc_indices{10} = 1:50;

M = 0;
for e = 1:length(estID_vec)
    M = M + length(mc_indices{e});
end
err_x_time_total = [];
err_y_time_total = [];
err_psi_time_total = [];

mse_x_time_total = [];
mse_y_time_total = [];
mse_psi_time_total = [];
mse_dist_time_total = [];

obs_landmarks_total = [];
obs_players_total = [];

data = cell(1,4);

for e = 1:length(estID_vec)
    e
    
    strbase26 = ['output_final/2020022-novomc/20200226-' num2str(estID_vec(e))];
    strbase27 = ['output_final/2020022-novomc/20200227-' num2str(estID_vec(e))];
    m_vec = mc_indices{e};
    M_each = length(m_vec);
    
    for m = 1:M_each
        
        if (e == 3 || e == 5 || e == 6)
            if m <10
                strbase = strbase26;
            else
                strbase = strbase27;
            end
        else
            if m <= 10
                strbase = strbase26;
            else
                strbase = strbase27;
            end
        end
        
        for s = 1:4
            data{s} = load([strbase '_' setupName{s} '_MCtraj' num2str(m,'%03d') '.mat']);
        end
%         data{2} = load([strbase '_PLH_MCtraj' num2str(m,'%03d') '.mat']);
%         data{3} = load([strbase '_PLHA_MCtraj' num2str(m,'%03d') '.mat']);
%         data{4} = load([strbase '_PLA_MCtraj' num2str(m,'%03d') '.mat']);
    
        if m == 1
            R = data{1}.simConfig.R;
            nIterations = data{1}.simConfig.nIterations;
            T = data{1}.simConfig.T;
            time_vec = T * [1:nIterations];
            
            obs_landmarks = zeros(M_each, R, nIterations);
            obs_players = zeros(M_each, R, nIterations);

            err_x_time = zeros(M_each, nIterations, R, 4);
            err_y_time = zeros(M_each, nIterations, R, 4);
            err_psi_time = zeros(M_each, nIterations, R, 4);

            mse_x_time = zeros(M_each, nIterations, R, 4);
            mse_y_time = zeros(M_each, nIterations, R, 4);
            mse_psi_time = zeros(M_each, nIterations, R, 4);
            mse_dist_time = zeros(M_each, nIterations, R, 4);
        end 

        for r=1:R
            for i=2:nIterations
                try
                    obs_landmarks(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.goalsID) + length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.cornersID);
                    obs_players(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.playersID);
                catch
                    %disp(['no camera observations at time instant ' num2str(i)]);
                end
            end
        end
        
        for r = 1:R
            for s = 1:4
                err_x = data{s}.TeamEstSave{r,s}.estimationError(1,:);
                err_x_time(m, :, r, s) = err_x;
                mse_x_time(m, :, r, s) = err_x.^2;

                err_y = data{s}.TeamEstSave{r,s}.estimationError(2,:);
                err_y_time(m, :, r, s) = err_y;
                mse_y_time(m, :, r, s) = err_y.^2;

                mse_dist_time(m, :, r, s) = mse_x_time(m, :, r, s) + mse_y_time(m, :, r, s);

                err_psi = data{s}.TeamEstSave{r,s}.estimationError(3,:);
                err_psi_time(m, :, r, s) = err_psi;
                mse_psi_time(m, :, r, s) = err_psi.^2;
            end
        end
    end
    
    err_x_time_total = cat(1, err_x_time_total, err_x_time);
    err_y_time_total = cat(1, err_y_time_total, err_y_time);
    err_psi_time_total = cat(1, err_psi_time_total, err_psi_time); 
    
    mse_x_time_total = cat(1, mse_x_time_total, mse_x_time);
    mse_y_time_total = cat(1, mse_y_time_total, mse_y_time);
    mse_dist_time_total = cat(1, mse_dist_time_total, mse_dist_time);
    mse_psi_time_total = cat(1, mse_psi_time_total, mse_psi_time);
    
    obs_landmarks_total = cat(1, obs_landmarks_total, obs_landmarks);
    obs_players_total = cat(1, obs_players_total, obs_players);
    
end

save('output_final/2020022-novomc/novomc-2627fev.mat');

%% --------------------------------------------------
clear all; close all
% Second set of simulations: from BU cluster
% modifications: T = 0.02, smaller noise variances, less total time, no
% noise on input sequence (not odometry anymore, but deterministic input

setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

obs_landmarks_total = [];
obs_players_total = [];

err_x_time_total = [];
err_y_time_total = [];
err_psi_time_total = [];

mse_x_time_total = [];
mse_y_time_total = [];
mse_psi_time_total = [];
mse_dist_time_total = [];

data = cell(1,4);

% config BU
ID = 513;
M = 123;
strbase = ['output_final/sim_cluster_bu/' num2str(ID)];

% config teste
ID = 553;
M = 10;
strbase = ['output_final/teste1/' num2str(ID)];

% config teste
ID = 692;
M = 10;
strbase = ['output_final/teste2/' num2str(ID)];

% config itandroids
ID = 119;
M = 31;
strbase = ['output_final/sim_itandroids/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.2
ID = 13;
M = 100;
strbase = ['output_final/sim_itandroids_2/' num2str(ID)];

% config itandroids, trajet�ria e parametros antigos, T = 0.02, ruido muito baixo na camera
ID = 17;
M = 37;
strbase = ['output_final/sim_itandroids_3/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.02, ruido medio
% na camera
ID = 19;
M = 76;
strbase = ['output_final/sim_itandroids_4/' num2str(ID)];

% config itandroids, trajetoria e parametros antigos, T = 0.02, ruido medio
% na camera
ID = 23;
M = 18;
strbase = ['output_final/sim_itandroids_5/' num2str(ID)];

% teste apenas PL
setupName = {'PL', 'PL', 'PL', 'PL'};
ID = 641;
M = 10;
strbase = ['output_final/' num2str(ID)];

ID = 953;
M = 10;
strbase = ['output_final/' num2str(ID)];



for m = 1:M
    for s = 1:4
        aux = load([strbase '_' setupName{s} '_MCtraj' num2str(m,'%03d') '.mat']);
        data{s} = aux.savedvariable;
    end     
        
    % APENAS PARA O TESTE S� DE PL
    for s = 2:4
        R = data{1}.simConfig.R;
        for r = 1:R
            data{s}.TeamEstSave{r,s} = data{1}.TeamEstSave{r,1};
        end
    end
    % COMENTAR DEPOIS
    
    if m == 1
        R = data{1}.simConfig.R;
        nIterations = data{1}.simConfig.nIterations;
        T = data{1}.simConfig.T;
        time_vec = T * [1:nIterations];

        obs_landmarks = zeros(M, R, nIterations);
        obs_players = zeros(M, R, nIterations);

        err_x_time = zeros(M, nIterations, R, 4);
        err_y_time = zeros(M, nIterations, R, 4);
        err_psi_time = zeros(M, nIterations, R, 4);

        mse_x_time = zeros(M, nIterations, R, 4);
        mse_y_time = zeros(M, nIterations, R, 4);
        mse_psi_time = zeros(M, nIterations, R, 4);
        mse_dist_time = zeros(M, nIterations, R, 4);

        
    end 

    for r=1:R
        for i=2:nIterations
            try
                obs_landmarks(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.goalsID) + length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.cornersID);
                obs_players(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.playersID);
            catch
                %disp(['no camera observations at time instant ' num2str(i)]);
            end
        end
    end

    for r = 1:R
        for s = 1:4
            err_x = data{s}.TeamEstSave{r,s}.estimationError(1,:);
            err_x_time(m, :, r, s) = err_x;
            mse_x_time(m, :, r, s) = err_x.^2;

            err_y = data{s}.TeamEstSave{r,s}.estimationError(2,:);
            err_y_time(m, :, r, s) = err_y;
            mse_y_time(m, :, r, s) = err_y.^2;

            mse_dist_time(m, :, r, s) = mse_x_time(m, :, r, s) + mse_y_time(m, :, r, s);

            err_psi = data{s}.TeamEstSave{r,s}.estimationError(3,:);
            err_psi_time(m, :, r, s) = err_psi;
            mse_psi_time(m, :, r, s) = err_psi.^2;
        end
    end
    
end

obs_landmarks_total = obs_landmarks;
obs_players_total = obs_players;

err_x_time_total = err_x_time;
err_y_time_total = err_y_time;
err_psi_time_total = err_psi_time;

mse_x_time_total = mse_x_time;
mse_y_time_total = mse_y_time;
mse_psi_time_total = mse_psi_time;
mse_dist_time_total = mse_dist_time;

% save(['output_final/sim_cluster_bu/sim_cluster_bu' num2str(ID) '.mat']);
% save(['output_final/sim_itandroids_5/sim' num2str(ID) '.mat']);
save(['output_final/sim' num2str(ID) '.mat']);

%% Opening files
clear all; close all
% load('output_final/2020022-novomc/novomc-2627fev.mat');
% load('output_final/sim_cluster_bu/sim_cluster_bu513.mat');
% load('output_final/teste1/sim_cluster_bu553.mat');
% load('output_final/teste2/sim_cluster_bu692.mat');
% load('output_final/sim_itandroids/sim119.mat');
% load('output_final/sim_itandroids_2/sim13.mat');
% load('output_final/sim_itandroids_3/sim17.mat');
% load('output_final/sim_itandroids_4/sim19.mat');
% load('output_final/sim_itandroids_5/sim23.mat');
% load('output_final/sim641.mat'); % PL only, noise of mine, 1000 particles
load('output_final/sim953.mat'); % PL only, noise itandroids, 1000 particles

%% Generating plots
% choosing only a subset of all MC realizations
subset_mc = 1:M;


mse_dist_time_sub = mse_dist_time_total(subset_mc, :, :, :);
mse_psi_time_sub = mse_psi_time_total(subset_mc, :, :, :);

mse_dist_time_mean = mean(mse_dist_time_sub, 2); 
mse_psi_time_mean = mean(mse_psi_time_sub, 2);

mse_dist_mc_time_mean = squeeze(mean(mse_dist_time_mean, 1));
mse_psi_mc_time_mean = squeeze(mean(mse_psi_time_mean, 1));

mse_dist_mc_time_var = squeeze(var(mse_dist_time_mean, 0, 1));
mse_psi_mc_time_var = squeeze(var(mse_psi_time_mean, 0, 1));

mse_dist_averaged = squeeze(mean(mean(mse_dist_time,1),2));
mse_psi_averaged = squeeze(mean(mean(mse_psi_time,1),2));

% average number of observations
obs_landmarks_average = squeeze(mean(obs_landmarks_total,1));
obs_players_average = squeeze(mean(obs_players_total,1));

% plotting R figures, each with 4 subfigures (dist, psi, number of landmarks, number of allies),
% each with the four methods
% for r = 1:R
%     figure(50+r);
%     subplot(1,3,1);
%     for s = 1:4
%         stem(s, mse_dist_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
%     xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist');
%     legend(setupName); grid on;
%     ylim([0, 1]); xlim([0,5]);
%     
%     subplot(1,3,2);
%     for s = 1:4
%         stem(s, mse_psi_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
%     xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi');
%     legend(setupName); grid on;
%     ylim([0, 0.05]); xlim([0,5]);
%         
%     subplot(2,3,3);
%     stem(time_vec, obs_landmarks_average(r,:), 'filled', 'MarkerSize', 3);
%     xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);
% 
%     subplot(2,3,6);
%     stem(time_vec, obs_players_average(r,:), 'filled', 'MarkerSize', 3);
%     xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
% end

% plotting R figures, each with 4 subfigures (dist, psi, number of landmarks, number of allies),
% each with the four methods
for r = 1:R
    figure(30+r);
    subplot(2,3,1);
    errorbar(1:4, mse_dist_mc_time_mean(r,:), mse_dist_mc_time_var(r,:));
%     for s = 1:4
%         stem(s, mse_dist_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist');
    legend(setupName); grid on;
    ylim([0, 1]); xlim([0,5]);
    
    subplot(2,3,2);
    errorbar(1:4, mse_psi_mc_time_mean(r,:), mse_psi_mc_time_var(r,:));
%     for s = 1:4
%         stem(s, mse_psi_averaged(r,s), 'filled', 'MarkerSize', 5); hold all;
%     end
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi');
    legend(setupName); grid on;
    ylim([0, 0.05]); xlim([0,5]);
        
    subplot(2,3,3);
    stem(time_vec, obs_landmarks_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,3,6);
    stem(time_vec, obs_players_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
end

% plotting mse versus time for some agents
r_vec = 1:5;
for r = r_vec
    mse_dist_time_sub_r = mse_dist_time_sub(:,:,r,:);
    mse_psi_time_sub_r = mse_psi_time_sub(:,:,r,:);
    
    mse_dist_time_sub_r_mean = squeeze(mean(mse_dist_time_sub_r, 1));
    mse_psi_time_sub_r_mean = squeeze(mean(mse_psi_time_sub_r, 1));
    
    mse_dist_time_sub_r_var = squeeze(var(mse_dist_time_sub_r, 0, 1));
    mse_psi_time_sub_r_var = squeeze(var(mse_psi_time_sub_r, 0, 1));
        
    figure(100*r);
    for s = 1:4
        subplot(2,5,s);
        errorbar(time_vec, mse_dist_time_sub_r_mean(:,s), mse_dist_time_sub_r_var(:,s) );
        legend(setupName{s});
        xlabel('time (s)'); ylabel('mse dist (m2)'); ylim([0,1]);
        title('averaged on all monte carlo');       
    end    
        
    subplot(2,5,5);
    stem(time_vec, obs_landmarks_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,5,10);
    stem(time_vec, obs_players_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);
        
    figure(100*r+1);    
    for s = 1:4
        subplot(2,5,s);
        errorbar(time_vec, mse_psi_time_sub_r_mean(:,s), mse_psi_time_sub_r_var(:,s) );
        legend(setupName{s});
        xlabel('time (s)'); ylabel('mse psi (rad2)'); ylim([0,0.1]);
        title('averaged on all monte carlo');
    end
end


%% Applying modified z-test
% individually on x and y axes; if a monte carlo is an outlier in either one of
% them, the sample is an outlier
% outlier detection is performed using modified z-test: assumes the
% distribution of errors is Gaussian. Then, calculates the median and the
% MAD: median of the absolute deviations from the median, as a way to
% remove outliers without them affecting the sample mean and variance.

subset_mc = 1:M;

err_x_time_sub = err_x_time_total(subset_mc, :, :, :);
err_y_time_sub = err_y_time_total(subset_mc, :, :, :);
err_psi_time_sub = err_psi_time_total(subset_mc, :, :, :);

% applying modified z test for each time, robot, method (i, r, s)
% outliers are identified in x and y individually (gaussians)
% error quantification is done on mse_dist
outlierList = cell(R,4);
mse_dist_no_outlier = cell(R,4);

% para N(0,2^2), sigma = 2 e MAD = 0.66*sigma
% logo, se os outliers est�o em k*sigma, o threshold usando mad deve ser
% 1.5*k*mad
k = 5.5*1000;
threshold = 1.5*k;
for r = 1:R
    for s = 1:4
        list = [];
        for i = 1:nIterations
            err_x_allmc = err_x_time_sub(:,i,r,s);
            med_x = median(err_x_allmc);
            mad_x = median( abs(err_x_allmc - med_x) );
            
            err_y_allmc = err_y_time_sub(:,i,r,s);
            med_y = median(err_y_allmc);
            mad_y = median( abs(err_y_allmc - med_y) );
            for m = 1:M
                z_value_x = (err_x_allmc(m) - med_x)/mad_x;
                z_value_y = (err_y_allmc(m) - med_y)/mad_y;
                if abs(z_value_x) > threshold || abs(z_value_y) > threshold           
                    list = [list, m];  
                end
            end            
        end
        list = sort(unique(list)); %sort and remove duplicate values from list
        aux = mse_dist_time_sub(:,:,r,s);
        aux(list,:) = []; % even if list has repeated values, only one row is removed
        mse_dist_no_outlier{r,s} = aux;
        outlierList{r,s} = list;
    end
end

% plotting mse versus time, without outliers
for r = 1:R        
    figure(100*r);
    for s = 1:4
        mse_dist_no_outlier_time = mse_dist_no_outlier{r,s};
        subplot(2,5,s+5);
        errorbar(time_vec, mean(mse_dist_no_outlier_time,1), var(mse_dist_no_outlier_time,0,1) );
        legend(setupName{s});
        xlabel('time (s)'); ylabel('mse dist (m2)'); ylim([0,1]);
        title('averaged on all monte carlo, without outliers');       
    end    
        
    subplot(2,5,5);
    stem(time_vec, obs_landmarks_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs landmarks'); ylim([0,8]);

    subplot(2,5,10);
    stem(time_vec, obs_players_average(r,:), 'filled', 'MarkerSize', 3);
    xlabel('time (s)'); ylabel('# obs players'); ylim([0,8]);

end

% plotting mse averaged over time, without outliers, for each method
for r = 1:R
    figure(30+r);
    for s = 1:4
        mse_dist_averaged_mean = mean(mean(mse_dist_no_outlier{r,s},2),1);
        mse_dist_averaged_var = var(mean(mse_dist_no_outlier{r,s},2),1);
        
%         mse_psi_averaged_mean = mean(mean(mse_psi_no_outlier{r,s},2),1);
%         mse_psi_averaged_var = var(mean(mse_psi_no_outlier{r,s},2),1);
        
        subplot(2,3,4);
        errorbar(s, mse_dist_averaged_mean, mse_dist_averaged_var); hold all;
        
%         subplot(2,3,5);
%         errorbar(s, mse_psi_averaged_mean, mse_psi_averaged_var); hold all;
    end
    subplot(2,3,4);    
    xlabel('method'); ylabel('mse'); title('Averaged on time and MC, dist, w/o outliers');
    legend(setupName); grid on;
    ylim([0, 1]); xlim([0,5]);
    
%     subplot(2,3,5);
%     xlabel('method'); ylabel('mse'); title('Averaged on time and MC, psi, w/o outliers');
%     legend(setupName); grid on;
%     ylim([0, 0.05]); xlim([0,5]);
end

figure(17);
for r=1:R
    outliers_per_method = zeros(4,1);
    for s=1:4
        outliers_per_method(s) = length(outlierList{r,s});
%         subplot(5,4,s+(r-1)*4)
%         stem(outlierList{r,s}, ones(length(outlierList{r,s})))
%         size(outlierList{r,s})
    end
    subplot(1,5,r)
    plot([1:4], outliers_per_method, 'rx:')
    xlim([0,5]); ylim([0,100])
    xlabel('method'); ylabel('# outliers removed'); title(['robot ' num2str(r)]);
end

% Removing outliers in Psi (will not be done soon)
mse_psi_no_outlier = cell(R,4);
for r=1:R
    for s = 1:4
        mse_psi_no_outlier{r,s} = mse_psi_time_sub(:,:,r,s);
    end
end


%%
% Para plotar, vamos deixar o plot do PLHA por �ltimo.
setupNamePlot = {'PL', 'PLH', 'PLA', 'PLHA'};
for r = 1:R
    aux = mse_dist_no_outlier{r,3};
    mse_dist_no_outlier{r,3} = mse_dist_no_outlier{r,4};
    mse_dist_no_outlier{r,4} = aux;
end

for r = 1:R
    aux = mse_psi_no_outlier{r,3};
    mse_psi_no_outlier{r,3} = mse_psi_no_outlier{r,4};
    mse_psi_no_outlier{r,4} = aux;
end

%% Plots finais para o artigo: dist�ncia
% time_vec = T:T:30*T;
time_vec = T * [1:nIterations];

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 1.1];
ylim_dist_param = [0, 1.4];

% PLOT 1: en vs. tempo (erro m�dio, entre todos os rob�s, versus tempo),
% para cada m�todo

% conte�do est� em mse_dist_no_outlier{r,s}, que � uma matriz M vs.
% nIteration
en_dist_metodo = zeros(4, nIterations);
for s=1:4
    enr_dist = zeros(R, nIterations);
    for r=1:R
        % mean over M
        enr_dist(r,:) = mean(mse_dist_no_outlier{r,s}, 1);
    end
    % mean over R
    en_dist_metodo(s,:) = mean(enr_dist,1);
end

% reminder: what must be plotted is rmse
figure(1);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {':', '--', '-.', '-'};
for s=1:4
    plot(time_vec, sqrt(en_dist_metodo(s,:)), linetype{s}, 'LineWidth', 1.5); hold all;
end
grid on; hold off;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Position RMSE (m)'); 
% title('Position error, averaged over all robots');
xlim(xlim_param); ylim(ylim_dist_param)

% PLOT 2: er para cada m�todo. 5 barras por m�todo
er_dist_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        er_dist_metodo(s,r) = mean(mean(mse_dist_no_outlier{r,s}));        
    end
end

figure(2);
bar(sqrt(er_dist_metodo)); ylim([0, 1.1]);
ylabel('Position RMSE (m)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
% title('Position error, for each robot, averaged over all time instants.');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

% casos mais favor�veis: rob�s que veem poucos landmarks (2 e 3)

% PLOT 3: rob�(s) para os quais a coopera��o � mais importante
ticktype = {'x', 'v', 'o', 's'}; %per method
linetype = {'--', '-.', '-', '-', ':'}; % per robot
figure(3)
r1 = 3; % primeira simula��o
r1 = 2; % segunda simula��o
for s = [1,4]
    mean_mse = mean( mse_dist_no_outlier{r1,s}, 1);
    var_mse = var( mse_dist_no_outlier{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);
    plot(time_vec, sqrt(mean_mse), [linetype{r1}], 'LineWidth', 1.5);
    hold all;
end

r2 = 5; % primeira simula��o
r2 = 1; % segunda simula��o
for s = [1,4]
    mean_mse = mean( mse_dist_no_outlier{r2,s}, 1);
    var_mse = var( mse_dist_no_outlier{r2,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r2}], 'LineWidth', 1.5);
    plot(time_vec, sqrt(mean_mse), [linetype{r2}], 'LineWidth', 1.5);
    hold all;
end
grid on; hold off; 
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], ['Robot ' num2str(r2) ' - ' setupNamePlot{1}], ['Robot ' num2str(r2) ' - ' setupNamePlot{4}]);
xlabel('Time (s)'); ylabel('Position RMSE (m)');
xlim(xlim_param); ylim(ylim_dist_param)
% title(['Position error for best and worst robot, averaged over all Monte Carlo realizations']);

% PLOT 4: n�mero m�dio de landmarks vistos pelo rob� mais e menos favoravel
figure(4);
% subplot(1,2,1);
total_camera_obs = [obs_landmarks_average(r1,:); obs_players_average(r1,:)].';
H = bar(time_vec, total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
xlabel('Time (s)'); ylabel('Number of camera observations');
% title(['Average number of observed features, for all Monte Carlo realizations. Robot ' num2str(r1)]); 
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);

figure(5);
% subplot(1,2,2);
total_camera_obs = [obs_landmarks_average(r2,:); obs_players_average(r2,:)].';
H = bar(time_vec, total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
xlabel('Time (s)'); ylabel('Number of camera observations');
% title(['Average number of observed features, for all Monte Carlo realizations. Robot ' num2str(r2)]); 
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);

%% Plots finais para o artigo: orientation

% PLOT 6: en vs. tempo (erro m�dio, entre todos os rob�s, versus tempo),
% para cada m�todo

% conte�do est� em mse_psi_no_outlier{r,s}, que � uma matriz M vs.
% nIteration
ylim_psi_param = [0, 0.25];
ylim_psi_param = [0, 10.3];

en_psi_metodo = zeros(4, nIterations);
for s=1:4
    enr_psi = zeros(R, nIterations);
    for r=1:R
        % mean over M
        enr_psi(r,:) = mean(mse_psi_no_outlier{r,s}, 1);
    end
    % mean over R
    en_psi_metodo(s,:) = mean(enr_psi,1);
end

% reminder: what must be plotted is rmse
figure(6);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {'--', '-.', '-', '-', ':'}; % per robot
for s=1:4
    plot(time_vec, sqrt(en_psi_metodo(s,:)), linetype{s}, 'LineWidth', 1.5); hold all;
end
hold off; grid on;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');
xlim(xlim_param); ylim(ylim_psi_param);

% PLOT 7: er para cada m�todo. 5 barras por m�todo
er_psi_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        er_psi_metodo(s,r) = mean(mean(mse_psi_no_outlier{r,s}));        
    end
end

figure(7); ylim(ylim_psi_param);
bar(sqrt(er_psi_metodo));
ylabel('Orientation RMSE (rad)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

% PLOT 8: er para cada rob�. 4 barras por rob�
figure(8);
H = bar(sqrt(er_psi_metodo.'));
ax = gca;
for k=1:4
    auxcolor = lines(k);
    set(H(k), 'facecolor', auxcolor(k,:), 'EdgeColor', 'none');
end
ylabel('Orientation RMSE (rad)'); xlabel('Robot')
legend(setupNamePlot); ylim(ylim_psi_param);

% casos mais favor�veis: rob�s que veem poucos landmarks (2 e 3)
% PLOT 9: rob�(s) para os quais a coopera��o � mais importante
ticktype = {'x', 'v', 'o', 's'}; %per method
linetype = {'--', '-.', '-', '-', ':'}; % per robot
figure(9)
r1 = 3;
r1 = 2;
for s = [1,4]
    mean_mse = mean( mse_psi_no_outlier{r1,s}, 1);
    var_mse = var( mse_psi_no_outlier{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);
    plot(time_vec, sqrt(mean_mse), [linetype{r1}], 'LineWidth', 1.5);
    hold all;
end

r2 = 5;
r2 = 1;
for s = [1,4]
    mean_mse = mean( mse_psi_no_outlier{r2,s}, 1);
    var_mse = var( mse_psi_no_outlier{r2,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
%     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
%     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r2}], 'LineWidth', 1.5);
    plot(time_vec, sqrt(mean_mse), [linetype{r2}], 'LineWidth', 1.5);
    hold all;
end
grid on; ylim(ylim_psi_param); xlim(xlim_param);
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], ['Robot ' num2str(r2) ' - ' setupNamePlot{1}], ['Robot ' num2str(r2) ' - ' setupNamePlot{4}]);
xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');

%% Plotting all robots, error vs. time, all methods in one plot
figure(20);
for r = 1:R
    
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( mse_dist_no_outlier{r,s}, 1);
        var_mse = var( mse_dist_no_outlier{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;
    %     errorbar(time_vec, sqrt(mean_mse), sqrt(var_mse));
    %     plot(time_vec, sqrt(mean_mse), [ticktype{s}, linetype{r1}], 'LineWidth', 1.5);

        plot(time_vec, sqrt(mean_mse), [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
    xlabel('Time (s)'); ylabel('Position RMSE (m)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
    subplot(2,5,r+5);
    total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    H = bar(time_vec, total_camera_obs, 'stacked');
    colors = [0, 0.75, 0; 0.75, 0, 0.75];
    for k = 1:2
        set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
    end
    xlabel('Time (s)'); ylabel('Number of camera observations');
    legend('Landmarks', 'Players'); ylim([0,8]);

end

%% Plotting each robot with the four methods and all Monte Carlos
for r = 1:R
    figure(20+r); close
end

beginning = 1:750;
for r = 1:R
    figure(20+r);
    for s = 1:4
        aux = mse_dist_no_outlier{r,s};
        for m = 1:size(aux,1)
            subplot(2,5,m);
            plot(time_vec(beginning), aux(m,beginning));
            ylim([0, 1]);
            hold all
        end
    end
end