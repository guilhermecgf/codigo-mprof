function [centralCornerList] = initializeCentralCorners(field)
    fieldWidth = field.fieldWidth;
    
    centralCornerList=zeros(2,2); %Landmark i => landmarkList(i,x,y)
    centralCornerList(1,:)=[0,-fieldWidth/2];%Center-bottom corner
    centralCornerList(2,:)=[0,+fieldWidth/2];%Center-top corner
end
