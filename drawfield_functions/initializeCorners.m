function [cornerList] = initializeCorners(field)
    fieldLength = field.fieldLength;
    fieldWidth = field.fieldWidth;
    
    cornerList=zeros(4,2); %Landmark i => landmarkList(i,x,y)
    cornerList(1,:)=[-fieldLength/2,  fieldWidth/2];%Left-top corner
    cornerList(2,:)=[-fieldLength/2, -fieldWidth/2];%Left-bottom corner
    cornerList(3,:)=[ fieldLength/2,  fieldWidth/2];%Right-top corner
    cornerList(4,:)=[ fieldLength/2, -fieldWidth/2];%Right-bottom corner
end
