function [penaltyKickList] = initializePenaltyKick(field)
    penaltyKickCoordinate = field.penaltyKickCoordinate;
    
    penaltyKickList=zeros(2,2);
    penaltyKickList(1,:)=[-penaltyKickCoordinate,0];
    penaltyKickList(2,:)=[ penaltyKickCoordinate,0];
end
