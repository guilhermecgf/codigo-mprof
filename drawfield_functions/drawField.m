function [] = drawField(landmarks, figureIndex)

if nargin == 1
    figureIndex = 1;
end

field = landmarks.field;

%setings
pointSize = 10;
lineWidth = 1;


%figure(figureIndex);
%figure(1); subplot(2,2,figureIndex);

%hold on;
for i = 1:length(landmarks.lineList)
    plot([landmarks.lineList(i,1),landmarks.lineList(i,3)], [landmarks.lineList(i,2), landmarks.lineList(i,4)],'b','LineWidth',lineWidth);
    hold on;
end

hold on;

plot(landmarks.cornerList(:,1),landmarks.cornerList(:,2),'.g','MarkerSize',5*pointSize);
plot(landmarks.centralCornerList(:,1),landmarks.centralCornerList(:,2), '.y', 'MarkerSize',pointSize);
plot(landmarks.middleOfTheFieldList(:,1),landmarks.middleOfTheFieldList(:,2),'.c','MarkerSize',pointSize);
plot(landmarks.goalList(:,1),landmarks.goalList(:,2),'.r','MarkerSize',5*pointSize);
plot(landmarks.penaltyKickList(:,1),landmarks.penaltyKickList(:,2),'.m','MarkerSize',pointSize);
plot(landmarks.goalAreaCornerList(:,1),landmarks.goalAreaCornerList(:,2),'.k','MarkerSize',pointSize);

fieldLength = field.fieldLength;
fieldWidth = field.fieldWidth;
goalWidth = field.goalWidth;
eps = 0.35;

text(-fieldLength/2, fieldWidth/2+eps, 'F1L', 'HorizontalAlignment', 'center');
text(-fieldLength/2, -fieldWidth/2-eps, 'F2L', 'HorizontalAlignment', 'center');
text(fieldLength/2, fieldWidth/2+eps, 'F1R', 'HorizontalAlignment', 'center');
text(fieldLength/2, -fieldWidth/2-eps, 'F2R', 'HorizontalAlignment', 'center');

text(-fieldLength/2, goalWidth/2+eps, 'G1L', 'HorizontalAlignment', 'left');
text(-fieldLength/2, -goalWidth/2-eps, 'G2L', 'HorizontalAlignment', 'left');
text(fieldLength/2, goalWidth/2+eps, 'G1R', 'HorizontalAlignment', 'right');
text(fieldLength/2, -goalWidth/2-eps, 'G2R', 'HorizontalAlignment', 'right');

plotLim = fieldLength/2 + 1;
axis([-plotLim, plotLim, -plotLim, plotLim]);
end
