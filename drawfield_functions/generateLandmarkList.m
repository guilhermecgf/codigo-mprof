function landmarks = generateLandmarkList(field)
landmarks.field = field;
landmarks.lineList              = initializeLines(field);
landmarks.cornerList            = initializeCorners(field);
landmarks.centralCornerList     = initializeCentralCorners(field);
landmarks.middleOfTheFieldList  = initializeMiddleOfTheField(field);
landmarks.goalList              = initializeGoals(field);
landmarks.penaltyKickList       = initializePenaltyKick(field);
landmarks.goalAreaCornerList    = initializeGoalAreaCorners(field);
end
