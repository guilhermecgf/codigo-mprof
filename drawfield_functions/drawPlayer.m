% Draws a player in the field, along with its local coordinate axes and
% angle of vision

function [] = drawPlayer(x,y,psi,visionAngle,ID)
%settings
pointSize = 10;
lineWidth = 1;

text(x+0.25,y-0.25,num2str(ID));
plot(x,y,'hk','MarkerSize',pointSize,'MarkerFaceColor','k');

plot([x,x+1*cos(psi)], [y, y+1*sin(psi)], '-', 'Color', [0.3, 0.3, 0.3], 'LineWidth', lineWidth);
plot([x,x+1*cos(psi+pi/2)], [y, y+1*sin(psi+pi/2)], '-', 'Color', [0.75, 0.75, 0.75], 'LineWidth', lineWidth);

if ID == 5
    hold on;
    plot([x,x+20*cos(psi+visionAngle)],[y,y+20*sin(psi+visionAngle)],'--k','LineWidth',lineWidth);
    plot([x,x+20*cos(psi-visionAngle)],[y,y+20*sin(psi-visionAngle)],'--k','LineWidth',lineWidth);
    
    text(x + 1.25*cos(psi), y+1.25*sin(psi), 'x�')
    text(x + 1.25*cos(psi+pi/2), y+1.25*sin(psi+pi/2), 'y�')
end

end
