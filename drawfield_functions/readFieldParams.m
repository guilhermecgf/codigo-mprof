function field = readFieldParams(filename)
    
    fid = fopen(filename);
    noFeatures = 7;
    features = cell(noFeatures,1);
    for i = 1:noFeatures
        line = fgets(fid);
        [~, content] = strtok(line, ' ');
        if strcmp(content(end),'\n')
            content(end) = [];
        end
        features{i} = content; 
    end
    field.type = features{1};
    field.fieldLength = str2double(features{2});
    field.fieldWidth = str2double(features{3});
    field.goalWidth = str2double(features{4});
    field.penaltyKickCoordinate = str2double(features{5});
    field.goalAreaLength = str2double(features{6});
    field.goalAreaWidth = str2double(features{7});
    
end