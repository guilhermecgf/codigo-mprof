close all; clear all;
addpath('algebraic_functions')
addpath('drawfield_functions')
addpath('measurement_functions')
addpath('savevariables_functions')
%% Multiplayer simulation: based on simulateHumanoidKidSize;
% simID = randi(1000) % para o nome do arquivo onde os dados ser�o salvos
simID = floor(1e3*rem(1e6*rem(now(),1),1))

rng(simID);

%% Parameter for generating scenario
Np = 600;
simConfig.time = 15;
simConfig.T = 0.02;
simConfig.nIterations = simConfig.time/simConfig.T;
simConfig.R = 5;

% Number of internal Monte Carlo iterations. Would be used if we wanted to
% preserve a noisy ground truth and obtain multiple (M) noisy measurements.
simConfig.M = 1;

talkerSchedule = zeros(simConfig.nIterations, 1);
nextTalker = 1;
for n = 2:2:simConfig.nIterations
    talkerSchedule(n) = nextTalker;
    nextTalker = nextTalker + 1;
    if nextTalker > simConfig.R
        nextTalker = nextTalker - simConfig.R;
    end
end

%Outputs configuration 
showfigOverview = false; % plot only with the players' position and pose
outputConfig.showfig = false;
outputConfig.withpause = false;
outputConfig.generatePrints = false;
makeVideo = false;

if(showfigOverview && makeVideo)
    video1 = VideoWriter('overall.avi');
    video1.FrameRate = 1/simConfig.T;
    
    open(video1);
end

if(outputConfig.showfig && makeVideo)
    video2 = VideoWriter('individual.avi');
    video2.FrameRate = 1/simConfig.T;
    
    open(video2);
end

% Initializing field
% fieldfile = 'fieldParamsHumanoid.txt';
fieldfile = 'fieldParamsSoccer3D.txt';
field = readFieldParams(fieldfile);
simConfig.landmarks = generateLandmarkList(field);

% Initial conditions for each robot. It is actually initialized with a
% random component

% Initial setup, all walking in circles
% zagueiro esquerdo, camisa 4. Old: -3, 1, 0.2, 0
initialConditions(1).x0 = -0.3*field.fieldLength;
% initialConditions(1).x0 = 0.3*field.fieldLength;
initialConditions(1).y0 = 0.2*field.fieldWidth;
initialConditions(1).psi0 = 0.9;
% initialConditions(1).psi0 = 160*pi/180;
initialConditions(1).v_x = 0.02*field.fieldLength;
initialConditions(1).v_y = 0*field.fieldWidth;
initialConditions(1).v_psi = 1*-0.45;
% initialConditions(1).v_psi = 1*0.75;

% segundo volante, camisa 8. old: 0.5, -0.5, 0.1, 0.1
initialConditions(2).x0 = 0.05*field.fieldLength;
initialConditions(2).y0 = -0.1*field.fieldWidth;
initialConditions(2).psi0 = 45*pi/180;
initialConditions(2).v_x = 0.01*field.fieldLength;
initialConditions(2).v_y = 0.02*field.fieldWidth;
initialConditions(2).v_psi = 1*0.10;

% ponta esquerda, camisa 11. Old: 2.5, 1, 0, -0.2 
initialConditions(3).x0 = 0.25*field.fieldLength;
initialConditions(3).y0 = 0.2*field.fieldWidth;
initialConditions(3).psi0 = -90*pi/180;
initialConditions(3).v_x = 0*field.fieldLength;
initialConditions(3).v_y = -0.04*field.fieldWidth;
initialConditions(3).v_psi = 1*0.12;

% centro avante, camisa 9. Old: 3.5, -1, 0.35, 0
initialConditions(4).x0 = 0.35*field.fieldLength;
initialConditions(4).y0 = -0.2*field.fieldWidth;
initialConditions(4).psi0 = 180*pi/180;
initialConditions(4).v_x = 0.035*field.fieldLength;
initialConditions(4).v_y = 0*field.fieldWidth;
initialConditions(4).v_psi = 1*-0.28;

% zagueiro direito, camisa 3. -3, -1, 0.22, 0
initialConditions(5).x0 = -0.3*field.fieldLength;
initialConditions(5).y0 = -0.2*field.fieldWidth;
initialConditions(5).psi0 = -10*pi/180;
initialConditions(5).v_x = 0.022*field.fieldLength;
initialConditions(5).v_y = 0*field.fieldWidth;
initialConditions(5).v_psi = 1*0.25;

% % Second setup, some desired trajectories
% % sees the other field from a big distance
% initialConditions(1).x0 = -4.0;
% initialConditions(1).y0 = 0;
% initialConditions(1).psi0 = -pi/180*0;
% initialConditions(1).v_x = 0.12;
% initialConditions(1).v_y = 0.0;
% initialConditions(1).v_psi = 0.00;
% 
% % does not see landmarks; walks along the centerline, towards the lateral
% initialConditions(2).x0 = -0.5;
% initialConditions(2).y0 = -2.5;
% initialConditions(2).psi0 = 90*pi/180;
% initialConditions(2).v_x = 0.12;
% initialConditions(2).v_y = 0.0;
% initialConditions(2).v_psi = 0.00;
% 
% % circular trajectory in the middle of the field; sees landmarks from a
% % moderate distance
% initialConditions(3).x0 = 0.0;
% initialConditions(3).y0 = 0.0;
% initialConditions(3).psi0 = 45*pi/180;
% initialConditions(3).v_x = 1*0.35;
% initialConditions(3).v_y = 0*0.0;
% initialConditions(3).v_psi = 1*-0.28;
% 
% % does not see landmarks; walks along the centerline, towards the lateral
% initialConditions(4).x0 = 0.5;
% initialConditions(4).y0 = 2.5;
% initialConditions(4).psi0 = -90*pi/180;
% initialConditions(4).v_x = 0.15;
% initialConditions(4).v_y = 0.0;
% initialConditions(4).v_psi = 0.0;
% 
% % zagueiro direito, camisa 3
% initialConditions(5).x0 = 3.5;
% initialConditions(5).y0 = -2.5;
% initialConditions(5).psi0 = 60*pi/180;
% initialConditions(5).v_x = 0.22;
% initialConditions(5).v_y = 0.0;
% initialConditions(5).v_psi = 0.25;

% % primeiro volante, camisa 5
% initialConditions(6).x0 = -0.5;
% initialConditions(6).y0 = 0.25;
% initialConditions(6).psi0 = 0*pi/180;
% initialConditions(6).v_x = 1*0.1;
% initialConditions(6).v_y = 1*0.1;
% initialConditions(6).v_psi = 1*0.35;
% 
% % ponta direita, camisa 7
% initialConditions(7).x0 = 2.5;
% initialConditions(7).y0 = -1.0;
% initialConditions(7).psi0 = 45*pi/180;
% initialConditions(7).v_x = 0*0.0;
% initialConditions(7).v_y = 1*0.2;
% initialConditions(7).v_psi = 1*-0.20;
% 
% % meia cl�ssico, camisa 10
% initialConditions(8).x0 = 2.5;
% initialConditions(8).y0 = 0.0;
% initialConditions(8).psi0 = 0*pi/180;
% initialConditions(8).v_x = 1*0.1;
% initialConditions(8).v_y = 0*0.0;
% initialConditions(8).v_psi = 1*-0.30;

% depois, conforme surgir necessidade, posicionar os outros jogadores

% MONTE CARLO ITERATIONS
Mtrajectory = 10;
for m = 1:Mtrajectory
    
% Defining team of robots and initializing the parameters for each one
Team = cell(simConfig.R,1);
playerMap = zeros(simConfig.R, 3, simConfig.nIterations);

%% Starting simulation: generating scenario
    
for r = 1:simConfig.R
    % por enquanto, s� especifiquei posi��o de 8 jogadores
    % actually, the initial position will be randomized in the Simulation
    % Setup method
    Team{r} = Humanoid(r, initialConditions(r));
    
    % estou copiando todo o objeto... talvez d� para copiar apenas o que for relevante
    Team{r} = Team{r}.simulationSetup(simConfig, outputConfig);
    
%     obj = obj.method();
%     obj.method();
    
    playerMap(r,:,1) = Team{r}.realData(1,:);
end
playerMapUpdated = playerMap;

% Running simulation; external loop is temporal, so each step corresponds to a time step T
% Inner loops could be run in parallel
% This loop generates sensor information.
% estimation can either be done inside or in another timeIndex loop
for timeIndex = 2:simConfig.nIterations
    disp(['generating simulation data, iteration number: ' num2str(timeIndex)]);
    
    for r = 1:simConfig.R
        % estou copiando todo o objeto...
        Team{r} = Team{r}.move(timeIndex);        
        playerMapUpdated(r,:,timeIndex) = Team{r}.realData(timeIndex,:);        
    end
    playerMap = playerMapUpdated;
    
    % Hear bearing every two time instants
%     if mod(timeIndex,2) == 0
    talkerIndex = talkerSchedule(timeIndex);
    if talkerIndex ~= 0
        for r = [1:talkerIndex-1, talkerIndex+1:simConfig.R]
            Team{r} = Team{r}.hear(timeIndex, talkerIndex, playerMap);
        end
    end
    
    % Observe with the camera every three time instants
    if mod(timeIndex,3) == 0
        for r = 1:simConfig.R
            Team{r} = Team{r}.observe(timeIndex, playerMap);
        end
    end
    
    if(outputConfig.showfig)        
        figure(1);
        
        ncols_plot = ceil(sqrt(simConfig.R));
        nrows_plot = ceil(simConfig.R/ncols_plot);
        for r = 1:simConfig.R
            subplot(nrows_plot, ncols_plot, r); hold off;
            Team{r}.plotPlayerInfo(timeIndex);
        end
        set(gcf, 'position', [-100,-100, 1251, 788]);
        subplot(nrows_plot, ncols_plot, simConfig.R);        
        title(['index = ' num2str(timeIndex) ', time = ' num2str(simConfig.T*timeIndex) 's']);
        
        if(outputConfig.showfig && makeVideo)
            frame2 = getframe(gcf);
            writeVideo(video2, frame2);
        end        
    end            
    
    if(showfigOverview)    
        figure(100); %draw playerMapUpdated   
        clf('reset');
        drawField(simConfig.landmarks)
        for r = 1:simConfig.R
            drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex), pi/6, r );
            title(['index = ' num2str(timeIndex) ', time = ' num2str(simConfig.T*timeIndex) 's']);
            set(gcf, 'position', [100,0,577,548]); % no meu Asus de 15.6 polegadas, o desenho do campo tem dimens�es 9x6 cm
            %drawPlayer( playerMapUpdated(r,1,timeIndex), playerMapUpdated(r,2,timeIndex), playerMapUpdated(r,3,timeIndex), Team{r}.visionAngle );
        end
        
        if(showfigOverview && makeVideo)
            frame1 = getframe(gcf);
            writeVideo(video1, frame1);
        end        
    end
    
    if(outputConfig.withpause)
%         pause(simConfig.T - timeElapsedInCalculations);
        pause; % advance pressing any key
    end
end

if(showfigOverview && makeVideo)
    close(video1);
end

if(outputConfig.showfig && makeVideo)
    close(video2);
end

%% Configuring setup for estimation
setup = {'PL','PLH','PLHA','PLA'};
flags = [true, true, false, false;
         true, true, true, false;
         true, true, true, true;
         true, true, false, true];
     
% setup = {'P'};
% flags = [true, false, false, false];
%      
% setup = {'PL'};
% flags = [true, true, false, false];
%      
% setup = {'PLH'};
% flags = [true, true, true, false];
% 
% setup = {'PLA'};
% flags = [true, true, false, true];
% 
% setup = {'PLHA'};
% flags = [true, true, true, true];
     
% % if true, resamples after every set assimilation (landmarks, hearing and/or allies) 
% % if false, resamples depending on the effective number of particles
% alwaysResample = false;

% if true, filter performs resampling and regularization
% if false, filter performs only resampling
bRegularize = true;

% if true, all players know at all times the most recent belief of each ally
% if false, the information from each ally's belief comes only at the talking instants
mostRecentBelief = false;

% Number of particles. P and L steps are linear on Np, whereas H and A are
% quadratic
Np = Np;

%% Estimation step
    
% on each column, one estimation algorithm will be performed and the
% corresponding data for each robot will be stored.
TeamEst = cell(simConfig.R, length(setup));
TeamEstSave = cell(simConfig.R, length(setup));
for r = 1:simConfig.R
    for s = 1:length(setup)
        TeamEst{r,s} = Team{r};
    end
end

% initializing the algorithms
% self position uncertainty
for s = 1:length(setup)
    
    for r = 1:simConfig.R
        Sigma0 = [0.5, 0, 0;
                  0, 0.5, 0;
                  0, 0, 0.05].^2;
              
%         TeamEst{r,s} = TeamEst{r,s}.initializeSMC(Np, var_x0, var_y0, var_psi0);
        TeamEst{r,s} = TeamEst{r,s}.initializeSMC(Np, Sigma0);
    end
    
    % All allies have each other initial belief
    for talkerIndex = 1:simConfig.R
        initialBelief = cell(1,5);
        initialBelief{1} = 0; % time
        initialBelief{2} = 1; % number of modes
        initialBelief{3} = 1; % vector of weight of modes
        initialBelief{4} = [initialConditions(talkerIndex).x0; initialConditions(talkerIndex).y0; initialConditions(talkerIndex).psi0] + mvnrnd([0;0;0], Sigma0).';
        % WRAP ANGLE
%         initialBelief{5} = diag([var_x0, var_y0, var_psi0]);
        initialBelief{5} = Sigma0;
        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, initialBelief);
        end
    end  
end

avg_cputime_per_robot = zeros(4, simConfig.nIterations);

for timeIndex = 2:simConfig.nIterations
    
%     timeIndex
    
%     for s = 1:4
    for s = 1:length(setup)
%         s
        % measuring time, for each method, for all robots
        tic;

        performPrediction = flags(s,1);
        performLandmarks = flags(s,2);
        performHearing = flags(s,3);
        performAlliesObs = flags(s,4);

        if mod(timeIndex, 1) == 0
            disp(['MC: ' num2str(m) ' - ' setup{s} ' - estimation, it. no.: ' num2str(timeIndex)]);
        end

        if performPrediction
            for r = 1:simConfig.R
                % WITHOUT MONTE CARLO MODIFICATION
                
%                 if r == 1
%                     % plot before performing prediction
%                     TeamEst{r,s}.scatterParticles(timeIndex, 1, 'before P');
%                 end
                
                TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex);
                
%                 if r == 1
%                     % plot after performing prediction
%                     TeamEst{r,s}.scatterParticles(timeIndex, 2, 'after P');
%                 end
                
%                 % WITH OLD MONTE CARLO MODIFICATION
%                 TeamEst{r,s} = TeamEst{r,s}.predictSelfPosition(timeIndex, m);
                

            end
        end

        if performLandmarks
            for r = 1:simConfig.R
                % WITHOUT MONTE CARLO ASSIMILATION
                                
                TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex);
                
%                 % WITH OLD MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateLandmarks(timeIndex, m);
                
%                 if r == 1
%                     % plot after performing assimilation
%                     TeamEst{r,s}.scatterParticles(timeIndex, 3, 'after L');
%                 end
               
                % Although performing resampling and regularization here
                % would somehow improve the numerical stability, it would
                % bias the new particles to those more favorable to this
                % assimilation step, giving less relevance to the
                % variability that could be exploited by further incoming
                % measurements.
%                 Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
%                 if Neff < 0.6*Np
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
% %                     disp(['index ' num2str(timeIndex) ', after L, agent ' num2str(r) ' - resampled']);
%                 end
                
%                 TeamEst{r,s}.scatterParticles(timeIndex, 6);
            end
        end

        if mostRecentBelief
            % Receiving all allies' most recent belief at all time instants
            for talkerIndex = 1:simConfig.R
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end        
        else    
            % Receive neighbors' self belief according to talkerSchedule
            talkerIndex = talkerSchedule(timeIndex);
            if talkerIndex ~= 0
                [belief, TeamEst{talkerIndex,s}] = TeamEst{talkerIndex,s}.generateGMM(timeIndex);    
                for r = 1:simConfig.R
                    TeamEst{r,s} = TeamEst{r,s}.receiveBelief(timeIndex, talkerIndex, belief);
                end
            end
        end

        if performHearing
            for r = 1:simConfig.R
                % WITHOUT MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex);
                
%                 if r == 1
%                     TeamEst{r,s}.scatterParticles(timeIndex, 4, 'after H');
%                 end
                
%                 % WITH OLD MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateDoA(timeIndex, m);
                           
                
                % Although performing resampling and regularization here
                % would somehow improve the numerical stability, it would
                % bias the new particles to those more favorable to this
                % assimilation step, giving less relevance to the
                % variability that could be exploited by further incoming
                % measurements.
%                 Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
%                 if Neff < 0.6*Np
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
% %                     disp(['index ' num2str(timeIndex) ', after H, agent ' num2str(r) ' - resampled']);
%                 end
                
                
            end
        end

        if performAlliesObs
            for r = 1:simConfig.R
                % WITHOUT MONTE CARLO ASSIMILATION
                TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex);
                
%                 if r == 1
%                     TeamEst{r,s}.scatterParticles(timeIndex, 4, 'after A');
%                 end
                
%                 % WITH MONTE CARLO ASSIMILATION
%                 TeamEst{r,s} = TeamEst{r,s}.assimilateAllies(timeIndex, m);
                
                               
                
                % Although performing resampling and regularization here
                % would somehow improve the numerical stability, it would
                % bias the new particles to those more favorable to this
                % assimilation step, giving less relevance to the
                % variability that could be exploited by further incoming
                % measurements.
%                 Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
%                 if Neff < 0.6*Np
%                     TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
% %                     disp(['index ' num2str(timeIndex) ', after A, agent ' num2str(r) ' - resampled']);
%                 end
                
            end
        end        

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateNewBelief(timeIndex);
        end
        
        % After the assimilation step, perform resampling and regularization
        for r = 1:simConfig.R
            Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);        
        
%             if r == 5
%                 disp('Neff pre resampling = ');
%                 disp(Neff);
%             end

            if Neff < 0.6*Np
                TeamEst{r,s} = TeamEst{r,s}.resampleParticles(timeIndex, bRegularize);
                
%                 % Neff after regularization must be Np. Sanity check
%                 if r == 1
%                     disp('Neff post resampling = ');
%                     Neff = TeamEst{r,s}.effectiveParticles(TeamEst{r,s}.mess_Trans_Particles);
%                     disp(Neff);
%                 end
            end
        end
        
        % elapsed time, on that time instant, in millisseconds. All robots
        % are processed here
        avg_cputime_per_robot(s, timeIndex) = toc/simConfig.R*1000;

        for r = 1:simConfig.R
            TeamEst{r,s} = TeamEst{r,s}.evaluateErrors(timeIndex);
        end
        
%         pause;

        % proceeding with save
        if mod(timeIndex, 5) == 0 || timeIndex == simConfig.nIterations
            % desired name of folder/file
            if mod(timeIndex, 5) == 0
                filename = ['output_temp/' num2str(simID) '_' setup{s} '_MCtraj' num2str(m, '%03d') '.mat'];
            end
            
            if timeIndex == simConfig.nIterations
                filename = ['output_final/' num2str(simID) '_' setup{s} '_MCtraj' num2str(m, '%03d') '.mat'];
            end
            
            % eliminating particles from TeamEstSave, which are very heavy
            for r = 1:simConfig.R
                TeamEstSave{r,s} = extractAllPropertiesHumanoidOctave(TeamEst{r,s});
                TeamEstSave{r,s}.mess_B_Particles = [];
                TeamEstSave{r,s}.mess_P_Particles = [];
                TeamEstSave{r,s}.mess_L_Particles = [];
                TeamEstSave{r,s}.mess_H_Particles = [];
                TeamEstSave{r,s}.mess_A_Particles = [];
                TeamEstSave{r,s}.mess_Trans_Particles = [];
            end
            
            structsave = [];
            structsave.simConfig = simConfig;
            structsave.TeamEstSave = TeamEstSave;
            structsave.Mtrajectory = Mtrajectory;
            structsave.Np = Np;
            structsave.initialConditions = initialConditions;
            structsave.initialBelief = initialBelief;
            structsave.mostRecentBelief = mostRecentBelief;
            structsave.setup = setup;
            structsave.simID = simID;
            structsave.talkerSchedule = talkerSchedule;
            structsave.avg_cputime_per_robot = avg_cputime_per_robot;
            
            try                
                parsave(filename, structsave);
            catch ME
                warning(['could not save ', filename]);
                throw(ME);
            end            
        end
        
    end % s = 1:length(setup)

end % timeIndex = 1:nIterations

end % m = 1:M
